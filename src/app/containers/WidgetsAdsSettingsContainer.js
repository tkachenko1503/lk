import React from 'react';
import connectToStore from 'lib/connectToStore';
import WidgetsAdsSettings from 'components/WidgetsAdsSettings';
import {setAppContext} from 'modules/appContext/actions';
import {findByProp} from 'lib/index';
import {updateWidget, fetchWidgets, resetWidgetsChanges,
        saveAllWidgets, initSavableListener} from 'actions/widgets';

class WidgetsAdsSettingsContainer extends React.Component {
    constructor(props) {
        super(props);
        
        this.setAdsPlaces = this.setAdsPlaces.bind(this);
        this.saveAllChanges = this.saveAllChanges.bind(this);
    }

    componentWillMount() {
        const {currentSite, widgetsList} = this.props;

        if (currentSite && !widgetsList.length) {
            fetchWidgets(currentSite);
        }

        initSavableListener(true);

        this.fetchWidgetsOnSiteChange = setAppContext.map(fetchWidgets);
        this.initSavableListenerOnSiteChange = setAppContext.map(initSavableListener);
        this.initSavableListenerOnSaveAllWidgets = saveAllWidgets.map(initSavableListener);
        this.resetWidgetsChangesOnSiteChange = setAppContext.map(() => resetWidgetsChanges(this.props.widgetsList));
    }

    componentWillUnmount() {
        this.fetchWidgetsOnSiteChange.end(true);
        this.resetWidgetsChangesOnSiteChange.end(true);
        this.initSavableListenerOnSiteChange.end(true);
        this.initSavableListenerOnSaveAllWidgets.end(true);

        resetWidgetsChanges(this.props.widgetsList);
    }

    setAdsPlaces(widgetId, adsPlacesNumber) {
        const {widgetsList} = this.props;

        updateWidget({
            widget: findByProp(widgetId, widgetsList, 'id'),
            attrs: {
                adsPlacesNumber
            }
        });
    }

    saveAllChanges() {
        saveAllWidgets(this.props.widgetsList);
    }
    
    render() {
        const {widgetsList, currentSite, canSaveAdsSettings} = this.props;

        return (
            <div>
                {currentSite
                    ? (
                        <WidgetsAdsSettings
                            widgetsList={widgetsList}
                            canSaveAdsSettings={canSaveAdsSettings}
                            setAdsPlaces={this.setAdsPlaces}
                            saveAllChanges={this.saveAllChanges}
                        />
                    )
                    : 'Выберите сайт'}
            </div>

        );
    }
}

export default connectToStore(WidgetsAdsSettingsContainer, ({app, widgets}) => ({
    currentSite: app.selectedContext,
    widgetsList: widgets.list,
    canSaveAdsSettings: widgets.canSaveAdsSettings
}));
