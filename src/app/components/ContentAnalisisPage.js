import React from 'react';
import Layout from 'components/Layout';
import Panel from 'react-bootstrap/lib/Panel';
import PopularArticles from 'containers/PopularArticles';
import DashboardFilters from 'containers/DashboardFilters';

import panel from 'styles/panel.css';

const ContentAnalisisPage = props => {
    const panelHeader = (<h4>Наиболее популярные статьи</h4>);

    return (
        <Layout>
            <div className="widgets-analysis-content-container">
                <DashboardFilters
                    actionForApplyFilters={props.actionForApplyFilters}
                />
                <div className="wrapper">
                    <Panel
                        header={panelHeader}
                        className={panel.fixedHeader}
                    >
                        <PopularArticles />
                    </Panel>
                </div>
            </div>
        </Layout>
    );
};

export default ContentAnalisisPage;
