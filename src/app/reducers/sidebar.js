import curryN from 'ramda/src/curryN';
import {Sidebar} from 'actions/types';

/**
 * Дефолтное состояние сайдбара
 * @returns {{collapsed: boolean, navItem: {opened: boolean, id: null}}}
 */
export function defaultSidebar() {
    return {
        collapsed: false,
        navItem: {
            opened: false,
            id: null
        }
    };
}

const reducefn = Sidebar.caseOn({
    /**
     * Выставляем флаг развернуть/свернуть сайдбар
     * @param state
     */
    ToggleSidebar: (state) => ({
        ...state,
        collapsed: !state.collapsed
    }),

    /**
     * Выставляем флаг развернуть/свернуть навигационное подменю
     * @param navId
     * @param state
     */
    ToggleNavMenu: (navId, state) => ({
        ...state,
        navItem: {
            opened: state.navItem.id === navId ? !state.navItem.opened : true,
            id: navId
        }
    })
});

export default curryN(2, reducefn);
