import React from 'react';
import flyd from 'flyd';

/**
 * Создаёт HOC для прокидывания данных из стора в пропсы переданного компонента
 * @param {React.Component} Component
 * @param {Function} mapState функция указывает какие данные нужны компоненту
 * @returns {React.Component}
 */
export default function connectToStore(Component, mapState) {
    return React.createClass({
        contextTypes: {
            store$: React.PropTypes.func
        },

        getInitialState () {
            return this.context.store$();
        },

        componentDidMount () {
            this.__subscribe$ = flyd.on(this.handleStoresChanged, this.context.store$);
        },

        componentWillUnmount () {
            this.__subscribe$.end(true);
        },

        handleStoresChanged (state) {
            this.setState(state);
        },

        render () {
            const props = {
                ...this.props,
                ...(mapState ? mapState(this.state) : {})
            };

            return <Component {...props} />;
        }
    });
};
