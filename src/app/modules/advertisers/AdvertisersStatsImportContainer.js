import React from 'react';
import connectToStore from 'lib/connectToStore';
import AdvertisersStatsImport from 'modules/advertisers/AdvertisersStatsImport';
import {chooseAdvertisersStatsFile, uploadAdvertisersStatsFile, chooseAdsCampaign} from 'modules/advertisers/actions';
import {isAdvertiserInstance} from 'modules/advertisers';
import AdsCampaignsDataContainer from 'modules/adsCampaigns/AdsCampaignsDataContainer';
import {findByProp} from 'lib/index';

class AdvertisersStatsImportContainer extends React.Component {
    constructor(props) {
        super(props);

        this.uploadFile = this.uploadFile.bind(this);
    }

    chooseFile(files) {
        chooseAdvertisersStatsFile(files[0]);
    }

    chooseAdsCampaign(e, AdsCampaignId) {
        chooseAdsCampaign(AdsCampaignId);
    }

    uploadFile() {
        const {currentContext, file, campaign} = this.props;

        uploadAdvertisersStatsFile({
            advertisersStats: file,
            advertiser: currentContext.id,
            campaign
        });
    }

    render() {
        const {currentContext, isAdmin, adsCampaignsList, file, campaign, adsCampaign} = this.props;
        const isAdvertiserContext = currentContext && isAdvertiserInstance(currentContext);

        if (!isAdmin) {
            return null;
        }

        return (
            <div>
                <AdsCampaignsDataContainer />
                <AdvertisersStatsImport
                    uploadFile={this.uploadFile}
                    chooseFile={this.chooseFile}
                    openFileBrowser={this.openFileBrowser}
                    chooseAdsCampaign={this.chooseAdsCampaign}
                    file={file}
                    campaign={campaign}
                    campaignName={adsCampaign && adsCampaign.get('name')}
                    isAdvertiserContext={isAdvertiserContext}
                    adsCampaignsList={adsCampaignsList}
                />
            </div>
        );
    }
}

export default connectToStore(AdvertisersStatsImportContainer, ({advertisers, app, user, adsCampaigns}) => ({
    file: advertisers.file,
    campaign: advertisers.campaign,
    adsCampaign: advertisers.campaign && findByProp(advertisers.campaign, adsCampaigns.adsCampaignsList, 'id'),
    isAdmin: user.isAdmin,
    adsCampaignsList: adsCampaigns.adsCampaignsList,
    currentContext: app.selectedContext && findByProp(app.selectedContext, app.context, 'id')
}));