import React from 'react';
import classnames from 'classnames';
import Dropdown from 'components/Dropdown';
import MenuItem from 'react-bootstrap/lib/MenuItem';

import styles from 'styles/sitesSelect.css';

const SitesSelect = (props) => {
    const {currentSite, chooseSite, currentSiteName} = props;

    return  (
        <div id="navbar-collapse-1"
             className={classnames(
                styles.sitesSelect,
                "navbar-collapse collapse yamm mega-menu"
             )}
        >
            <ul
                className={classnames(
                    styles.sitesSelectNav,
                    "nav navbar-nav"
                )}
            >
                <li className="dropdown">
                    <Dropdown
                        id="sites-select"
                        title={currentSite ? currentSiteName : 'Выберите сайт'}
                        className={styles.sitesSelectMenu}
                    >
                        {props.availableSites.length
                            ?   props.availableSites.map(site => (
                                    <MenuItem
                                        key={site.objectId}
                                        active={(site.objectId === currentSite)}
                                        eventKey={site.objectId}
                                        onSelect={chooseSite}
                                    >
                                        {site.name}
                                    </MenuItem>
                                ))
                            :   <div className={styles.noAvailableSites}>
                                    Нет доступных сайтов
                                </div>
                        }
                    </Dropdown>
                </li>
            </ul>
        </div>
    );
};

export default SitesSelect;
