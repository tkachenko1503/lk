import Panel from 'react-bootstrap/lib/Panel';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import Button from 'react-bootstrap/lib/Button';
import Chart from 'components/Chart';
import classnames from 'classnames';

const ClickByAlgoritm = () => {
    const panelHeader = (
        <div>
            Клики по алгоритмам
            <span className="tools pull-right">
                <a className="fa fa-repeat box-refresh" href="javascript:;"></a>
            </span>
        </div>
    );

    return (
        <Panel header={panelHeader} bsClass={classnames(
            styles.chartPanel,
            "panel"
        )}>

        </Panel>
    );
};

const PopularPosts = () => {
    const panelHeader = (
        <div>
            САМЫЕ ПОПУЛЯРНЫЕ СТАТЬИ
            <small> ПО ПОКАЗАМ В ВИДЖЕТЕ</small>
        </div>
    );

    return (
        <Panel header={panelHeader} bsClass={classnames(
            styles.chartPanel,
            "panel"
        )}>
            <div className="tbl-top clearfix">
                <div className="dataTables_length pull-right">
                    <ButtonGroup bsSize="small">
                        <Button>CSV</Button>
                    </ButtonGroup>
                </div>
            </div>
            <table className="table convert-data-table data-table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Число показов</th>
                    <th>Число кликов</th>
                    <th>CTR</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Козел Тимур находится в шоковом состоянии после конфликта с тигром Амуром </td>
                    <td>912</td>
                    <td>473</td>
                    <td>41.1%</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Козел Тимур находится в шоковом состоянии после конфликта с тигром Амуром </td>
                    <td>912</td>
                    <td>473</td>
                    <td>41.1%</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Козел Тимур находится в шоковом состоянии после конфликта с тигром Амуром </td>
                    <td>912</td>
                    <td>473</td>
                    <td>41.1%</td>
                </tr>
                </tbody>
            </table>
        </Panel>
    );
};

const AlgoritmClicksChart = () => {
    const chartConfig =  {
        data: {
            columns: [
                ['Semantic', 42],
                ['Default', 2],
                ['Categorial', 9],
                ['Personal', 10],
                ['Behavioral', 36]
            ],
            type : 'donut'
        },
        size: {
            width: 280,
            height: 268
        }
    };

    return (
        <div className="pie-chart">
            <Chart config={chartConfig} />
        </div>
    );
};
