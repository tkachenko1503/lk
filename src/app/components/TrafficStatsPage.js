import React from 'react';
import Layout from 'components/Layout';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import DashboardFilters from 'containers/DashboardFilters';
import TrafficStatsOverview from 'containers/TrafficStatsOverview';
import TrafficStatsChartContainer from 'containers/TrafficStatsChartContainer';
import LoadInfo from 'components/LoadInfo';

import styles from 'styles/dashboard.css';

const TrafficStatsPage = (props) => {
    return (
        <Layout>
            <div className="ads-statistic-container">
                <DashboardFilters
                    actionForApplyFilters={props.actionForApplyFilters}
                />
                <div className="wrapper">
                    <TrafficStatsOverview />

                    <Grid fluid={true} className={styles.dashboardGrid}>
                        <Row>
                            <Col xs={8}>
                                <TrafficStatsChartContainer />
                            </Col>
                            <Col xs={4}>
                                <LoadInfo
                                    loadCSV={props.loadCSV}
                                />
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </div>
        </Layout>
    );
};

export default TrafficStatsPage;
