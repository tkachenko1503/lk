import React from 'react';
import Layout from 'components/Layout';
import Panel from 'react-bootstrap/lib/Panel';
import WidgetsAdsSettingsContainer from 'containers/WidgetsAdsSettingsContainer';

import panel from 'styles/panel.css';

const AdsPage = _ => {
    const panelHeader = (<h4>Настройки показа рекламы</h4>);

    return (
        <Layout>
            <div className="widgets-ads-container">
                <div className="wrapper">
                    <Panel
                        header={panelHeader}
                        className={panel.fixedHeader}
                    >
                        <h6>
                            Выберите минимум один виджет для начала показа рекламы
                        </h6>

                        <WidgetsAdsSettingsContainer />
                    </Panel>
                </div>
            </div>
        </Layout>
    );
};

export default AdsPage;
