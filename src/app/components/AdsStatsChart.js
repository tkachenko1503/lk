import React from 'react';
import Chart from 'components/Chart';
import StatsChart from 'components/StatsChart';
import classnames from 'classnames';

import styles from 'styles/chart.css';

class AdsStatsChart extends StatsChart {
    render() {
        const {dateRange} = this.props;
        const withTime = this.isNeedShowTime(dateRange);
        const config = chartConfig(this.props, withTime);

        return (
            <div className={classnames(styles.fixChartTooltipAlign)}>
                <Chart config={config} redraw={this.needRedraw} />
            </div>
        );
    }
}

export default AdsStatsChart;

function chartConfig({created, first, second, col1, col2}, withTime) {
    return {
        data: {
            x: 'x',
            xFormat: '%Y-%m-%dT%H:%M:%S.%Z',
            columns: [
                ['x'].concat(created),
                [first].concat(col1),
                [second].concat(col2)
            ],
            types: {
                [first]: 'area'
            },
            axes: {
                [first]: 'y',
                [second]: 'y2'
            },
            colors: {
                [first]: '#6ED594',
                [second]: '#179AD0'
            },
            unload: true
        },
        axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: withTime ? '%d.%m-%H:%M' : '%d.%m',
                    culling: {
                        max: 10
                    }
                }
            },
            y: {
                label: {
                    position: 'outer-middle'
                },
                padding: {
                    top: 10,
                    bottom: 0
                },
                min: 0
            },
            y2: {
                label: {
                    position: 'outer-middle'
                },
                padding: {
                    top: 10,
                    bottom: 0
                },
                min: 0,
                show: true
            }
        }
    }
}
