import curryN from 'ramda/src/curryN';
import {AdsPayments} from 'actions/types';

export function defaultAdsPayments() {
    return {
        summaries: {
            availableForCashing: 0,
            incomAll: 0,
            incomLastMonth: 0,
            incomLastThreeMonths: 0
        },
        payments: {
            total: {
                creditedWith: 0,
                cashedOut: 0
            },
            daily: []
        }
    };
}


const reducefn = AdsPayments.caseOn({
    SetAdsPayments: (adsPayments, state) => ({
        ...state,
        ...adsPayments
    }),
    RequestAdsPayments: (params, state) => ({...state})
});

export default curryN(2, reducefn);