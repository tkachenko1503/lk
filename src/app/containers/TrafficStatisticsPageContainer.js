import React from 'react';
import connectToStore from 'lib/connectToStore';
import TrafficStatsPage from 'components/TrafficStatsPage';
import {requestAdsStatsForSite} from 'actions/adsStats';
import {setAppContext} from 'modules/appContext/actions';
import {exportTrafficCsvFile} from 'actions/trafficStats';
import {findByProp} from 'lib/index';

/**
 * Главный контейнер со статистикой по рекламе
 * @constructor
 */
class TrafficStatisticsPageContainer extends React.Component {
    constructor(props) {
        super(props);
        this.requestAdsStats = this.requestAdsStats.bind(this);
        this.exportCSVFile = this.exportCSVFile.bind(this);
    }

    componentDidMount() {
        const {currentSiteId} = this.props;

        if (currentSiteId) {
            this.requestAdsStats(currentSiteId);
        }
        this.contextChangeHandler = setAppContext.map((s) => {
            setTimeout(() => this.requestAdsStats(s), 0);
        });
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
    }

    requestAdsStats(siteId) {
        const {availableSites, filters} = this.props;
        const site = findByProp(siteId, availableSites, 'id');

        requestAdsStatsForSite({
            site: site && site.get('name'),
            filters: filters
        });
    }

    exportCSVFile() {
        const {fullAdsList, filters} = this.props;
        const {currentSiteId, availableSites} = this.props;
        const {currentWidgetId, widgetsList} = this.props;

        const site = findByProp(currentSiteId, availableSites, 'id');
        const widget = currentWidgetId && findByProp(currentWidgetId, widgetsList, 'id');
        
        exportTrafficCsvFile({
            rows: fullAdsList,
            datesRange: filters.date,
            currentSite: site && site.get('name'),
            currentWidget: widget && widget.get('name')
        });
    }

    render() {
        const {load, createdAt} = this.props;
        return (
            <TrafficStatsPage
                actionForApplyFilters={requestAdsStatsForSite}
                load={load}
                createdAt={createdAt}
                loadCSV={this.exportCSVFile}
            />
        );
    }
}

export default connectToStore(TrafficStatisticsPageContainer, ({app, filters, adsStats, widgets}) => ({
    currentSiteId: app.selectedContext,
    availableSites: app.context,
    filters,
    load: adsStats.statsSummaries.widget_load,
    createdAt: adsStats.statsSummaries.created_at,
    fullAdsList: adsStats.statsList,
    currentWidgetId: filters.widget,
    widgetsList: widgets.list
}));

