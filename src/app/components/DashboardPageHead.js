import React from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import DateRangeFilter from 'components/DateRangeFilter';
import CustomDateRangeFilter from 'components/CustomDateRangeFilter';
import WidgetSelect from 'components/WidgetSelect';
import WidgetSelectWithIcons from 'components/WidgetSelectWithIcons';
import {findByProp} from 'lib/index';

import styles from 'styles/dashboardPageHead.css';

const DashboardPageHead = (props) => {
    const {
        filters, setFilter,
        setFromDate, setToDate,
        currentSite, applyFilters,
        widgetsList, setWidgetFilter,
        setCampaignFilter, campaignsList,
        contextType, widgetSelectWithIcon,
        showWidgetSelect = true
    } = props;

    let widgetSelect;
    
    if (showWidgetSelect) {
        if (contextType === 'Site') {
            if (widgetSelectWithIcon) {
                widgetSelect = (
                    <WidgetSelectWithIcons
                        widgetsList={widgetsList}
                        widgetSelectHandler={setWidgetFilter}
                        choosenWidget={filters.widget}
                        defaultTitle="Все форматы"
                    />
                );
            } else {
                widgetSelect = (
                    <WidgetSelect
                        widgetsList={widgetsList}
                        widgetSelectHandler={setWidgetFilter}
                        choosenWidget={filters.widget}
                        defaultTitle="Все виджеты"
                    />
                );
            }
        } else {
            widgetSelect = (
                <WidgetSelect
                    widgetsList={campaignsList}
                    widgetSelectHandler={setCampaignFilter}
                    choosenWidget={filters.campaign}
                    defaultTitle="Все кампании"
                />
            );
        }
    }

    return (
        <div className="page-head">
            <Grid fluid={true} className={styles.noPadding}>
                <Row>
                    <Col xs={12} md={5} className={styles.datePrecet}>
                        <DateRangeFilter
                            currentFilter={filters.date.range}
                            setFilter={setFilter}
                        />
                    </Col>
                    <Col xs={12} md={4} className={styles.dateInputs}>
                        <CustomDateRangeFilter
                            date={filters.date}
                            meta={filters.meta}
                            setFromDate={setFromDate}
                            setToDate={setToDate}
                        />
                    </Col>
                    <Col className={styles.widgetSelect} xs={6} md={1}>
                        {widgetSelect}
                    </Col>
                    <Col className={styles.applyButton} xs={6} md={2} lg={12}>
                        <ApplyButton
                            currentSite={currentSite}
                            applyFilters={applyFilters}
                        />
                    </Col>
                </Row>
            </Grid>
        </div>
    );
};

export default DashboardPageHead;

const ApplyButton = (props) => {
    return (
        <Button
            bsStyle="success"
            bsSize="small"
            disabled={!props.currentSite}
            onClick={props.applyFilters}
        >
            Применить
        </Button>
    );
};
