import React from 'react';
import connectToStore from 'lib/connectToStore';
import StatsSummaryPanels from 'components/StatsSummaryPanels';
import {TRAFFIC_STATS_SUMMARIES} from 'lib/constants';
import {summariesBuildFn} from 'lib/index';

/**
 * Четыре плитки с общей статистикой на странице статистики обмена
 * @constructor
 */
class AdsStatsOverview extends React.Component {
    render() {
        const {summaries} = this.props;

        return (
            <StatsSummaryPanels
                summaries={summaries}
            />
        );
    }
}

const fields = ['ads_shows', 'ads_referrer', 'ctr', 'widget_load'];
const buildSummaries = summariesBuildFn(fields, TRAFFIC_STATS_SUMMARIES);

export default connectToStore(AdsStatsOverview, ({adsStats}) => ({
    summaries: buildSummaries(adsStats.statsSummaries)
}));

