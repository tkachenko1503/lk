import React from 'react';
import CreateWidget from 'components/CreateWidget';
import EscKeyClosable from 'components/EscKeyClosable';
import {closeAdsCampaignsAction, updateAdsCampaign} from 'modules/adsCampaigns/actions';

class AdsCampaignRenameAction extends EscKeyClosable {
    constructor(props) {
        super(props);

        this.update = this.update.bind(this);
        this.closeAction = closeAdsCampaignsAction;
    }

    getAttrs(form) {
        const name = form.widgetName && form.widgetName.trim();

        if (name) {
            return {
                name: name
            }
        }
    }

    serialize(form) {
        return Object.keys(form.elements)
            .reduce((obj, elem) => {
                if (form[elem]) {
                    obj[elem] = form[elem].value;
                }
                return obj;
            }, {});
    }

    update(e) {
        e.preventDefault();
        const form = this.serialize(e.target);
        const attrs = this.getAttrs(form);

        if (attrs) {
            updateAdsCampaign({
                adsCampaign: this.props.adsCampaign,
                attrs
            });
        }
    }

    render() {
        const {adsCampaign} = this.props;

        return (
            <CreateWidget
                submitAction={this.update}
                name={adsCampaign.get('name')}
                inputSize={7}
                buttonSize={5}
            />
        );
    }
}

export default AdsCampaignRenameAction;