import React from 'react';
import connectToStore from 'lib/connectToStore';
import Widget from 'components/Widget';
import WidgetsList from 'components/WidgetsList';
import {fetchWidgets, toggleRemoveDialog, removeWidget} from 'actions/widgets';
import {setAppContext} from 'modules/appContext/actions';
import filter from 'ramda/src/filter';

class WidgetsListContainer extends React.Component {
    constructor(props) {
        super(props);
        this.renderWidget = this.renderWidget.bind(this);
        this.removeWidget = this.removeWidget.bind(this);
    }

    componentWillMount() {
        fetchWidgets(this.props.currentSite);
        this.contextChangeHandler = setAppContext.map(fetchWidgets);
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
    }

    askRemoveWidget(widget) {
        toggleRemoveDialog({
            widget,
            open: true
        });
    }

    closeRemoveWidget() {
        toggleRemoveDialog({
            widget: null,
            open: false
        });
    }

    removeWidget() {
        const {widget} = this.props.removeDialog;
        
        removeWidget(widget);
        this.closeRemoveWidget()
    }
    
    renderWidget(widget) {
        const {action} = this.props;

        return (
            <Widget
                key={widget.id}
                widget={widget}
                action={action.id === widget.id ? action : null}
                askRemoveWidget={this.askRemoveWidget}
            />
        );
    }

    render() {
        const {widgets, removeDialog} = this.props;

        return (
            <WidgetsList
                widgets={widgets}
                removeDialog={removeDialog}
                renderWidget={this.renderWidget}
                removeWidget={this.removeWidget}
                closeRemoveWidget={this.closeRemoveWidget}
            />
        );
    }
}

export default connectToStore(WidgetsListContainer, ({app, widgets}) => ({
    currentSite: app.selectedContext,
    widgets: filter(w => w.get('format') === 'default', widgets.list),
    action: widgets.action,
    removeDialog: widgets.removeDialog
}));
