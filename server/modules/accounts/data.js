import Promise from 'bluebird';
import json2csv from '../../lib/json2csv';
import { getSiteAccountCsv, getAdvertiserAccountCsv } from './resource';

const SITE_CONTEXT = 'Site';
const ADVERTISER_CONTEXT = 'Advertiser';
const CSV_DOT_DELIMITER = '.';
const CSV_DEFAULT_VALUE = 0;

function formatCtr({ ctr = 0 }) {
    const ctrStringValue = String(ctr).replace('.', ',');

    return `${ctrStringValue}%`;
}

const REPORT_FIELDS = [
    { label: 'Вебсайт', value: 'website' },
    { label: 'Дата', value: 'date' },
    { label: 'Загрузки', value: 'load' },
    { label: 'Показы', value: 'impression' },
    { label: 'Клики', value: 'referrer' },
    { label: 'CTR', value: formatCtr }
];

function prepareHtmlData({ result }) {
    return {
        data: result,
        fields: REPORT_FIELDS
    };
}

export function getAccountCsvData(params) {
    let csvRequest;

    if (params.contextType === SITE_CONTEXT) {
        csvRequest = getSiteAccountCsv(params);
    } else if (params.contextType === ADVERTISER_CONTEXT) {
        // csvRequest = getAdvertiserAccountCsv(params);
        return Promise.reject(new Error('Not implemented yet'));
    }

    return Promise
            .resolve()
            .then(() => csvRequest);
}

export function getAccountHtmlData(params) {
    return getAccountCsvData(params)
            .then(prepareHtmlData);
}

export function makeCsvReport(csvData) {
    try {
        return json2csv({
            data: csvData,
            fields: REPORT_FIELDS,
            del: CSV_DOT_DELIMITER,
            defaultValue: CSV_DEFAULT_VALUE
        });
    } catch (error) {
        return Promise.reject(error);
    }
}
