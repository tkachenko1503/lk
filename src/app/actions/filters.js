import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import sampleOn from 'flyd/module/sampleon';
import pipe from 'ramda/src/pipe';
import curryN from 'ramda/src/curryN';
import actions$ from 'actions/index';
import {App, Filters} from 'actions/types';

export const setFilter = flyd.stream();
export const applyCurrentFilters = flyd.stream();

const isValidFilters = flyd.stream();
export const applyWhenFiltersValid = sampleOn(isValidFilters, applyCurrentFilters);

const applyFiltersWithValidation = curryN(2, Filters.ApplyFilters)(isValidFilters);


flyd.on(
    pipe(App.Filters, actions$),
    mergeAll([
        setFilter.map(Filters.SetCurrentFilter),
        applyCurrentFilters.map(applyFiltersWithValidation)
    ])
);

