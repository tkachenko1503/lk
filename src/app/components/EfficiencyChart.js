import React from 'react';
import classnames from 'classnames';

import Chart from './Chart';
import StatsChart from './StatsChart';
import { chartConfig } from '../modules/dashboard/chartConfigs';

import styles from 'styles/chart.css';

class EfficiencyChart extends StatsChart {
    isNeedRedrawChart(newProps){
        var _super = StatsChart.prototype.isNeedRedrawChart.call(this, newProps);
        return _super || this.props.first !== newProps.first;
    }

    render() {
        const {dateRange} = this.props;
        const withTime = this.isNeedShowTime(dateRange);
        const config = chartConfig(this.props, withTime);

        return (
            <div className={classnames(
                'earning-chart-space',
                styles.fixChartTooltipAlign
            )}>
                <Chart config={config} redraw={this.needRedraw} />
            </div>
        );
    }
}

export default EfficiencyChart;
