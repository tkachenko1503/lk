import React from 'react';
import classnames from 'classnames';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Table from 'react-bootstrap/lib/Table';
import WidgetRenameAction from 'components/WidgetRenameAction';
import WidgetEditViewAction from 'components/WidgetEditViewAction';
import WidgetDownloadAction from 'components/WidgetDownloadAction';
import {makeAction} from 'actions/widgets';

import styles from 'styles/widget.css';
import table from 'styles/widgetTable.css';

import {ACTIONS_MAP} from 'lib/constants';

class Widget extends React.Component {
    constructor(props) {
        super(props);

        this.makeAction = this.makeAction.bind(this);
        this.removeWidget = this.removeWidget.bind(this);
    }

    removeWidget() {
        const {askRemoveWidget, widget} = this.props;

        askRemoveWidget(widget);
    }

    makeAction(action) {
        makeAction({
            id: this.props.widget.id,
            name: action
        });
    }
    
    renderAction(action) {
        return (
            <NavItem
                className={styles.actionItem}
                key={action.key}
                eventKey={action.key}
            >
                {action.title}
            </NavItem>
        );
    }

    renderActionRow(action, activeAction) {
        if (action && action.name !== 'rename') {
            return (
                <tr>
                    <td colSpan={4} className={styles.actionPanel}>
                        {activeAction}
                    </td>
                </tr>
            );
        }
        return null;
    }

    renderActiveAction(actionName) {
        let actionForm;

        switch (actionName) {
            case 'rename':
                actionForm = <WidgetRenameAction widget={this.props.widget}/>;
                break;
            case 'editView':
                actionForm = <WidgetEditViewAction widget={this.props.widget} />;
                break;
            case 'download':
                actionForm = <WidgetDownloadAction widget={this.props.widget} />;
                break;
        }
        return actionForm;
    }

    renderNameIdNormal(name, id) {
        return [
            <td
                className={classnames(
                    styles.widgetName,
                    table.nameCol
                )}
                key="name"
            >
                <span alt={name}>
                    {name}
                </span>
            </td>,
            <td key="id" className={table.idCol}>
                {`ID_${id}`}
            </td>
        ];
    }

    renderRenameAction(action) {
        return (
            <td
                className={styles.noPadTop}
                colSpan={2}
            >
                {action}
            </td>
        );
    }

    render() {
        const {widget, action} = this.props;
        const {name, objectId} = widget.toJSON();

        const activeAction = action && objectId === action.id
            ? this.renderActiveAction(action.name)
            : null;

        return (
            <tr>
                <td colSpan={4} className={table.noPad}>
                    <Table className={styles.widgetInnerTable}>
                        <tbody>
                            <tr>
                                {action && action.name === 'rename'
                                    ? this.renderRenameAction(activeAction)
                                    : this.renderNameIdNormal(name, objectId)}

                                <td key="actions" className={table.actionsCol}>
                                    <Nav
                                        activeKey={action ? action.name : null}
                                        bsStyle="pills"
                                        onSelect={this.makeAction}
                                    >
                                        {ACTIONS_MAP.map(this.renderAction)}
                                    </Nav>
                                </td>

                                <td key="remove" className={table.removeCol}>
                                    <Button
                                        className={styles.removeBtn}
                                        onClick={this.removeWidget}
                                    >
                                        <Glyphicon glyph="trash"/>
                                    </Button>
                                </td>
                            </tr>
                            {this.renderActionRow(action, activeAction)}
                        </tbody>
                    </Table>
                </td>
            </tr>
        );
    }
}

export default Widget;
