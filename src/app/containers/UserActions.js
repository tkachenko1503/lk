import React from 'react';
import connectToStore from 'lib/connectToStore';
import UserMenu from 'components/UserMenu';
import {userLogout} from 'actions/user';

class UserActions extends React.Component {
    render() {
        const {user} = this.props;

        return (
            <UserMenu
                username={user.current ? user.current.getUsername() : ''}
                logOut={userLogout}
            />
        );
    }
}

export default connectToStore(UserActions, state => ({
    user: state.user
}));
