import { prepareRequestParams } from './utils';
import { getAccountCsvData, makeCsvReport } from './data';

function makeSendReportFn(response, reportName) {
    response.attachment(reportName);

    return report => response.end(report);
}

function makeReportName({ account }) {
    return `${account}.csv`;
}

function handleCsvDataResults(csvData) {
    if (csvData.error) {
        throw new Error(csvData.error);
    }
    return makeCsvReport(csvData.result);
}

export function getAccountCsvReport(request, response, next) {
    const params = prepareRequestParams(request);
    const reportName = makeReportName(params);
    const sendReport = makeSendReportFn(response, reportName);

    getAccountCsvData(params)
        .then(handleCsvDataResults)
        .then(sendReport)
        .catch(next);
}
