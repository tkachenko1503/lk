import React from 'react';
import WidgetAction from 'components/WidgetAction';
import Button from 'react-bootstrap/lib/Button';
import Label from 'react-bootstrap/lib/Label';

import styles from 'styles/widget.css';

class WidgetEditViewAction extends WidgetAction {
    getAttrs(form) {
        return {
            styles: form.styles
        }
    }

    render() {
        const {widget} = this.props;
        const widgetStyles = widget.get('styles') || `.ntvk#${widget.id} {\n\n}`;

        return (
            <form onSubmit={this.update}>
                <Label>
                    Редактор CSS
                </Label>
                <textarea
                    className={styles.viewEditor}
                    name="styles"
                    defaultValue={widgetStyles}
                    rows={6}
                />
                <Button
                    className={styles.viewEditorSubmit}
                    type="submit"
                >
                    Сохранить
                </Button>
            </form>
        );
    }
}

export default WidgetEditViewAction;
