import React from 'react';
import {Link} from 'react-router';
import classnames from 'classnames';
import UserActions from 'containers/UserActions';
import SitesSelect from 'containers/SitesSelect';
import {toggleSidebar} from 'actions/sidebar';

import header from 'styles/header.css';

/**
 * Хедер - самая верхняя панелька с выбором сайта логотипом и юзер акшенами
 * @constructor
 */
const Header = () => {
    return (
        <div className="header-section">
            <div className={classnames(
                "logo dark-logo-bg hidden-xs",
                header.whiteLogo
            )}>
                <Link to="/">
                    <img
                        className={header.logo}
                        src="/vendor/img/Natimatica-logo.png"
                        alt="Natimatica" />
                </Link>
            </div>

            <div className={classnames(
                "icon-logo dark-logo-bg hidden-xs hidden-sm",
                header.whiteLogo
            )}>
                <Link to="/">
                    <img
                        className={header.Nlogo}
                        src="/vendor/img/N-logo.png"
                        alt="Natimatica"
                    />
                </Link>
            </div>

            <a
                className="toggle-btn"
                onClick={toggleSidebar}
            >
                <i className="fa fa-outdent"/>
            </a>

            <SitesSelect />
            <UserActions />
        </div>
    );
};

export default Header;
