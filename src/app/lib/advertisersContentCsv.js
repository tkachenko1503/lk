import json2csv from 'lib/json2csv';
import {downloadCSVFile} from 'lib/download';

const ADS_CSV_FIELDS = [
    {label: 'Рекламодатель', value: 'name'},
    {label: 'Доход', value: 'profit'},
    {label: 'Цена клика', value: 'clickCost'},
    {label: 'CTR', value: v => `${v.ctr.toString().replace('.', ',')}%`}
];

function generateCSVData(rows, currentSite, currentWidget) {
    const siteAndWidget = [
        {label: 'Вебсайт', value: 'site', default: currentSite},
        {label: 'Виджет', value: 'widget', default: (currentWidget || 'All')}
    ];

    return json2csv({
        del: '.',
        defaultValue: 0,
        data: rows,
        fields: siteAndWidget.concat(ADS_CSV_FIELDS)
    });
}

export function downloadAdvertisersContentCSV({rows, datesRange, currentSite, currentWidget}) {
    generateCSVData(rows, currentSite, currentWidget)
        .then(data => downloadCSVFile({
            name: currentSite,
            range: datesRange,
            url: encodeURI("data:text/csv;charset=utf-8," + data),
            widget: currentWidget
        }));
}
