import React from 'react';
import connectToStore from 'lib/connectToStore';
import AdsCampaignsList from 'modules/adsCampaigns/AdsCampaignsList';
import AdsCampaignsDataContainer from 'modules/adsCampaigns/AdsCampaignsDataContainer';
import {closeAdsCampaignsAction, makeAction,
        toggleRemoveDialog, removeAdsCampaign} from 'modules/adsCampaigns/actions';

class AdsCampaignsListContainer extends React.Component {
    constructor(props) {
        super(props);

        this.removeAdsCampaign = this.removeAdsCampaign.bind(this);
    }

    removeAdsCampaign() {
        const {adsCampaign} = this.props.removeDialog;

        removeAdsCampaign(adsCampaign);
        this.closeRemoveAdsCampaign()
    }

    closeRemoveAdsCampaign() {
        toggleRemoveDialog({
            adsCampaign: null,
            open: false
        });
    }

    openRemoveDialog(adsCampaign) {
        toggleRemoveDialog({
            adsCampaign,
            open: true
        });
    }

    render() {
        const {adsCampaignsList, removeDialog, action} = this.props;

        return (
            <div>
                <AdsCampaignsDataContainer />
                <AdsCampaignsList
                    adsCampaignsList={adsCampaignsList}
                    removeDialog={removeDialog}
                    action={action}
                    makeAction={makeAction}
                    removeAdsCampaign={this.removeAdsCampaign}
                    closeRemoveAdsCampaign={this.closeRemoveAdsCampaign}
                    openRemoveDialog={this.openRemoveDialog}
                />
            </div>
        );
    }
}

export default connectToStore(AdsCampaignsListContainer, ({adsCampaigns, app}) => ({
    adsCampaignsList: adsCampaigns.adsCampaignsList,
    removeDialog: adsCampaigns.removeDialog,
    action: adsCampaigns.action
}));
