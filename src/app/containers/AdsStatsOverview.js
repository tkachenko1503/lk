import React from 'react';
import connectToStore from 'lib/connectToStore';
import StatsSummaryPanels from 'components/StatsSummaryPanels';
import {ADS_STATS_SUMMARIES} from 'lib/constants';
import {summariesBuildFn} from 'lib/index';

/**
 * Четыре плитки с общей статистикой на странице статистики по рекламе
 * @constructor
 */
class AdsStatsOverview extends React.Component {
    render() {
        const {summaries} = this.props;

        return (
            <StatsSummaryPanels
                summaries={summaries}
            />
        );
    }
}

const fields = ['ads_shows', 'ads_referrer', 'ads_profit_mean', 'ads_profit'];
const buildSummaries = summariesBuildFn(fields, ADS_STATS_SUMMARIES);

export default connectToStore(AdsStatsOverview, ({adsStats}) => ({
    summaries: buildSummaries(adsStats.statsSummaries)
}));

