import React from 'react';
import Layout from 'components/Layout';
import Panel from 'react-bootstrap/lib/Panel';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import Button from 'react-bootstrap/lib/Button';
import Dropzone from 'react-dropzone';
import {If, Then, Else} from 'react-if';

import panel from 'styles/panel.css';
import advertisersImport from 'modules/advertisers/advertisersImport.css';

class AdvertisersStatsImport extends React.Component {
    constructor(props) {
        super(props);

        this.openFileBrowser = this.openFileBrowser.bind(this);
    }

    openFileBrowser() {
        this.refs.fileInput.open();
    }

    render() {
        const {file, chooseFile, uploadFile, campaign, campaignName,
            isAdvertiserContext, adsCampaignsList, chooseAdsCampaign} = this.props;
        const panelHeader = (<h4>Импорт статистики для рекламодателей</h4>);

        return (
            <Layout>
                <div className="advertisers-stats-import-container">
                    <div className="wrapper">
                        <Panel
                            header={panelHeader}
                            className={panel.fixedHeader}
                        >
                            <If condition={Boolean(isAdvertiserContext)}>
                                <Then>
                                    <div>
                                        <Dropzone
                                            ref="fileInput"
                                            onDrop={chooseFile}
                                            multiple={false}
                                            accept="text/csv"
                                            className={advertisersImport.dropzone}
                                        >
                                            <div>Перетащите файл сюда</div>
                                        </Dropzone>

                                        <div className={advertisersImport.manual}>
                                            <span className={advertisersImport.or}>или</span>

                                            <Button
                                                className={advertisersImport.fileChooseBtn}
                                                onClick={this.openFileBrowser}
                                            >
                                                Выберите файл
                                            </Button>
                                        </div>

                                        <div className={advertisersImport.campaign}>
                                            <span className={advertisersImport.campaignText}>
                                                Выберите кампанию
                                            </span>

                                            <DropdownButton
                                                id="adsCampaign-select"
                                                title={campaign ? campaignName : '--'}
                                            >
                                                {adsCampaignsList.length
                                                    ? adsCampaignsList.map(adsCampaign => (
                                                    <MenuItem
                                                        key={adsCampaign.id}
                                                        active={(adsCampaign.id === campaign)}
                                                        eventKey={adsCampaign.id}
                                                        onSelect={chooseAdsCampaign}
                                                    >
                                                        {adsCampaign.get('name')}
                                                    </MenuItem>
                                                ))
                                                    : <div>Нет доступных кампаний</div>
                                                }
                                            </DropdownButton>
                                        </div>

                                        <If condition={Boolean(file)}>
                                            <Then>
                                                <div className={advertisersImport.file}>
                                                    <span className={advertisersImport.fileName}>
                                                        {file ? file.name : null}
                                                    </span>
                                                    <Button
                                                        onClick={uploadFile}
                                                        disabled={campaign ? false : true}
                                                    >
                                                        Загрузить файл
                                                    </Button>
                                                </div>
                                            </Then>
                                        </If>
                                    </div>
                                </Then>
                                <Else>
                                    <div>Выберите рекламодателя.</div>
                                </Else>
                            </If>
                        </Panel>
                    </div>
                </div>
            </Layout>
        );
    }
}

export default AdvertisersStatsImport;
