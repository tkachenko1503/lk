import 'lib/setup';
import React from 'react';
import ReactDOM from 'react-dom';
import {browserHistory} from 'react-router';
import RouterContainer from 'containers/RouterContainer';
import routes from './routes';
import Connect from 'lib/Connect';
import {store$} from './store';
import {scrollTopPage} from 'lib/index';
import {trackPage} from 'lib/analitics';

const routerUpdateHandlers = [trackPage, scrollTopPage];

document.addEventListener('DOMContentLoaded', () => {
    const root = document.querySelector("#nativkaApp");
    ReactDOM.render(
        (
            <Connect store$={store$}>
                <RouterContainer
                    history={browserHistory}
                    updateHandlers={routerUpdateHandlers}
                >
                    {routes}
                </RouterContainer>
            </Connect>
        ),
        root
    );
});
