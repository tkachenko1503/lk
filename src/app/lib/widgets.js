import map from 'ramda/src/map';
import without from 'ramda/src/without';

/**
 * Хелперы для работы с виджетами
 */

import {downloadTxtFile} from 'lib/download'


/**
 * Возвращает имя и код виджета для экспорта в виде текстового файла
 *
 * @param {Object} widget
 * @returns {string}
 */
function getWidgetExportCode(widget) {
    var name = getWidgetExportName(widget);
    var source = getWidgetSourceCode(widget);
    var padding = ' '.repeat(4);

    return `${name}\n\n${padding}${source}`;
}

/**
 * Возвращает имя виджета для экспорта
 *
 * @param {Object} widget
 * @returns {String}
 */
function getWidgetExportName(widget) {
    return `Widget_ID_${widget.id}`;
}


/**
 * Возвращает код виджета
 *
 * @param {Object} widget
 * @returns {String}
 */
export function getWidgetSourceCode(widget) {
    return `<ntvk id="${widget.id}"></ntvk>`;
}

/**
 * Возвращает имена и коды виджетов для экспорта в виде текстового файла
 *
 * @param {Object[]} widgetsList
 * @returns {String}
 */
export function getWidgetsExportCodes(widgetsList) {
    var codes = widgetsList.map(getWidgetExportCode);

    return codes.join('\n\n\n');
}

/**
 * Скачивание исходников для виджетов
 *
 * @param {Object[]} widgetsList
 */
export function downloadWidgetsExportCodes({widgetsList}) {
    var filename = 'Коды_виджетов.txt';
    var content = getWidgetsExportCodes(widgetsList);

    downloadTxtFile({
        name: filename,
        txt: content
    });
}

const widgetsInTest = map(t => t.get('Widget'));

export function getAvailableWidgets(testItems, widgetsList) {
    return without(widgetsInTest(testItems), widgetsList)
}
