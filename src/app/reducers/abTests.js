import curryN from 'ramda/src/curryN';
import findIndex from 'ramda/src/findIndex';
import propEq from 'ramda/src/propEq';
import uniq from 'ramda/src/uniq';
import {ABTests} from 'actions/types';

const action = {
    id: null,
    name: null
};
const newTestDefault = () => ({
    name: null,
    items: [
        {widgetId: null, frequency: 0},
        {widgetId: null, frequency: 0}
    ],
    canSaveTest: false,
    error: null
});

export function defaultABTests() {
    return {
        newTest: newTestDefault(),
        action,
        list: [],
        testItems: {},
        testItemsBackup: {},
        removeDialog: {
            open: false,
            test: null
        }
    };
}

const reducefn = ABTests.caseOn({
    MakeABTestsAction: (action, state) => ({
        ...state,
        action
    }),
    ResetABTestsAction: (testId, state) => {
        const newState = {
            ...state,
            newTest: newTestDefault(),
            action: {
                id: null,
                name: null
            }
        };
        
        return newState;
    },
    CancelABTestsChanges: ({testId, testItems}, state) => ({
        ...state,
        testItems: {
            ...state.testItems,
            [testId]: [
                ...testItems,
                ...(state.testItemsBackup[testId] || [])
            ]
        },
        testItemsBackup: {
            ...state.testItemsBackup,
            [testId]: []
        }
    }),
    SetTestName: (name, state) => ({
        ...state,
        newTest: {
            ...state.newTest,
            error: null,
            name
        }
    }),
    SetWidgetId: ({index, widgetId}, state) => {
        const {newTest} = state;
        const selectedIndex = findIndex(propEq('widgetId', widgetId), newTest.items);
        let newListWithoutSelected;

        // обнуляем айдишник чтобы исключить дубликаты
        if (selectedIndex >= 0) {
            newListWithoutSelected = [
                ...newTest.items.slice(0, selectedIndex),
                {widgetId: null, frequency: newTest.items[selectedIndex].frequency},
                ...newTest.items.slice(selectedIndex + 1)
            ];
        } else {
            newListWithoutSelected = newTest.items;
        }

        return {
            ...state,
            newTest: {
                ...newTest,
                error: null,
                items: [
                    ...newListWithoutSelected.slice(0, index),
                    {widgetId, frequency: newListWithoutSelected[index].frequency},
                    ...newListWithoutSelected.slice(index + 1)
                ]
            }
        };
    },
    SetWidgetFrequency: ({index, frequency}, state) => {
        const {newTest} = state;
        return {
            ...state,
            newTest: {
                ...newTest,
                error: null,
                canSaveTest: true,
                items: [
                    ...newTest.items.slice(0, index),
                    {widgetId: newTest.items[index].widgetId, frequency: frequency},
                    ...newTest.items.slice(index + 1)
                ]
            }
        };
    },
    AddTestItem: (state) => {
        const {newTest} = state;
        return {
            ...state,
            newTest: {
                ...newTest,
                error: null,
                items: [
                    ...newTest.items,
                    {widgetId: null, frequency: 0}
                ]
            }
        };
    },
    RemoveTestItem: (index, state) => {
        const {newTest} = state;
        return {
            ...state,
            newTest: {
                ...newTest,
                error: null,
                items: [
                    ...newTest.items.slice(0, index),
                    ...newTest.items.slice(index + 1)
                ]
            }
        };
    },
    SaveTest: (abTest, state) => {
        const {list} = state;
        return {
            ...state,
            newTest: newTestDefault(),
            action: {
                id: null,
                name: null
            },
            list: [
                ...list,
                abTest
            ]
        };
    },
    SetError: (error, state) => ({
        ...state,
        newTest: {
            ...state.newTest,
            error
        }
    }),
    SetAvailableTests: (abTests, state) => {
        return {
            ...state,
            list: [
                ...abTests
            ]
        };
    },
    UpdateTest: (abTest, state) => {
        const {list} = state;
        const updatedTestIndex = findIndex(propEq('id', abTest.id), list);

        return {
            ...state,
            action: {
                id: null,
                name: null
            },
            list: [
                ...list.slice(0, updatedTestIndex),
                abTest,
                ...list.slice(updatedTestIndex + 1)
            ]
        };
    },
    RemoveTest: (abTest, state) => {
        const {list} = state;
        const updatedTestIndex = findIndex(propEq('id', abTest.id), list);

        return {
            ...state,
            action: {
                id: null,
                name: null
            },
            list: [
                ...list.slice(0, updatedTestIndex),
                ...list.slice(updatedTestIndex + 1)
            ]
        };
    },
    SetTestItems: (testId, testItems = [], state) => {
        const oldItems = state.testItems[testId] || [];
        const mergedItems = uniq([...oldItems, ...testItems]);

        return{
            ...state,
            testItems: {
                ...state.testItems,
                [testId]: [...mergedItems]
            }
        };
    },
    UpdateState: (state) => ({
        ...state
    }),
    AddItemToExistenTest: (testId, testItem, state) => ({
        ...state,
        testItems: {
            ...state.testItems,
            [testId]: [
                ...state.testItems[testId],
                testItem
            ]
        }
    }),
    RemoveTestItemFromExistedTest: (testId, testItem, state) => {
        const items = state.testItems[testId];
        const index = items.indexOf(testItem);

        return {
            ...state,
            testItems: {
                ...state.testItems,
                [testId]: [
                    ...items.slice(0, index),
                    ...items.slice(index + 1)
                ]
            }
        }
    },
    RemoveAndBackupTestItemFromExistedTest: (testId, testItem, state) => {
        const items = state.testItems[testId];
        const index = items.indexOf(testItem);

        return {
            ...state,
            testItems: {
                ...state.testItems,
                [testId]: [
                    ...items.slice(0, index),
                    ...items.slice(index + 1)
                ]
            },
            testItemsBackup: {
                ...state.testItemsBackup,
                [testId]: [
                    ...(state.testItemsBackup[testId] || []),
                    items[index]
                ]
            }
        }
    },
    ToggleRemoveDialog: (dialog, state) => ({
        ...state,
        removeDialog: dialog
    })
});

export default curryN(2, reducefn);
