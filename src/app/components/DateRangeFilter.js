import React from 'react';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import Button from 'react-bootstrap/lib/Button';

import styles from 'styles/dashboardPageHead.css';

const availableFilters = {
    day: 'Сегодня',
    yesterday: 'Вчера',
    week: 'Неделя',
    month: 'Месяц',
    quarter: 'Квартал',
    year: 'Год'
};
const filtersKeys = Object.keys(availableFilters);

const DateRangeFilter = (props) => {
    return (
        <ButtonGroup className={styles.dateButtonGroup} bsSize="small">
            {filtersKeys.map(filterButton, props)}
        </ButtonGroup>
    );
};

export default DateRangeFilter;

function filterButton(buttonKey) {
    const isActive = buttonKey === this.currentFilter;

    return (
        <Button
            data-filter-id={buttonKey}
            key={buttonKey}
            onClick={this.setFilter}
            disabled={isActive ? true : false}
        >
            {availableFilters[buttonKey]}
        </Button>
    );
}
