import Type from 'union-type';
import flyd from 'flyd';

// Экшены для сайдбара с навигацией
export const Sidebar = Type({
    ToggleSidebar: [],
    ToggleNavMenu: {
        navId: String
    }
});

// Экшены для селекта с сайтами в хедере
export const Sites = Type({
    RequestSites: [],
    SetSites: {
        sites: Array
    },
    ChangeSite: {
        site: String
    }
});

// Экшены для работы со статистикой на дашборде
export const Stats = Type({
    RequestStats: {
        params: Object
    },
    SetStats: {
        stats: Object
    },
    DownloadCSV: {
        params: Object
    }
});

// Экшены для всех фильтров в хедере
export const Filters = Type({
    SetCurrentFilter: {
        filter: Object
    },
    ApplyFilters: {
        validationStream: flyd.isStream,
        params: Object
    }
});

// Экшены для дропдауна с пользовательскими действиями
// в хедере справа
export const User = Type({
    ResetUser: [],
    LogIn: {
        user: Object
    },
    SetAdminRights: {
        rights: Object
    }
});

// Экшены для работы с виджетами
export const Widgets = Type({
    RemoveWidget: {
        id: String
    },
    MakeWidgetAction: {
        action: Object
    },
    SaveWidget: {
        widget: Object
    },
    SetWidgets: {
        widgets: Array
    },
    UpdateWidget: {
        widget: Object
    },
    UpdateAndSaveWidget: {
        widget: Object
    },
    ResetWidgetAction: [],
    SetWidgetMeta: {
        meta: Object
    },
    SetWidgetsAdsSavable: [],
    ToggleRemoveDialog: {
        dialogSettings: Object
    }
});

export const AdsCampaigns = Type({
    RemoveAdsCampaign: {
        id: String
    },
    MakeAdsCampaignsAction: {
        action: Object
    },
    SaveAdsCampaign: {
        adsCampaign: Object
    },
    ResetAdsCampaigns: {
        adsCampaigns: Array
    },
    UpdateAdsCampaign: {
        adsCampaign: Object
    },
    ResetAdsCampaignsAction: [],
    ToggleRemoveDialog: {
        dialogSettings: Object
    }
});

export const AdsChartControls = Type({
    SetParam: {
        paramId: String,
        buttonId: String
    }
});

export const AdsStats = Type({
    SetAdsStats: {
        stats: Object
    },
    RequestAdsStats: {
        params: Object
    },
    DownloadAdsCSV: {
        params: Object
    }
});

export const AdsStatsControls = Type({
    SetControlValue: {
        stats: Object
    }
});

export const AdsPayments = Type({
    SetAdsPayments: [Object],
    RequestAdsPayments: [Object]
});

export const TrafficChartControls = Type({
    SetParams: {
        first: String,
        second: String
    }
});

export const ABTests = Type({
    MakeABTestsAction: {
        action: Object
    },
    SetTestName: {
        testName: String
    },
    SetWidgetId: {
        testItem: Object
    },
    SetWidgetFrequency: {
        testItem: Object
    },
    AddTestItem: [],
    RemoveTestItem: {
        index: Number
    },
    SaveTest: {
        abTest: Object
    },
    SetAvailableTests: {
        abTests: Array
    },
    UpdateTest: {
        abTest: Object
    },
    RemoveTest: {
        abTest: Object
    },
    SetTestItems: {
        testId: String,
        testItems: Array
    },
    ResetABTestsAction: {
        testId: String
    },
    UpdateState: [],
    SetError: {
        error: Object
    },
    AddItemToExistenTest: {
        testId: String,
        item: Object
    },
    CancelABTestsChanges: {
        params: Object
    },
    RemoveTestItemFromExistedTest: {
        testId: String,
        item: Object
    },
    RemoveAndBackupTestItemFromExistedTest: {
        testId: String,
        item: Object
    },
    ToggleRemoveDialog: {
        dialogSettings: Object
    }
});

export const Articles = Type({
    SetArticles: {
        articles: Array
    },
    SetControlValue: {
        stats: Object
    },
    RequestArticlesStats: {
        params: Object
    }
});

export const Advertisers = Type({
    SetStatsFile: {
        file: Object
    },
    SetAdsCampaign: {
        AdsCampaignId: String
    },
    ClearStatsFile: [],
    SetAdvertisersContent: {
        content: Array
    },
    SetContentTableControl: {
        control: Object
    }
});

export const AppContext = Type({
    ResetContext: {
        context: Array
    },
    SetSelectedContext: {
        context: String
    },
    SetSelectedContextType: {
        type: String
    },
    SetInited: []
});

// Общий аггрегирующий тип для удобства
export const App = Type({
    Sites: [Sites],
    Stats: [Stats],
    Filters: [Filters],
    Sidebar: [Sidebar],
    User: [User],
    Widgets: [Widgets],
    AdsChartControls: [AdsChartControls],
    AdsStats: [AdsStats],
    AdsStatsControls: [AdsStatsControls],
    AdsPayments: [AdsPayments],
    TrafficChartControls: [TrafficChartControls],
    ABTests: [ABTests],
    Articles: [Articles],
    Advertisers: [Advertisers],
    AppContext: [AppContext],
    AdsCampaigns: [AdsCampaigns],
    'ResetAll!': []
});
