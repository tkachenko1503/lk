import React from 'react';
import Panel from 'react-bootstrap/lib/Panel';
import AdvertisersContentTableContainer from 'modules/advertisers/AdvertisersContentTableContainer';
import Layout from 'components/Layout';
import DashboardFilters from 'containers/DashboardFilters';

import panel from 'styles/panel.css';

const AdvertisersContent = (props) => {
    const panelHeader = (<h4>Рекламодатели</h4>);

    return (
        <Layout>
            <div className="advertisers-content">
                <DashboardFilters actionForApplyFilters={props.actionForApplyFilters}
                                  widgetSelectWithIcon={true}
                                  allWidgetTypes={true} />
                <div className="wrapper">
                    <Panel header={panelHeader}
                           className={panel.fixedHeader}>
                        <AdvertisersContentTableContainer />
                    </Panel>
                </div>
            </div>
        </Layout>
    );
};

export default AdvertisersContent;
