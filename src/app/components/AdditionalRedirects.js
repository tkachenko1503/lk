import React from 'react';
import classnames from 'classnames';
import Panel from 'react-bootstrap/lib/Panel';

import EfficiencyChart from './EfficiencyChart';

import styles from 'styles/dashboard.css';

/**
 * Панель с граффиком по переходам и CTR
 * @param props
 * @constructor
 */
const AdditionalRedirects = (props) => {
    const {ctr, load, created} = props.columns;
    const panelHeader = (
        <div>Загрузки виджета/CTR</div>
    );

    return (
        <Panel
            header={panelHeader}
            bsClass={classnames(
                styles.chartPanel,
                "panel"
            )}
        >
            {ctr.length && load.length
                ? <EfficiencyChart
                    first="Загрузки виджета"
                    second="CTR"
                    secondTickFormat={(n) => (n.toFixed(1))}
                    col1={load}
                    col2={ctr}
                    created={created}
                    dateRange={props.dateRange}
                />
                : null}
        </Panel>
    );
};

export default AdditionalRedirects;
