import Parse from 'parse';
import ReactGA from 'react-ga';

const port = window.location.port;
const url = window.location.protocol + '//' + window.location.hostname;

Parse.initialize('NATIVKA_APP', 'NATIVKA_JS');
Parse.serverURL = (port ? url + ':' + port : url) + '/parse';


ReactGA.initialize('UA-70313012-2');
