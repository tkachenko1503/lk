import React from 'react';
import connectToStore from '../lib/connectToStore';
import Dashboard from '../components/Dashboard';
import { requestStatsForSite, downloadCSV, downloadStatisticsPdf } from '../actions/stats';
import { checkIsAdmin } from '../actions/user';
import { setAppContext } from '../modules/appContext/actions';
import { findByProp } from '../lib/index';

class Stats extends React.Component {
    constructor(props) {
        super(props);
        this.loadCSV = this.loadCSV.bind(this);
        this.loadStatisticsPdf = this.loadStatisticsPdf.bind(this);
        this.requestStats = this.requestStats.bind(this);
    }

    componentDidMount() {
        const { selectedContext } = this.props.app;

        if (selectedContext) {
            this.requestStats(selectedContext);
        }

        checkIsAdmin(true);
        this.contextChangeHandler = setAppContext.map((s) =>
            setTimeout(() => this.requestStats(s), 0));
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
    }

    getSiteById(siteId) {
        return findByProp(siteId, this.props.app.context, 'id');
    }

    requestStats(siteId) {
        const site = this.getSiteById(siteId);

        if (site) {
            requestStatsForSite({
                site: site.className === 'Site' ? site.get('name') : site.id,
                filters: this.props.filters,
                contextType: site.className
            });
        }
    }

    loadCSV(e) {
        const type = e.target.dataset.value;
        const site = this.getSiteById(this.props.app.selectedContext);

        downloadCSV({
            site: site.className === 'Site' ? site.get('name') : site.id,
            filters: this.props.filters,
            type,
            widgetList: this.props.widgetList,
            campaignList: this.props.campaignList,
            contextType: site.className,
            context: this.props.app.context
        });
    }

    loadStatisticsPdf() {
        const site = this.getSiteById(this.props.app.selectedContext);

        downloadStatisticsPdf({
            site: site.className === 'Site' ? site.get('name') : site.id,
            filters: this.props.filters,
            contextType: site.className
        });
    }

    render() {
        const { stats, filters, app, isAdmin } = this.props;

        return (
            <Dashboard
                stats={stats}
                filters={filters}
                loadCSV={this.loadCSV}
                loadStatisticsPdf={this.loadStatisticsPdf}
                actionForApplyFilters={requestStatsForSite}
                contextType={app.contextType}
                isAdmin={isAdmin}
            />
        );
    }
}

export default connectToStore(Stats, state => ({
    app: state.app,
    stats: state.stats,
    filters: state.filters,
    widgetList: state.widgets.list,
    campaignList: state.adsCampaigns.adsCampaignsList,
    isAdmin: state.user.isAdmin
}));
