import React from 'react';
import Layout from 'components/Layout';
import Panel from 'react-bootstrap/lib/Panel';
import Button from 'react-bootstrap/lib/Button';
import {If, Then, Else} from 'react-if';
import AdsCampaignCreateFormContainer from 'modules/adsCampaigns/AdsCampaignCreateFormContainer';
import AdsCampaignsListContainer from 'modules/adsCampaigns/AdsCampaignsListContainer';

import panel from 'styles/panel.css';

class AdsCampaigns extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {isAdvertiserContext, createAdsCampaign, adsCampaignsAction} = this.props;
        const panelHeader = (<h4>Настройка рекламных кампаний</h4>);

        return (
            <Layout>
                <div className="advertisers-ads-campaigns">
                    <div className="wrapper">
                        <Panel
                            header={panelHeader}
                            className={panel.fixedHeader}
                        >
                            <If condition={Boolean(isAdvertiserContext)}>
                                <Then>
                                    <div>
                                        <Button onClick={createAdsCampaign}>
                                            Добавить рекламную кампанию
                                        </Button>

                                        <If condition={adsCampaignsAction.name === 'create'}>
                                            <Then>
                                                <AdsCampaignCreateFormContainer />
                                            </Then>
                                        </If>

                                        <AdsCampaignsListContainer />
                                    </div>
                                </Then>
                                <Else>
                                    <div>Выберите рекламодателя.</div>
                                </Else>
                            </If>
                        </Panel>
                    </div>
                </div>
            </Layout>
        );
    }
}

export default AdsCampaigns;
