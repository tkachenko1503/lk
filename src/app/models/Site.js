import Parse from 'parse';
import BaseModel from 'models/Base';

const className = 'Site';

class Site extends BaseModel {
    constructor(attrs) {
        super(className);

        if (attrs) {
            this.set(attrs);
        }
    }

    static getAvailableSites() {
        const query = new Parse.Query(Site);
        return query.find();
    }
}

export default Site;

Parse.Object.registerSubclass('Site', Site);
