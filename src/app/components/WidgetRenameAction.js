import React from 'react';
import WidgetAction from 'components/WidgetAction';
import CreateWidget from 'components/CreateWidget';

class widgetRenameAction extends WidgetAction {
    getAttrs(form) {
        const name = form.widgetName && form.widgetName.trim();

        if (name) {
            return {
                name: name
            }
        }
    }

    render() {
        const {widget} = this.props;

        return (
            <CreateWidget
                submitAction={this.update}
                name={widget.get('name')}
                inputSize={7}
                buttonSize={5}
            />
        );
    }
}

export default widgetRenameAction;
