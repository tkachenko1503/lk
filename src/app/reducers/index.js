import evolve from 'ramda/src/evolve';
import sitesReducer from 'reducers/sites';
import statsReducer from 'reducers/stats';
import filtersReducer from 'reducers/filters';
import sidebarReducer from 'reducers/sidebar';
import userReducer from 'reducers/user';
import widgetsReducer from 'reducers/widgets';
import adsChartControls from 'reducers/adsChartControls';
import adsStatsReducer from 'reducers/adsStats';
import adsStatsControlsReducer from 'reducers/adsStatsControls';
import adsPaymentsReducer from 'reducers/adsPayments';
import trafficChartControlsReducer from 'reducers/trafficChartControls';
import abTestsReducer from 'reducers/abTests';
import articlesReducer from 'reducers/articles';
import advertisersReducer from 'modules/advertisers/reducer';
import appContextReducer from 'modules/appContext/reducer';
import adsCampaignsReducer from 'modules/adsCampaigns/reducer';

import {defaultStats} from 'reducers/stats';
import {defaultFilters} from 'reducers/filters';
import {defaultSidebar} from 'reducers/sidebar';
import {defaultUser} from 'reducers/user';
import {defaultSites} from 'reducers/sites';
import {defaultWidgets} from 'reducers/widgets';
import {defaultAdsChartControls} from 'reducers/adsChartControls';
import {defaultAdsStats} from 'reducers/adsStats';
import {defaultAdsStatsControls} from 'reducers/adsStatsControls';
import {defaultAdsPayments} from 'reducers/adsPayments';
import {defaultTrafficChartControls} from 'reducers/trafficChartControls';
import {defaultABTests} from 'reducers/abTests';
import {defaultArticles} from 'reducers/articles';
import {defaultAdvertisers} from 'modules/advertisers/reducer';
import {defaultContext} from 'modules/appContext/reducer';
import {defaultAdsCampaigns} from 'modules/adsCampaigns/reducer';

import {App} from 'actions/types';

export function initialState() {
    return {
        sidebar: defaultSidebar(),
        sites: defaultSites(),
        stats: defaultStats(),
        filters: defaultFilters(),
        user: defaultUser(),
        widgets: defaultWidgets(),
        adsChartControls: defaultAdsChartControls(),
        adsStats: defaultAdsStats(),
        adsStatsControls: defaultAdsStatsControls(),
        adsPayments: defaultAdsPayments(),
        trafficChartControls: defaultTrafficChartControls(),
        abTests: defaultABTests(),
        articles: defaultArticles(),
        advertisers: defaultAdvertisers(),
        app: defaultContext(),
        adsCampaigns: defaultAdsCampaigns()
    };
}

export default App.caseOn({
    Sites: (action, state) => evolve({sites: sitesReducer(action)}, state),
    Stats: (action, state) => evolve({stats: statsReducer(action)}, state),
    Filters: (action, state) => evolve({filters: filtersReducer(action)}, state),
    Sidebar: (action, state) => evolve({sidebar: sidebarReducer(action)}, state),
    User: (action, state) => evolve({user: userReducer(action)}, state),
    Widgets: (action, state) => evolve({widgets: widgetsReducer(action)}, state),
    AdsChartControls: (action, state) => evolve({adsChartControls: adsChartControls(action)}, state),
    AdsStats: (action, state) => evolve({adsStats: adsStatsReducer(action)}, state),
    AdsStatsControls: (action, state) => evolve({adsStatsControls: adsStatsControlsReducer(action)}, state),
    AdsPayments: (action, state) => evolve({adsPayments: adsPaymentsReducer(action)}, state),
    TrafficChartControls: (action, state) => evolve({trafficChartControls: trafficChartControlsReducer(action)}, state),
    ABTests: (action, state) => evolve({abTests: abTestsReducer(action)}, state),
    Articles: (action, state) => evolve({articles: articlesReducer(action)}, state),
    Advertisers: (action, state) => evolve({advertisers: advertisersReducer(action)}, state),
    AppContext: (action, state) => evolve({app: appContextReducer(action)}, state),
    AdsCampaigns: (action, state) => evolve({adsCampaigns: adsCampaignsReducer(action)}, state),
    'ResetAll!': (state) => initialState()
});
