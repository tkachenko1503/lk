import React from 'react';
import connectToStore from 'lib/connectToStore';
import StatsSummaryPanels from 'components/StatsSummaryPanels';
import {DASHBOARD_SITE_STATS_SUMMARIES, DASHBOARD_ADVERTISER_STATS_SUMMARIES} from 'lib/constants';
import {summariesBuildFn} from 'lib/index';

/**
 * Четыре плитки с общей статистикой на странице дашборда
 * @constructor
 */
class DashboardOverview extends React.Component {
    render() {
        const {summaries} = this.props;

        return (
            <StatsSummaryPanels
                summaries={summaries}
            />
        );
    }
}

const fields = ['shows', 'referrer', 'deepView', 'ctr'];

export default connectToStore(DashboardOverview, ({stats, app}) => ({
    summaries: summariesBuildFn(fields, getSummariesByContextType(app.contextType))(stats.overview)
}));

function getSummariesByContextType(contextType) {
    return contextType === 'Site' ? DASHBOARD_SITE_STATS_SUMMARIES : DASHBOARD_ADVERTISER_STATS_SUMMARIES;
}