import React from 'react';
import classNames from 'classnames';
import Panel from 'react-bootstrap/lib/Panel';
import Pagination from 'react-bootstrap/lib/Pagination';
import Button from 'react-bootstrap/lib/Button';
import Input from 'react-bootstrap/lib/Input';
import StatsTable from 'components/StatsTable';

import styles from 'styles/panel.css';
import adsStatsTable from 'styles/adsStatsTable.css';

const HEADERS = [
    {id: 'widget_name', title: 'Рекламный формат'},
    {id: 'created', title: 'Дата'},
    {id: 'widget_load', title: 'Загрузки рекламы'},
    {id: 'ads_shows', title: 'Показы объявлений'},
    {id: 'ads_referrer', title: 'Переходы по объявлениям'},
    {id: 'ads_profit', title: 'Доходы за день руб.'},
    {id: 'ads_profit_thousand_shows', title: 'Доход на тысячу показов руб.'},
    {id: 'ads_profit_mean', title: 'Средняя цена клика руб.'}
];

/**
 * Панель с таблицей статистики по рекламе
 * @constructor
 */
const AdsStatsTablePanel = (props) => {
    return (
        <Panel
            bsClass={classNames(
                styles.fixedHeader,
                "panel"
            )}
        >
            <ExportAdsPanel
                exportTableInCsv={props.exportTableInCsv}
            />

            <StatsTable
                rows={props.rows}
                rowsSummary={props.rowsSummary}
                tableSort={props.tableSort}
                datesRange={props.datesRange}
                headers={HEADERS}
                setTableSort={props.setTableSort}
            />

            <AdsTablePagerAndSize
                pagerSize={props.pagerSize}
                activePage={props.activePage}
                currentSize={props.currentSize}
                setTableSize={props.setTableSize}
                setTablePage={props.setTablePage}
            />
        </Panel>
    );
};

export default AdsStatsTablePanel;

const ExportAdsPanel = ({exportTableInCsv}) => {
    return (
        <div className={adsStatsTable.csvExportPanel}>
            <Button onClick={exportTableInCsv}>
                в CSV
            </Button>
        </div>
    );
};

const OPTIONS = [10, 20, 50];

const AdsTablePagerAndSize = (props) => {
    const needArrows = props.pagerSize > 4;

    return (
        <div className={adsStatsTable.tableSizePanel}>
            <div className={adsStatsTable.tableSizeLabel}>
                Размер таблицы
            </div>

            <div className={adsStatsTable.tableSize}>
                <Input
                    className={adsStatsTable.tableSizeSelect}
                    type="select"
                    onChange={props.setTableSize}
                    defaultValue={props.currentSize}
                >
                    {OPTIONS.map(SizeOption)}
                </Input>
            </div>

            {props.pagerSize
                ? (
                    <div className={adsStatsTable.tablePager}>
                        <Pagination
                            first={needArrows}
                            last={needArrows}
                            maxButtons={4}
                            items={props.pagerSize}
                            activePage={props.activePage}
                            onSelect={props.setTablePage}
                        />
                    </div>
                )
                : null
            }
        </div>
    );
};

const SizeOption = (option, i) => (
    <option
        key={i}
        value={option}
    >
        {option}
    </option>
);
