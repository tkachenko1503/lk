import curryN from 'ramda/src/curryN';
import findIndex from 'ramda/src/findIndex';
import propEq from 'ramda/src/propEq';
import {AdsCampaigns} from 'actions/types';

const action = {
    id: null,
    name: null
};

const removeDialog = {
    open: false,
    adsCampaign: null
};

export function defaultAdsCampaigns() {
    return {
        action,
        removeDialog,
        adsCampaignsList: []
    };
}

const reducefn = AdsCampaigns.caseOn({
    MakeAdsCampaignsAction: (action, state) => ({
        ...state,
        action
    }),
    SaveAdsCampaign: (adsCampaign, state) => ({
        ...state,
        adsCampaignsList: [
            ...state.adsCampaignsList,
            adsCampaign
        ]
    }),
    ResetAdsCampaigns: (adsCampaigns, state) => ({
        ...state,
        adsCampaignsList: [
            ...adsCampaigns
        ]
    }),
    RemoveAdsCampaign: (id, state) => {
        const i = findIndex(propEq('id', id), state.adsCampaignsList);

        return {
            ...state,
            adsCampaignsList: [
                ...state.adsCampaignsList.slice(0, i),
                ...state.adsCampaignsList.slice(i + 1)
            ]
        };
    },
    UpdateAdsCampaign: (adsCampaign, state) => state,
    ResetAdsCampaignsAction: state => ({
        ...state,
        action
    }),
    ToggleRemoveDialog: (dialog, state) => ({
        ...state,
        removeDialog: dialog
    })
});

export default curryN(2, reducefn);
