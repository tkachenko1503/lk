import Promise from "bluebird";
import express from "express";
import {getFromTo} from '../../lib/index';
import {getAdsPaymentsBySiteCSV} from '../../models/adsPayments';
import {getSiteNameById} from '../../models/sites';

const ads = express.Router();

ads.use('/payments', (req, res, next) => {
    const {siteId} = req.query;
    const {from, to} = getFromTo(req.query);

    const siteName = getSiteNameById(siteId);
    const payments = getAdsPaymentsBySiteCSV(siteId, from , to);

    Promise.all([siteName, payments])
        .then(([siteName, payments]) => {
            const filename = buildAdsPaymentsCSVFilename(siteName, from, to);

            res.attachment(filename);
            res.end(payments);
        })
        .catch((err) => {
            res.json(err.message);
        });
});

function buildAdsPaymentsCSVFilename(siteName, from, to) {
    const format = 'YYYY-MM-DD';
    const filename = `ads-payments_${siteName}_${from.format(format)}_${to.format(format)}.csv`;

    return filename
};

export default ads;
