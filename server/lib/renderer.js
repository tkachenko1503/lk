import pug from 'pug';
import path from 'path';
import fs from 'fs';

const pdfLayout = path.resolve('server/views/pdf/index.pug');
const styleFiles = [
    path.resolve('node_modules/c3/c3.min.css'),
    path.resolve('dist/app/vendor/style/bootstrap.min.css'),
    path.resolve('dist/app/vendor/style/bootstrap-reset.css'),
    path.resolve('dist/app/vendor/style/font-awesome.css'),
    path.resolve('dist/app/vendor/style/simple-line-icons.css'),
    path.resolve('dist/app/vendor/style/default-theme.css'),
    path.resolve('dist/app/vendor/style/style.css'),
    path.resolve('dist/app/vendor/style/style-responsive.css'),
    path.resolve('dist/app/vendor/style/pdf-dashboard.css')
];

const styles = styleFiles.reduce((styleString, file) => {
    const content = fs.readFileSync(file, 'utf8');
    return `${styleString} \n ${content}`;
}, '');

const port = process.env.PORT || 3000;
const host = process.env.SERVER_URL ? process.env.SERVER_URL : `http://localhost:${port}`;

export function render(params) {
    return pug.renderFile(pdfLayout, {
        pdfData: params.data,
        site: params.site,
        efficiencyConfig: JSON.stringify({
            bindto: '#efficiency-chart',
            transition: {
                duration: 0
            },
            ...params.efficiencyConfig,
            size: {
                width: 520
            }
        }),
        additionalRedirects: JSON.stringify({
            bindto: '#load-chart',
            transition: {
                duration: 0
            },
            ...params.additionalRedirects,
            size: {
                width: 520
            }
        }),
        prettyDates: params.prettyDates,
        summaries: params.summaries,
        htmlReportData: params.htmlReportData,
        styles,
        host
    });
}
