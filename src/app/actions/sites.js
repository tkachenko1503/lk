import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import head from 'ramda/src/head';
import prop from 'ramda/src/prop';
import Site from 'models/Site';
import actions$ from 'actions/index';
import {App, Sites} from 'actions/types';

export const getAvailableSites = flyd.stream();
export const setAvailableSites = flyd.stream();
export const setCurrentSite = flyd.stream();

const sitesCollection = getAvailableSites
    .map(Site.getAvailableSites)
    .map(Site.toJSON);

flyd.on(
    setCurrentSite,
    sitesCollection
        .map(pipe(head, prop('objectId')))
);

flyd.on(
    setAvailableSites,
    sitesCollection
);

flyd.on(
    pipe(App.Sites, actions$),
    mergeAll([
        getAvailableSites.map(() => Sites.RequestSites()),
        setAvailableSites.map(Sites.SetSites),
        setCurrentSite.map(Sites.ChangeSite)
    ])
);
