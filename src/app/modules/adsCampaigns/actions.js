import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import actions$ from 'actions/index';
import AdsCampaign from 'models/AdsCampaign';
import {updateAndSaveAdsCampaign} from 'modules/adsCampaigns';
import {App, AdsCampaigns} from 'actions/types';

export const makeAction = flyd.stream();
export const saveAdsCampaign = flyd.stream();
export const fetchAdsCampaigns = flyd.stream();
export const removeAdsCampaign = flyd.stream();
export const updateAdsCampaign = flyd.stream();
export const closeAdsCampaignsAction = flyd.stream();
export const toggleRemoveDialog = flyd.stream();

const resetAction = mergeAll([
    saveAdsCampaign,
    updateAdsCampaign
]);

flyd.on(() => flyd.endsOn(resetAction, closeAdsCampaignsAction.map(resetAction)), makeAction);

flyd.on(
    pipe(App.AdsCampaigns, actions$),
    mergeAll([
        makeAction.map(AdsCampaigns.MakeAdsCampaignsAction),

        removeAdsCampaign
            .map(AdsCampaign.removeFromAccount)
            .map(adsCampaign => AdsCampaigns.RemoveAdsCampaign(adsCampaign.id)),

        saveAdsCampaign
            .map(AdsCampaign.spawn)
            .map(AdsCampaigns.SaveAdsCampaign),

        fetchAdsCampaigns
            .map(AdsCampaign.getByAdvertiser)
            .map(AdsCampaigns.ResetAdsCampaigns),

        updateAdsCampaign
            .map(updateAndSaveAdsCampaign)
            .map(AdsCampaigns.UpdateAdsCampaign),

        resetAction
            .map(() => AdsCampaigns.ResetAdsCampaignsAction()),

        toggleRemoveDialog
            .map(AdsCampaigns.ToggleRemoveDialog)
    ])
);
