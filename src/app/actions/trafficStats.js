import flyd from 'flyd';
import {downloadTrafficCSV} from 'lib/trafficStatsCsv';

export const exportTrafficCsvFile = flyd.stream();

flyd.on(downloadTrafficCSV, exportTrafficCsvFile);
