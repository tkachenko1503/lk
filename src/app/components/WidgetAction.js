import React from 'react';
import EscKeyClosable from 'components/EscKeyClosable';
import {updateAndSaveWidget, closeWidgetAction} from 'actions/widgets';

class WidgetAction extends EscKeyClosable {
    constructor(props) {
        super(props);

        this.update = this.update.bind(this);
        this.closeAction = closeWidgetAction;
    }

    getAttrs() {
        // заглушка
    }

    serialize(form) {
        return Object.keys(form.elements)
            .reduce((obj, elem) => {
                if (form[elem]) {
                    obj[elem] = form[elem].value;
                }
                return obj;
            }, {});
    }

    update(e) {
        e.preventDefault();
        const form = this.serialize(e.target);
        const attrs = this.getAttrs(form);

        if (attrs) {
            updateAndSaveWidget({
                widget: this.props.widget,
                attrs
            });
        }
    }
}

export default WidgetAction;
