import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import actions$ from 'actions/index';
import {App, AdsChartControls} from 'actions/types';

export const setParam = flyd.stream();

flyd.on(
    pipe(App.AdsChartControls, actions$),
    mergeAll([
        setParam.map(({buttonId, paramId}) => AdsChartControls.SetParam(paramId, buttonId))
    ])
);
