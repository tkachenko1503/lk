import React from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'lib/react-bootstrap-date-picker';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import Overlay from 'react-bootstrap/lib/Overlay';

import styles from 'styles/dashboardPageHead.css';

const dayLabels = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
const monthLabels = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

class CustomDateRangeFilter extends React.Component {
    errorTooltip({id, message}) {
        const getTarget = () => ReactDOM.findDOMNode(this.refs.fromPicker);
        
        return (
            <Overlay
                container={this}
                show={true}
                placement="top"
                target={getTarget}
            >
                <Tooltip id={id}>
                    {message}
                </Tooltip>
            </Overlay>
        );
    }

    render() {
        const {error, validationError} = this.props.meta;

        return (
            <div>
                <div className={styles.datePickerBlock}>
                    <label className={styles.datePickerLabel}>От</label>
                    <div className={styles.inlineDateInput}>
                        {(error && validationError)
                            ? this.errorTooltip({
                                id: validationError.field,
                                message: validationError.message
                            })
                            : null}
                        <DatePicker
                            ref="fromPicker"
                            bsSize="small"
                            value={this.props.date.from.format()}
                            onChange={this.props.setFromDate}
                            dayLabels={dayLabels}
                            monthLabels={monthLabels}
                            dateFormat="DD/MM/YYYY"
                        />
                    </div>
                </div>

                <div className={styles.datePickerBlock}>
                    <label className={styles.datePickerLabel}>До</label>
                    <div className={styles.inlineDateInput}>
                        <DatePicker
                            bsSize="small"
                            value={this.props.date.to.format()}
                            onChange={this.props.setToDate}
                            dayLabels={dayLabels}
                            monthLabels={monthLabels}
                            dateFormat="DD/MM/YYYY"
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default CustomDateRangeFilter;
