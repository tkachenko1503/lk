import React from 'react';
import connectToStore from 'lib/connectToStore';
import AdsPaymentsPage from 'components/AdsPaymentsPage';
import {ADS_PAYMENTS_SUMMARIES} from 'lib/constants';
import {summariesBuildFn} from 'lib/index';
import {requestAdsPaymentsForSite, exportAdsPaymentsInCSV} from 'actions/adsPayments';
import {setAppContext} from 'modules/appContext/actions';


class AdsPaymentsPageContainer extends React.Component {

    constructor(props) {
        super(props);

        this.requestPayments = this.requestPayments.bind(this);
        this.exportPaymentsInCsv = this.exportPaymentsInCsv.bind(this);
    }

    componentWillMount() {
        const {currentSiteId} = this.props;

        if (currentSiteId) {
            this.requestPayments()
        }

        this.contextChangeHandler = setAppContext.map(() => {
            setTimeout(this.requestPayments, 0);
        });
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
    }

    render() {
        const {payments, summaries} = this.props;

        return (
            <AdsPaymentsPage
                summaries={summaries}
                payments={payments}
                actionForApplyFilters={requestAdsPaymentsForSite}
                exportPaymentsInCsv={this.exportPaymentsInCsv}
            />
        );
    }

    requestPayments() {
        const {currentSiteId, filters} = this.props;

        requestAdsPaymentsForSite({
            siteId: currentSiteId,
            filters
        });
    }

    exportPaymentsInCsv() {
        const {currentSiteId, filters} = this.props;

        exportAdsPaymentsInCSV({
            siteId: currentSiteId,
            filters
        });
    }
}

const fields = ['availableForCashing', 'incomAll', 'incomLastMonth', 'incomLastThreeMonths'];
const buildSummaries = summariesBuildFn(fields, ADS_PAYMENTS_SUMMARIES);

export default connectToStore(AdsPaymentsPageContainer, ({app, filters, adsPayments}) => ({
    currentSiteId: app.selectedContext,
    summaries: buildSummaries(adsPayments.summaries),
    payments: adsPayments.payments,
    filters
}));


