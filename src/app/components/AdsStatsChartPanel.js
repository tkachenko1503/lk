import React from 'react';
import classnames from 'classnames';
import Panel from 'react-bootstrap/lib/Panel';
import AdsStatsChart from 'components/AdsStatsChart';
import AdsStatsChartControls from 'components/AdsStatsChartControls';

import panel from 'styles/panel.css';

/**
 * Панель с граффиком и кнопками переключения статистики по рекламе
 * @constructor
 */
const AdsStatsChartPanel = (props) => {
    const {switchParam, params, created, dateRange} = props;
    const {firstColunmData, secondColunmData} = props;
    const {firstColunmTitle, secondColunmTitle} = props;

    const panelHeader = (
        <div>Статистика</div>
    );

    return (
        <Panel
            header={panelHeader}
            bsClass={classnames(
                panel.fixedHeader,
                "panel"
            )}
        >
            <AdsStatsChartControls
                params={params}
                handler={switchParam}
            />

            <AdsStatsChart
                first={firstColunmTitle}
                second={secondColunmTitle}
                col1={firstColunmData}
                col2={secondColunmData}
                created={created}
                dateRange={dateRange}
            />
        </Panel>
    );
};

export default AdsStatsChartPanel;
