import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import actions$ from 'actions/index';
import {App, TrafficChartControls} from 'actions/types';

export const setParams = flyd.stream();

flyd.on(
    pipe(App.TrafficChartControls, actions$),
    mergeAll([
        setParams.map(({first, second}) => TrafficChartControls.SetParams(first, second))
    ])
);
