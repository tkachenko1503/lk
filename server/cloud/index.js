// запрещаем удалять и при условном удалении оставляем доступ только админам
// Виджеты
Parse.Cloud.beforeDelete("Widget", beforeDeleteRemovedItem);
Parse.Cloud.beforeSave("Widget", beforeSaveRemovedItem);

// АБ тесты
Parse.Cloud.beforeDelete("ABTest", beforeDeleteRemovedItem);
Parse.Cloud.beforeSave("ABTest", beforeSaveRemovedItem);


function beforeDeleteRemovedItem(request, response) {
    // помечаем виджет как удалённый
    request.object.set('removed', true);

    // выставляем доступ только для админов
    var acl = getAdminACL();
    request.object.setACL(acl);

    request.object
        .save(null)
        .then(function () {
            response.error('Removed Marked');
        })
}

function beforeSaveRemovedItem(request, response) {
    if (request.object.get("removed")) {
        // выставляем доступ только для админов
        var acl = getAdminACL();
        request.object.setACL(acl);
    }

    response.success();
}

function getAdminACL() {
    var adminACL = new Parse.ACL();

    adminACL.setRoleReadAccess('Client', false);
    adminACL.setRoleWriteAccess('Client', false);
    adminACL.setRoleReadAccess('Admin', true);
    adminACL.setRoleWriteAccess('Admin', true);

    return adminACL;
}
