import Parse from 'parse';

/**
 * Проверяет залогинен ли юзер
 * @returns {Boolean}
 */
export function isLoggedIn() {
    return !!Parse.User.current();
}

/**
 * Возвращает текущего юзера
 * @returns {Parse.User}
 */
export function getUser() {
    return Parse.User.current();
}

/**
 * Отправляет запрос на авторизацию
 * @param {String} username
 * @param {String} password
 * @returns {Parse.Promise}
 */
export function login({username, password}) {
    return Parse.User.logIn(username, password);
}

/**
 * Отправляет запрос на регистрацию
 * @param {String} username
 * @param {String} email
 * @param {String} password
 * @returns {Parse.Promise}
 */
export function register({username, password, email}) {
    const user = new Parse.User();

    user.set("username", username);
    user.set("password", password);
    user.set("email", email);

    return user.signUp();
}

/**
 * Отправляет запрос на выход из аккаунта
 * @param {String} username
 * @param {String} password
 * @returns {Parse.Promise}
 */
export function loguot() {
    return Parse.User.logOut();
}

export function isAdmin() {
    const adminRoleQuery = new Parse.Query(Parse.Role);

    adminRoleQuery.equalTo('name', 'Admin');

    return adminRoleQuery.first()
        .then((adminRole) => {
            const relation = new Parse.Relation(adminRole, 'users');
            const admins = relation.query();

            admins.equalTo('objectId', Parse.User.current().id);
            return admins.first();
        })
        .then(user => {
            if (user) {
                return { isAdmin: true };
            } else {
                return { isAdmin: false };
            }
        })
        .catch(() => ({ isAdmin: false }));
}
