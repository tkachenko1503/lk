import Parse from 'parse';
import Widget from 'models/Widget';
import BaseModel from 'models/Base';

const className = 'ABTestItem';

class ABTestItem extends BaseModel {
    constructor(attrs) {
        super(className);

        if (attrs) {
            this.set(attrs);
        }
    }

    static createTestItem({widgetId, frequency}) {
        const testItem = new ABTestItem();
        const widget = new Widget();

        widget.id = widgetId;

        testItem.set('frequency', frequency);
        testItem.set('Widget', widget);

        return testItem;
    }
    
    static setFrequency({testItem, frequency}) {
        testItem.set('frequency', frequency);
    }
}

export default ABTestItem;

Parse.Object.registerSubclass('ABTestItem', ABTestItem);
