import curryN from 'ramda/src/curryN';
import findIndex from 'ramda/src/findIndex';
import propEq from 'ramda/src/propEq';
import {Widgets} from 'actions/types';

const meta = {
    error: false,
    pending: false
};
const action = {
    id: null,
    name: null
};

export function defaultWidgets() {
    return {
        list: [],
        action,
        meta,
        canSaveAdsSettings: false,
        removeDialog: {
            open: false,
            widget: null
        }
    };
}

const reducefn = Widgets.caseOn({
    MakeWidgetAction: (action, state) => ({
        ...state,
        action
    }),
    SaveWidget: (widget, state) => ({
        ...state,
        list: [
            ...state.list,
            widget
        ]
    }),
    SetWidgets: (widgets, state) => ({
        ...state,
        list: [
            ...widgets
        ],
        canSaveAdsSettings: false
    }),
    RemoveWidget: (id, state) => {
        const i = findIndex(propEq('id', id), state.list);

        return {
            ...state,
            list: [
                ...state.list.slice(0, i),
                ...state.list.slice(i + 1)
            ]
        };
    },
    
    UpdateAndSaveWidget: (widget, state) => ({
        ...state,
        canSaveAdsSettings: false
    }),
    UpdateWidget: (widget, state) => state,

    ResetWidgetAction: state => ({
        ...state,
        action
    }),
    SetWidgetMeta: (meta, state) => ({
        ...state,
        meta: {
            ...state.meta,
            ...meta
        }
    }),
    SetWidgetsAdsSavable: (state) => ({
        ...state,
        canSaveAdsSettings: true
    }),
    ToggleRemoveDialog: (dialog, state) => ({
        ...state,
        removeDialog: dialog
    })
});

export default curryN(2, reducefn);
