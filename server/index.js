import 'babel-register';

import express from 'express';
import morgan from 'morgan';
import path from 'path';
import compression from 'compression';

import webpack from 'webpack';
import devMiddleware from 'webpack-dev-middleware';
import hotMiddleware from 'webpack-hot-middleware';
import config from '../configs/webpack.client';

import { catchError } from './lib';
import apiServer from './apiServer';

const app = express();
const PORT = process.env.PORT || 3000;
const DEVELOP = 'develop';
const NODE_ENV = process.env.NODE_ENV || DEVELOP;

// basic setup
app.set('views', path.resolve('server/views'));
app.set('view engine', 'pug');

// setup for different env
if (NODE_ENV === DEVELOP) {
    const compiler = webpack(config);

    app.use(morgan('dev'));

    app.use(express.static(path.resolve('dist/app')));

    app.use(devMiddleware(compiler, {
        noInfo: true,
        publicPath: config.output.publicPath
    }));

    app.use(hotMiddleware(compiler, {
        log: console.log,
        path: '/__webpack_hmr',
        heartbeat: 10 * 1000
    }));
} else {
    const ONE_DAY = 86400000;

    app.use(morgan('combined'));

    app.use(compression());
    app.use(express.static(path.resolve('dist/app'), { maxAge: ONE_DAY }));
}

// api
app.use(apiServer);

// render single page
app.use((req, res) => res.render('index', {
    env: NODE_ENV
}));

// catch all errors
app.use(catchError);

// start
app.listen(PORT, () => console.log(
    'Server running on:', PORT,
    'Mode:', NODE_ENV
));
