import React from 'react';
import classnames from 'classnames';
import curry from 'ramda/src/curry';
import map from 'ramda/src/map';
import pluck from 'ramda/src/pluck';
import pipe from 'ramda/src/pipe';
import splitEvery from 'ramda/src/splitEvery';
import join from 'ramda/src/join';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import Button from 'react-bootstrap/lib/Button';
import {TRAFFIC_CHART_BUTTONS} from 'lib/constants';

const buttons = splitEvery(2, TRAFFIC_CHART_BUTTONS);
const getTitles = map( pipe(pluck('title'), join('/')) );
const getIds = map( pipe(pluck('id'), join('/')) );

const [firstButtonId, secondButtonId] = getIds(buttons);
const [firstButtonTitle, secondButtonTitle] = getTitles(buttons);

const TrafficChartControls = ({controls, changeFields}) => {
    const action = chartControlsHandler(changeFields);
    const firstIsActive = `${controls.first}/${controls.second}` === firstButtonId;

    return (
        <ButtonGroup bsSize="xsmall">
            <Button
                bsStyle={classnames({
                    default: !firstIsActive,
                    primary: firstIsActive
                })}
                onClick={action}
                data-button-id={firstButtonId}
            >
                {firstButtonTitle}
            </Button>

            <Button
                bsStyle={classnames({
                    default: firstIsActive,
                    primary: !firstIsActive
                })}
                onClick={action}
                data-button-id={secondButtonId}
            >
                {secondButtonTitle}
            </Button>
        </ButtonGroup>
    );
};

export default TrafficChartControls;

const chartControlsHandler = curry((handler, e) => {
    const buttonId = e.target.dataset.buttonId;
    const [first, second] = buttonId.split('/');

    handler({
        first,
        second
    });
});

