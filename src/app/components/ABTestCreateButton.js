import React from 'react';
import Button from 'react-bootstrap/lib/Button';

const ABTestCreateButton = ({createABTest}) => {
    return (
        <div>
            <Button
                onClick={createABTest}
            >
                Создать новый A/B тест
            </Button>
        </div>
    );
};

export default ABTestCreateButton;
