import knex from '../lib/knex';
import {pipe, prop, map} from 'ramda';

const processArticlesBySite = pipe(prop('rows'), map(formatResults));

export function getArticlesBySite({site, from, to}) {
    return knex.raw(`
        SELECT 
            url, load, show,
            (100 * load) / show AS ctr
        FROM (
            SELECT 
                url,
                sum(coalesce((data->'ntvk_load')::text::int, 0)) AS load,
                sum(coalesce((data->'show')::text::int, 0)) AS show
            FROM page_stats
            WHERE site = '${site}'
                AND since
                    BETWEEN '${from}'
                    AND '${to}'
            GROUP BY url
            ORDER BY load
            DESC
        ) 
        AS st 
    `)
        .then(processArticlesBySite)
        .catch(error => []);
}

function formatResults({ctr, load, show, url}) {
    return {
        ctr: Number(ctr),
        show: Number(show),
        load: Number(load),
        url: url
    };
}