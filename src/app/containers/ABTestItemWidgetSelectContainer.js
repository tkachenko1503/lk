import React from 'react';
import connectToStore from 'lib/connectToStore';
import {setWidgetToNewTestItem} from 'actions/abTests';
import {findByProp} from 'lib/index';
import {getAvailableWidgets} from 'lib/widgets';
import WidgetSelect from 'components/WidgetSelect';

class ABTestItemWidgetSelectContainer extends React.Component {
    constructor(props) {
        super(props);
        this.setWidget = this.setWidget.bind(this);
    }

    setWidget(e, widgetId) {
        const {testItem, widgetsList} = this.props;
        const widget = findByProp(widgetId, widgetsList, 'id');
        setWidgetToNewTestItem({testItem, widget});
    }

    render() {
        const {widget, widgetsList} = this.props;
        
        return (
            <WidgetSelect
                widgetsList={widgetsList}
                widgetSelectHandler={this.setWidget}
                choosenWidget={widget ? widget.id : null}
                defaultTitle="Нет"
                widgetName={widget ? widget.get('name') : null}
            />
        );
    }
}

export default connectToStore(ABTestItemWidgetSelectContainer, ({widgets, abTests}) => {
    const activeTestId = abTests.action.id;
    const testItems = abTests.testItems[activeTestId] || [];

    return {
        widgetsList: getAvailableWidgets(testItems, widgets.list)
    };
});
