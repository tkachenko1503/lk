import React from 'react';
import connectToStore from 'lib/connectToStore';
import PopularArticlesTable from 'components/PopularArticlesTable';
import {setArticlesControl, setArticlesStatsForSite, exportArticlesCsvFile} from 'actions/articlesStats';
import {applyControls, findByProp} from 'lib/index';

class PopularArticles extends React.Component {
    constructor(props) {
        super(props);

        this.setTableSort = this.setTableSort.bind(this);
        this.exportTableInCsv = this.exportTableInCsv.bind(this);
    }

    componentDidMount() {
        this.resetPagerOnNewStats = setArticlesStatsForSite.map(() => {
            this.setTablePage(null, {eventKey: 1})
        });
    }

    componentWillUnmount() {
        this.resetPagerOnNewStats.end(true);
    }

    setTablePage(e, {eventKey}) {
        setArticlesControl({
            control: 'page',
            value: Number(eventKey)
        });
    }

    setTableSize(e) {
        const value = e.target.value;
        setArticlesControl({
            control: 'size',
            value: Number(value)
        });
    }

    setTableSort(e) {
        const {id, type} = this.props.tableSort;
        const sortId = e.currentTarget.dataset.sortId;
        const value = {
            id: sortId,
            type: id === sortId && type === 'desc' ? 'asc' : 'desc'
        };

        setArticlesControl({
            control: 'sort',
            value
        });
    }

    exportTableInCsv() {
        const {fullAdsList, datesRange} = this.props;
        const {currentSiteId, availableSites} = this.props;
        const {currentWidgetId, widgetsList} = this.props;

        const site = findByProp(currentSiteId, availableSites, 'id');
        const widget = currentWidgetId && findByProp(currentWidgetId, widgetsList, 'id');

        exportArticlesCsvFile({
            rows: fullAdsList,
            datesRange,
            currentSite: site && site.get('name'),
            currentWidget: widget && widget.get('name')
        });
    }

    render() {
        const {rows, tableSort, datesRange} = this.props;
        const {pagerSize, activePage, currentSize} = this.props;

        return (
            <div>
                {rows.length
                    ? (
                        <PopularArticlesTable
                            rows={rows}
                            tableSort={tableSort}
                            pagerSize={pagerSize}
                            activePage={activePage}
                            currentSize={currentSize}
                            datesRange={datesRange}
                            setTableSort={this.setTableSort}
                            setTableSize={this.setTableSize}
                            setTablePage={this.setTablePage}
                            exportTableInCsv={this.exportTableInCsv}
                        />
                    )
                    : 'На данный момент статистика отсутствует.'}
            </div>
        );
    }
}

export default connectToStore(PopularArticles, ({articles, filters, app}) => ({
    rows: applyControls(articles.articlesTableControls, articles.articlesList),
    tableSort: articles.articlesTableControls.sort,
    pagerSize: Math.ceil(articles.articlesList.length / articles.articlesTableControls.size),
    activePage: articles.articlesTableControls.page,
    currentSize: articles.articlesTableControls.size,
    datesRange: filters.date,
    currentSiteId: app.selectedContext,
    availableSites: app.context,
    currentWidgetId: filters.widget,
    fullAdsList: articles.articlesList
}));