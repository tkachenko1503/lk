import curryN from 'ramda/src/curryN';
import {AdsStatsControls} from 'actions/types';

export function defaultAdsStatsControls() {
    return {
        sort: {
            type: 'desc',
            id: null
        },
        page: 1,
        size: 10
    };
}

const reducefn = AdsStatsControls.caseOn({
    SetControlValue: ({control, value}, state) => {
        const newState = {
            ...state,
            [control]: value
        };

        // если меняем размер таблицы сбрасываем пейджинг
        if (control === 'size') {
            newState.page = 1;
        }

        return newState;
    }
});

export default curryN(2, reducefn);
