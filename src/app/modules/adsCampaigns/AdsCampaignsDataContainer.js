import React from 'react';
import connectToStore from 'lib/connectToStore';
import {fetchAdsCampaigns} from 'modules/adsCampaigns/actions';
import {setAppContext} from 'modules/appContext/actions';
import {findByProp} from 'lib/index';

class AdsCampaignsDataContainer extends React.Component {
    componentWillMount() {
        const {currentContext} = this.props;

        if (currentContext) {
            fetchAdsCampaigns(currentContext.id);
        }
        this.contextChangeHandler = setAppContext.map(fetchAdsCampaigns);
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
    }

    render() {
        return null;
    }
}

export default connectToStore(AdsCampaignsDataContainer, ({adsCampaigns, app}) => ({
    currentContext: app.selectedContext && findByProp(app.selectedContext, app.context, 'id')
}));