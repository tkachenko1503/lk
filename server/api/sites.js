import express from "express";
import {getSitesList} from '../models/sites';

const stats = express.Router();

stats.get('/', function (req, res, next) {
    getSitesList()
        .then((data) => {
            res.json(data);
        });
});

export default stats;
