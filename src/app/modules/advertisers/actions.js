import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import actions$ from 'actions/index';
import {uploadStatsFile} from 'modules/advertisers';
import {getAdvertisersContent} from 'queries/index';
import {downloadAdvertisersContentCSV} from 'lib/advertisersContentCsv';
import {App, Advertisers} from 'actions/types';

export const chooseAdvertisersStatsFile = flyd.stream();
export const chooseAdsCampaign = flyd.stream();
export const uploadAdvertisersStatsFile = flyd.stream();
export const fetchAdvertisersContent = flyd.stream();
export const setAdvertisersContent = flyd.stream();
export const setAdvertisersContentControl = flyd.stream();
export const exportAdvertisersContentCsvFile = flyd.stream();

flyd.on(downloadAdvertisersContentCSV, exportAdvertisersContentCsvFile);
flyd.on(setAdvertisersContent, fetchAdvertisersContent.map(getAdvertisersContent));

flyd.on(
    pipe(App.Advertisers, actions$),
    mergeAll([
        chooseAdvertisersStatsFile
            .map(Advertisers.SetStatsFile),

        chooseAdsCampaign
            .map(Advertisers.SetAdsCampaign),

        uploadAdvertisersStatsFile
            .map(uploadStatsFile)
            .map(() => Advertisers.ClearStatsFile()),

        setAdvertisersContentControl
            .map(Advertisers.SetContentTableControl),

        setAdvertisersContent
            .map(Advertisers.SetAdvertisersContent)
    ])
);
