import express from "express";
import multer from "multer";
import csvjson from 'csvjson';
import moment from 'moment';
import {writeStatsToDB, getAdvertisersContent} from '../models/advertisers';
import {getFromTo} from '../lib/index';

const upload = multer();
const advertisers = express.Router();
const options = {delimiter : ','};

advertisers.post('/upload-stats', upload.single('advertisersStats'), function (req, res, next) {
    const csvString = req.file.buffer.toString();
    const stats = csvjson.toObject(csvString, options);

    writeStatsToDB({
        stats,
        advertiser: req.body.advertiser,
        campaign: req.body.campaign,
        created_at: moment.utc().format('YYYY-MM-DD HH:mm:ssZ')
    })
        .then(result => res
            .status(200)
            .json({success: true}))
        .catch(error => res
            .status(500)
            .json({
                success: false,
                message: error.message
            }));
});

advertisers.get('/content/:site', function (req, res, next) {
    const datesRange = getFromTo(req.query);

    getAdvertisersContent({
        site: req.params.site,
        from: datesRange.from.format('YYYY-MM-DD HH:mm:ss'),
        to: datesRange.to.format('YYYY-MM-DD HH:mm:ss'),
        widgetId: req.query.widget
    })
        .then(content => {
            res.json({content});
        })
        .catch(next);
});

export default advertisers;
