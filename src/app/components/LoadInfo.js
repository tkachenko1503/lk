import React from 'react';
import classnames from 'classnames';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';
import Panel from 'react-bootstrap/lib/Panel';
import { If, Then } from 'react-if';

import styles from '../styles/dashboard.css';

const LoadInfo = ({ createdAt, load, loadCSV, loadStatisticsPdf, contextType, isAdmin }) => {
    const panelHeader = (
        <div>
            Статистика
            <span className="tools pull-right">
                <a className="fa fa-repeat box-refresh" href="javascript:;"></a>
            </span>
        </div>
    );

    const content = (
        <div>
            <If condition={contextType === 'Site'}>
                <Then>
                    <div>
                        {createdAt
                            ? (
                                <div>
                                    <strong className={styles.statisticsLabel}>Дата создания:</strong>
                                    {createdAt}
                                </div>
                            )
                            : null}

                        {load
                            ? (
                                <div>
                                    <strong className={styles.statisticsLabel}>Всего посещений:</strong>
                                    {Number(load).toLocaleString('ru-RU')}
                                </div>
                            )
                            : null}
                    </div>
                </Then>
            </If>

            <ButtonToolbar className={styles.toCsv}>
                <Button
                    data-value="simple"
                    bsSize="small"
                    onClick={loadCSV}
                >в CSV</Button>

                {isAdmin
                    ? (
                        <Button
                            bsSize="small"
                            onClick={loadStatisticsPdf}
                        >в PDF</Button>)
                    : null}
            </ButtonToolbar>
        </div>
    );

    return (
        <Panel
            header={panelHeader}
            bsClass={classnames(
                styles.chartPanel,
                'panel'
            )}
            className={styles.shortPanel}
        >
            {content}
        </Panel>
    );
};

export default LoadInfo;
