import Parse from "parse";
import map from "ramda/src/map";

const _toJSON = map((item) => {
    return item.toJSON();
});

class BaseModel extends Parse.Object {
    static toJSON(items) {
        return _toJSON(items);
    }

    static getById(id) {
        let query = new Parse.Query(this);
        return query.get(id);
    }

    static getCollectionByCurrentUser() {
        let query = new Parse.Query(this);
        query.equalTo('user', Parse.User.current());
        return query.find();
    }

    static fetchAll(widgetList) {
        return Parse.Object.fetchAll(widgetList);
    }

    static saveAll(widgetList) {
        return Parse.Object.saveAll(widgetList);
    }

    /**
     * Помечаем объект как удалённый
     * @param {Parse.Object} item
     * @returns {Parse.Promise}
     */
    static removeFromAccount(item) {
        item.set('removed', true);

        return item.save(null);
    }
}

export default BaseModel;
