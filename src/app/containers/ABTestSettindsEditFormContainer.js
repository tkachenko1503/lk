import React from 'react';
import connectToStore from 'lib/connectToStore';
import {findByProp} from 'lib/index';
import {getAvailableWidgets} from 'lib/widgets';
import ABTestSettindsEditForm from 'components/ABTestSettindsEditForm';
import {fetchABTestItems, saveABTestSettings, addWidgetToExistenTest,
        setWidgetFrequencyToExistenTest, cancelTestItemsChanges,
        removeTestItem, resetAction} from 'actions/abTests';

class ABTestSettindsEditFormContainer extends React.Component {
    constructor(props) {
        super(props);
        this.saveExistenTest = this.saveExistenTest.bind(this);
        this.addWidgetToExistenTest = this.addWidgetToExistenTest.bind(this);
        this.setFrequency = this.setFrequency.bind(this);
        this.cancelChangesAndResetAction = this.cancelChangesAndResetAction.bind(this);
        this.removeWidget = this.removeWidget.bind(this);
    }
    
    componentWillMount() {
        const {test} = this.props;
        fetchABTestItems(test);
    }

    componentWillUnmount() {
        const {activeTestId, test} = this.props;

        if (activeTestId && activeTestId === test.id) {
            this.cancelChanges();
        }
    }

    addWidgetToExistenTest() {
        const {test} = this.props;
        addWidgetToExistenTest(test);
    }

    saveExistenTest() {
        const {testItems, activeTestId, testlist, itemsToRemove} = this.props;
        const test = findByProp(activeTestId, testlist, 'id');

        saveABTestSettings({
            test,
            testItems,
            itemsToRemove
        });
    }

    setFrequency(index, frequency) {
        const {testItems} = this.props;
        const testItem = testItems[index];
        
        setWidgetFrequencyToExistenTest({
            testItem,
            frequency
        });
    }

    cancelChanges() {
        const {testItems, activeTestId, testlist} = this.props;
        const test = findByProp(activeTestId, testlist, 'id');

        cancelTestItemsChanges({
            test,
            testItems
        });
    }

    cancelChangesAndResetAction() {
        this.cancelChanges();
        resetAction('');
    }

    removeWidget(e) {
        const {testItems, activeTestId, testlist} = this.props;
        const test = findByProp(activeTestId, testlist, 'id');
        const {index} = e.currentTarget.dataset;
        const testItem = testItems[index];

        removeTestItem({
            test,
            testItem
        });
    }

    render() {
        const {testItems, availableWidgets} = this.props;

        if (!testItems && testItems.length === 0) {
            return null;
        }

        return (
            <ABTestSettindsEditForm
                testItems={testItems}
                availableWidgets={availableWidgets}
                addWidgetToExistenTest={this.addWidgetToExistenTest}
                saveExistenTest={this.saveExistenTest}
                setFrequency={this.setFrequency}
                removeWidget={this.removeWidget}
                cancelChanges={this.cancelChangesAndResetAction}
            />
        );
    }
}

export default connectToStore(ABTestSettindsEditFormContainer, ({widgets, abTests}) => {
    const activeTestId = abTests.action.id;
    const testItems = abTests.testItems[activeTestId] || [];
    const itemsToRemove = abTests.testItemsBackup[activeTestId] || [];

    return {
        activeTestId,
        testItems,
        itemsToRemove,
        testlist: abTests.list,
        availableWidgets: getAvailableWidgets(testItems, widgets.list)
    };
});
