import express from "express";
import Promise from 'bluebird';
import {getAdsBySite, getOverviewData} from '../models/ads';
import {getIntervalString, getCSVGroup} from '../lib/apiHelpers';
import {getFromTo} from '../lib/index';
import {getAdsPaymentsBySite} from '../models/adsPayments';

const ads = express.Router();

ads.use('/payments', (req, res, next) => {
    const {siteId} = req.query;
    const {from, to} = getFromTo(req.query); 

    getAdsPaymentsBySite(siteId, from , to)
        .then((result) => {
            res.json(result);
        })
        .catch((err) => {
            res.json(err.message);
        });
});


ads.get('/:site', function (req, res, next) {
    const datesRange = getFromTo(req.query);
    const interval = getIntervalString(datesRange);
    const hoursOffset = datesRange.from.utcOffset() / 60;

    const modelParams = {
        site: req.params.site,
        from: datesRange.from.format('YYYY-MM-DD HH:mm:ssZ'),
        to: datesRange.to.format('YYYY-MM-DD HH:mm:ssZ'),
        widgetId: req.query.widget,
        interval: interval,
        hoursOffset: hoursOffset
    };

    const stats = getAdsBySite(modelParams);
    const overview = getOverviewData(modelParams);

    Promise.all([stats, overview])
        .then(result => {
            const [stats, overview] = result;

            res.json({stats, overview});
        });
});

ads.get('/csv/:site', function (req, res, next) {
    // @todo сделать валидацию на наличие from to параметров
    // const type = req.query.type || 'simple';
    // const rangeDates = getFromTo(req.query);
    // const interval = getCSVGroup(rangeDates);
    // const widgetId = req.query.widget;
    // let getData;
    //
    // if (widgetId) {
    //     getData = getStatsForCSVByWidget({
    //         type: type,
    //         site: req.params.site,
    //         ...rangeDates,
    //         widget: widgetId,
    //         interval: interval
    //     });
    // } else {
    //     getData = getStatsForCSVAllWidgets({
    //         type: type,
    //         site: req.params.site,
    //         ...rangeDates,
    //         interval: interval
    //     });
    // }
    //
    // getData.then(result => {
    //     res.json(result);
    // });
});

export default ads;
