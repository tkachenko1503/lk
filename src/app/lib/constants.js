export const formatNumber = (s) => Number(s).toLocaleString('ru-RU');
const formatMoney = (s) => `${s} р.`;


// Доступные действия для виджета
export const ACTIONS_MAP = [
    {
        key: 'rename',
        title: 'Переименовать'
    },
    {
        key: 'editView',
        title: 'Изменить внешний вид'
    },
    {
        key: 'download',
        title: 'Показать код виджета'
    }
];

// Мапинг кодов ошибок на человеко-понятные сообщения
export const AUTH_ERROR_MESSAGES = {
    101: "Неправильный логин или пароль.",
    202: "На этот почтовый ящик уже зарегистрирован аккаунт.",
    203: "На этот почтовый ящик уже зарегистрирован аккаунт."
};

// Навигация в сайдбаре
export const DASHBOARD = '/dashboard';
export const WIDGETS = '/widgets';
export const ADS = '/ads';
export const TRAFFIC = '/traffic';
export const AB_TESTS = '/ab-tests';
export const ANALYSIS = '/analysis';
export const ADS_CAMPAIGNS = '/ads-campaigns';
export const ADVERTISER_IMPORT = '/advertisers/import-stats';

export const ADS_SUBLINKS = [
    {
        navId: '/ads/settings',
        title: 'Параметры рекламы'
    },
    {
        navId: '/ads/statistics',
        title: 'Статистика'
    },
    {
        navId: '/ads/advertisers',
        title: 'Рекламодатели'
    },
    {
        navId: '/ads/payments',
        title: 'Выплаты'
    }
];
export const TRAFFIC_SUBLINKS = [
    {
        navId: '/traffic/settings',
        title: 'Параметры биржи'
    },
    {
        navId: '/traffic/statistics',
        title: 'Статистика обмена'
    }
];
export const AB_TESTS_SUBLINKS = [
    {
        navId: '/ab-tests/settings',
        title: 'Настройки A/B тестов'
    }
];
export const ANALYSIS_SUBLINKS = [
    {
        navId: '/analysis/content',
        title: 'Анализ содержания'
    }
];

// Четыре цветные плитки
export const SUMMARIES_PARAMS = [
    {
        panelClass: 'purple',
        icon: 'fa-eye',
        valueColor: 'white'
    },
    {
        symbolClass: 'purple-color',
        icon: 'fa-mail-reply-all',
        valueColor: 'gray'
    },
    {
        panelClass: 'green',
        icon: 'fa-copy',
        valueColor: 'white'
    },
    {
        symbolClass: 'green-color',
        icon: 'fa-bullseye',
        valueColor: 'gray'
    }
];

export const DASHBOARD_SITE_STATS_SUMMARIES = [
    {
        title: 'Показы виджета',
        format: formatNumber
    },
    {
        title: 'Переходов в виджете',
        format: formatNumber
    },
    {
        title: 'Глубина просмотра'
    },
    {
        title: 'CTR виджета',
        format: (s) => `${s}%`
    }
];

export const DASHBOARD_ADVERTISER_STATS_SUMMARIES = [
    {
        title: 'Показы объявлений',
        format: formatNumber
    },
    {
        title: 'Переходов по объявлениям',
        format: formatNumber
    },
    {
        title: 'Глубина просмотра'
    },
    {
        title: 'CTR объявлений',
        format: (s) => `${s}%`
    }
];

// Плитки на странице рекламы
export const ADS_STATS_SUMMARIES = [
    {
        title: 'Показ объявлений',
        format: formatNumber
    },
    {
        title: 'Переходов по объявлениям',
        format: formatNumber
    },
    {
        title: 'Средняя стоимость за переход',
        format: formatMoney
    },
    {
        title: 'Доход за период',
        format: formatMoney
    }
];

// Плитки на странице реклама/выплаты
export const ADS_PAYMENTS_SUMMARIES = [
    {
        title: 'Доступно к выводу',
        format: formatMoney
    },
    {
        title: 'Общий доход за все время',
        format: formatMoney
    },
    {
        title: 'Доход за последний месяц',
        format: formatMoney
    },
    {
        title: 'Доход за последние 3 месяца',
        format: formatMoney
    }
];

// Плитки на странице статистики обмена
export const TRAFFIC_STATS_SUMMARIES = [
    {
        title: 'Показов ваших объявлений',
        format: formatNumber
    },
    {
        title: 'Переходов по вашим объявлениям',
        format: formatNumber
    },
    {
        title: 'CTR ваших объявлений',
        format: (s) => `${s}%`
    },
    {
        title: 'Показов в ваших виджетах',
        format: formatNumber
    }
];

// Кнопки выбора параметров на графике рекламной статистики
export const ADS_CHART_BUTTONS = [
    {id: 'widget_load', title: 'Загрузки рекламы'},
    {id: 'ads_shows', title: 'Показы объявлений'},
    {id: 'ads_referrer', title: 'Переходы по объявлениям'},
    {id: 'ads_profit_mean', title: 'Средняя цена клика'},
    {id: 'ads_profit', title: 'Доход'}
];

export const TRAFFIC_CHART_BUTTONS = [
    {id: 'ads_shows', title: 'Показы объявлений'},
    {id: 'ads_referrer', title: 'Переходы по объявлениям'},
    {id: 'widget_load', title: 'Загрузки виджета'},
    {id: 'ctr', title: 'CTR'}
];

export const WIDGET_MAX_PLACES_NUMBER = 4;
