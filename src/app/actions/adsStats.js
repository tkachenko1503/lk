import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import {getSiteAdsStats, getSiteAdsStatsCSV} from 'queries/index';
import actions$ from 'actions/index';
import {App, AdsStats} from 'actions/types';
import {getCSVParams} from 'lib/csv';
import {downloadCSVFile} from 'lib/download';
import {logger} from 'lib/index';

export const requestAdsStatsForSite = flyd.stream();
export const setAdsStatsForSite = flyd.stream();

flyd.on(setAdsStatsForSite, requestAdsStatsForSite.map(getSiteAdsStats));

flyd.on(
    pipe(App.AdsStats, actions$),
    mergeAll([
        requestAdsStatsForSite
            .map(AdsStats.RequestAdsStats),

        setAdsStatsForSite
            .map(AdsStats.SetAdsStats)
    ])
);
