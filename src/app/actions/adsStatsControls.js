import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import actions$ from 'actions/index';
import {App, AdsStatsControls} from 'actions/types';
import {downloadAdsCSV} from 'lib/adsStatsCsv';

export const setAdsControl = flyd.stream();
export const exportAdsCsvFile = flyd.stream();

flyd.on(downloadAdsCSV, exportAdsCsvFile);

flyd.on(
    pipe(App.AdsStatsControls, actions$),
    mergeAll([
        setAdsControl.map(AdsStatsControls.SetControlValue)
    ])
);
