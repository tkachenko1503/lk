import Promise from 'bluebird';
import { getStatsBySite, getOverviewData,
            getStatsByAdvertiser, getAdvertiserOverviewData } from '../../models/stats';
import { getIntervalString } from '../../lib/apiHelpers';
import { getFromTo } from '../../lib/index';

const SITE_CONTEXT = 'Site';
const ADVERTISER_CONTEXT = 'Advertiser';

export function statsByContext({ site, params }) {
    let stats;
    let overview;

    const datesRange = getFromTo(params);
    const interval = getIntervalString(datesRange);
    const hoursOffset = datesRange.from.utcOffset() / 60;

    if (params.contextType === SITE_CONTEXT) {
        stats = getStatsBySite({
            site,
            from: datesRange.from.format('YYYY-MM-DD HH:mm:ssZ'),
            to: datesRange.to.format('YYYY-MM-DD HH:mm:ssZ'),
            widgetId: params.widget,
            interval,
            hoursOffset
        });
        overview = getOverviewData({
            site,
            ...datesRange,
            widget: params.widget
        });
    } else if (params.contextType === ADVERTISER_CONTEXT) {
        stats = getStatsByAdvertiser({
            advertiser: site,
            from: datesRange.from.format('YYYY-MM-DD HH:mm:ssZ'),
            to: datesRange.to.format('YYYY-MM-DD HH:mm:ssZ'),
            campaign: params.campaign,
            hoursOffset
        });
        overview = getAdvertiserOverviewData({
            advertiser: site,
            ...datesRange,
            campaign: params.campaign
        });
    }

    return Promise.all([stats, overview])
        .then(([statsResult, overviewResult]) => ({
            stats: statsResult,
            overview: overviewResult[0]
        }));
}
