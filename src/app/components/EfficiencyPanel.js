import React from 'react';
import classnames from 'classnames';
import Panel from 'react-bootstrap/lib/Panel';

import EfficiencyChart from './EfficiencyChart';

import styles from 'styles/dashboard.css';

/**
 * Панель с граффиком по кликам и показами
 * @constructor
 */
const EfficiencyPanel = (props) => {
    const {impression, referrer, created} = props.columns;
    const panelHeader = (
        <div>Эффективность</div>
    );

    return (
        <Panel
            header={panelHeader}
            bsClass={classnames(
                styles.chartPanel,
                "panel"
            )}
        >
            {impression.length && referrer.length
                ? <EfficiencyChart
                    first={props.firstTitle}
                    second={props.secondTitle}
                    col1={impression}
                    col2={referrer}
                    created={created}
                    dateRange={props.dateRange}
                />
                : null}
        </Panel>
    );
};

export default EfficiencyPanel;
