import find from 'ramda/src/find';
import propEq from 'ramda/src/propEq';
import props from 'ramda/src/props';
import zipWith from 'ramda/src/zipWith';
import sortBy from 'ramda/src/sortBy';
import prop from 'ramda/src/prop';
import pipe from 'ramda/src/pipe';
import drop from 'ramda/src/drop';
import take from 'ramda/src/take';
import reverse from 'ramda/src/reverse';
import identity from 'ramda/src/identity';
import equals from 'ramda/src/equals';
import moment from 'moment';

import {isLoggedIn} from './auth';

// Login
export function redirectToLogin(nextState, replace) {
    if (!isLoggedIn()) {
        replace({
            pathname: '/login',
            state: { nextPathname: nextState.location.pathname }
        })
    }
}

export function redirectToDashboard(nextState, replace) {
    if (isLoggedIn()) {
        replace('/')
    }
}

export function findByProp(id, list, propName = 'objectId') {
    return find(propEq(propName, id), list);
}

export function logger(data) {
    console.log('logger-> ', data);
    return data;
}

const buildSummary = (summary, value) => ({...summary, value});

export function summariesBuildFn(fields, defaults) {
    const extractProps = props(fields);
    const zipSummariesWithValues = zipWith(buildSummary, defaults);
    return pipe(extractProps, zipSummariesWithValues);
}

// Time
const oneDay = moment.duration(1, 'days');
const zeroTime = {
    hour: 0,
    minute: 0,
    second: 0
};
const finalTime = {
    hour: 23,
    minute: 59,
    second: 59
};

export function getFromTo({range = 'month', from, to}) {
    const where = {
        range
    };

    switch (true) {
        case (range === 'custom' && Boolean(from)):
            where.from = startDay(from);
            where.to = endDay(to);
            break;
        case (range === 'yesterday'):
            where.from = startDay().subtract(1, 'day');
            where.to = endDay().subtract(1, 'day');
            break;
        case (range === 'day'):
            where.from = startDay();
            where.to = endDay();
            break;
        default:
            where.from = startDay().subtract(1, range);
            where.to = endDay();
            break;
    }
    return where;
}

export function isOneDay({from, to}) {
    const diff = to.valueOf() - from.valueOf();
    return diff <= oneDay;
}

function startDay(from) {
    return (from ? moment(from) : moment() ).set(zeroTime);
}

function endDay(to) {
    return ( to ? moment(to) : moment() ).set(finalTime);
}

export function scrollTopPage() {
    window.scrollTo(0, 0);
}


const isAsc = equals('asc');
const reverseIfAsc = (type) => isAsc(type) ? reverse : identity;

export const applyControls = ({sort, size, page}, stats) => {
    var sorted;
    const dropSize = (page - 1) * size;

    if (sort.id) {
        let sortByProp = sortBy(prop(sort.id));
        sorted = pipe(sortByProp, reverseIfAsc(sort.type))(stats);
    } else {
        sorted = stats;
    }

    return pipe(drop(dropSize), take(size))(sorted);
};

export function checkAdminRole() {
    // check user role
}

export function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(response.statusText);

        error.response = response;
        throw error;
    }
}
