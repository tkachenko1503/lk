import React from 'react';
import connectToStore from 'lib/connectToStore';
import LoginPage from 'components/LoginPage';
import {userLogin} from 'actions/user';
import {withRouter} from 'react-router';

class Login extends React.Component {
    loginSubmit(e) {
        e.preventDefault();

        const {email, password} = e.target.elements;
        const name = email.value && email.value.trim();
        const pass = password.value && password.value.trim();

        userLogin({
            username: name,
            password: pass
        });
    }

    render() {
        return (
            <LoginPage
                authSubmit={this.loginSubmit}
                user={this.props.user}
                router={this.props.router}
                location={this.props.location}
            />
        );
    }
}

export default connectToStore(withRouter(Login), state => ({
    user: state.user
}));
