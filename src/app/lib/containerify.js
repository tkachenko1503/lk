import React from 'react';

export default function containerify(methods = {}) {

    return function (WrappedComponent) {
        return React.createClass({
            componentDidMount() {
                if (methods.shouldFetch(this.props)) {
                    methods.fetch(this.props);
                }
            },

            render() {
                return (
                    <WrappedComponent {...this.props} />
                );
            }
        });
    }
}
