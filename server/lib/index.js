import moment from 'moment';

const oneDay = moment.duration(1, 'days');
const zeroTime = {
    hour: 0,
    minute: 0,
    second: 0
};
const finalTime = {
    hour: 23,
    minute: 59,
    second: 59
};

/**
 * Обработчик ошибок сервера
 * @param err
 * @param req
 * @param res
 * @param next
 */
export function catchError(err, req, res, next) {
    console.error(err.stack);

    res
        .status(err.status || 500)
        .json({
            error: err.message,
            succes: false
        });
}

/**
 * Возвращает объект с диапозоном дат
 * превращает строки в объекты moment
 * @param {String} range
 * @param {String} from
 * @param {String} to
 * @returns {Object}
 */
export function getFromTo({range = 'month', from, to}) {
    return {
        range,
        from: startDay(from),
        to: endDay(to)
    };
}

/**
 * Проверяет является ли диапозон дат меньше или равен одному дню
 * @param {Moment} from
 * @param {Moment} to
 * @returns {Boolean}
 */
export function isOneDay({from, to}) {
    const diff = to.valueOf() - from.valueOf();
    return diff <= oneDay;
}

/**
 * Парсит строку в объект moment с учётом тайм зоны
 * и выставляет время 00:00:00
 * @param {String} from
 * @returns {Moment}
 */
function startDay(from) {
    return moment.parseZone(from).set(zeroTime);
}

/**
 * Парсит строку в объект moment с учётом тайм зоны
 * и выставляет время 23:59:59
 * @param {String} to
 * @returns {Moment}
 */
function endDay(to) {
    return moment.parseZone(to).set(finalTime);
}
