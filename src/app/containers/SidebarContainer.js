import React from 'react';
import {withRouter} from 'react-router';
import connectToStore from 'lib/connectToStore';
import Sidebar from 'components/Sidebar';
import {toggleNavItem} from 'actions/sidebar';

/**
 * Контейнер для сайдбара с навигационным меню
 * @constructor
 */
class SidebarContainer extends React.Component {
    /**
     * Пушим id навигационного итема для скрытия/расскрытия
     * @param e
     */
    toggleNavItem(e) {
        e.preventDefault();
        const navId = e.currentTarget.dataset.navId;
        
        toggleNavItem(navId);
    }
    
    render() {
        const {navItem, router, contextType, isAdmin} = this.props;

        return (
            <Sidebar
                navItem={navItem}
                router={router}
                contextType={contextType}
                isAdmin={isAdmin}
                toggleNavItem={this.toggleNavItem}
            />
        );
    }
}

export default connectToStore(withRouter(SidebarContainer), ({sidebar, app, user}) => ({
    navItem: sidebar.navItem,
    isAdmin: user.isAdmin,
    contextType: app.contextType
}));
