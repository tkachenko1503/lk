import express from "express";
import ads from './ads';

const apiCsvRouter = express.Router();

apiCsvRouter.use('/blurb', ads);


export default apiCsvRouter;
