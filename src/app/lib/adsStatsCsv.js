import moment from 'moment';
import json2csv from 'lib/json2csv';
import {downloadCSVFile} from 'lib/download';

const ADS_CSV_FIELDS = [
    {label: 'Дата', value: createdFormat},
    {label: 'Загрузки', value: 'widget_load'},
    {label: 'Показы', value: 'ads_shows'},
    {label: 'Клики', value: 'ads_referrer'},
    {label: 'Доход', value: 'profit'},
    {label: 'Доход за 1000 показов', value: 'profit_thousand_shows'},
    {label: 'Цена за клик', value: 'click_cost'}
];

function createdFormat(row) {
    return moment(row.created).format('DD/MM/YYYY');
}

function generateCSVData(rows, currentSite, currentWidget) {
    const siteAndWidget = [
        {label: 'Вебсайт', value: 'site', default: currentSite},
        {label: 'Виджет', value: 'widget', default: (currentWidget || 'All')}
    ];

    return json2csv({
        del: '.',
        defaultValue: 0,
        data: rows,
        fields: siteAndWidget.concat(ADS_CSV_FIELDS)
    });
}

export function downloadAdsCSV({rows, datesRange, currentSite, currentWidget}) {
    generateCSVData(rows, currentSite, currentWidget)
        .then(data => downloadCSVFile({
            name: currentSite,
            range: datesRange,
            url: encodeURI("data:text/csv;charset=utf-8," + data),
            widget: currentWidget
        }));
}
