import express from 'express';

import stats from './stats';
import sites from './sites';
import ads from './ads';
import csv from './csv';
import articles from './articles';
import advertisers from './advertisers';
import statistics from './statistics/routes';
import accounts from '../modules/accounts/routes';

const apiRouter = express.Router();

apiRouter.use('/stats', stats);
apiRouter.use('/sites', sites);
apiRouter.use('/blurb', ads);
apiRouter.use('/csv', csv);
apiRouter.use('/articles', articles);
apiRouter.use('/advertisers', advertisers);

apiRouter.use('/statistics', statistics);

apiRouter.use('/accounts', accounts);

export default apiRouter;
