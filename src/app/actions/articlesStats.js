import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import {getSiteArticlesStats} from 'queries/index';
import actions$ from 'actions/index';
import {App, Articles} from 'actions/types';
import {downloadArticlesCSV} from 'lib/articlesStatsCsv';
import {logger} from 'lib/index';

export const requestArticlesStatsForSite = flyd.stream();
export const setArticlesStatsForSite = flyd.stream();
export const setArticlesControl = flyd.stream();
export const exportArticlesCsvFile = flyd.stream();

flyd.on(setArticlesStatsForSite, requestArticlesStatsForSite.map(getSiteArticlesStats));
flyd.on(downloadArticlesCSV, exportArticlesCsvFile);

flyd.on(
    pipe(App.Articles, actions$),
    mergeAll([
        setArticlesControl
            .map(Articles.SetControlValue),

        requestArticlesStatsForSite
            .map(Articles.RequestArticlesStats),

        setArticlesStatsForSite
            .map(Articles.SetArticles)
    ])
);
