import React from 'react';
import {Router} from 'react-router';
import juxt from 'ramda/src/juxt';
import connectToStore from 'lib/connectToStore';

class RouterContainer extends React.Component {
    constructor(props) {
        super(props);

        this.updateHandler = this.updateHandler.bind(this);
        this.combinedHandlers = juxt(props.updateHandlers);
    }

    updateHandler() {
        this.combinedHandlers(this.props);
    }

    render() {
        const {history, children} = this.props;

        return (
            <Router
                history={history}
                onUpdate={this.updateHandler}
            >
                {children}
            </Router>
        );
    }
}

export default connectToStore(RouterContainer, ({app}) => ({
    currentSite: app.selectedContext
}));
