import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import { getSiteStats, getSiteStatsCSV, getDashboardStatisticsPdf } from '../queries/index';
import actions$ from './index';
import { App, Stats } from './types';
import { getCSVParams } from '../lib/csv';
import { downloadCSVFile } from '../lib/download';

export const requestStatsForSite = flyd.stream();
export const setStatsForSite = flyd.stream();
export const downloadCSV = flyd.stream();
export const downloadStatisticsPdf = flyd.stream();

flyd.on(setStatsForSite, requestStatsForSite.map(getSiteStats));
flyd.on(
    downloadCSVFile,
    downloadCSV
        .map(getSiteStatsCSV)
        .map(getCSVParams)
);
flyd.on(getDashboardStatisticsPdf, downloadStatisticsPdf);

flyd.on(
    pipe(App.Stats, actions$),
    mergeAll([
        requestStatsForSite.map(Stats.RequestStats),
        setStatsForSite.map(Stats.SetStats),
        downloadCSV.map(Stats.DownloadCSV)
    ])
);

