import React from 'react';
import Pagination from 'react-bootstrap/lib/Pagination';
import Button from 'react-bootstrap/lib/Button';
import Input from 'react-bootstrap/lib/Input';
import StatsTable from 'components/StatsTable';

import adsStatsTable from 'styles/adsStatsTable.css';

const HEADERS = [
    {id: 'url', title: 'Статья'},
    {id: 'show', title: 'Показы'},
    {id: 'load', title: 'Клики'},
    {id: 'ctr', title: 'CTR', formatter: v => `${v}%`}
];

class PopularArticlesTable extends React.Component {
    render() {
        return (
            <div>
                <ExportAdsPanel
                    exportTableInCsv={this.props.exportTableInCsv}
                />

                <StatsTable
                    rows={this.props.rows}
                    rowsSummary={false}
                    tableSort={this.props.tableSort}
                    datesRange={this.props.datesRange}
                    headers={HEADERS}
                    fieldsOnly={true}
                    setTableSort={this.props.setTableSort}
                />

                <AdsTablePagerAndSize
                    pagerSize={this.props.pagerSize}
                    activePage={this.props.activePage}
                    currentSize={this.props.currentSize}
                    setTableSize={this.props.setTableSize}
                    setTablePage={this.props.setTablePage}
                />
            </div>
        );
    }
}

export default PopularArticlesTable;

const ExportAdsPanel = ({exportTableInCsv}) => {
    return (
        <div className={adsStatsTable.csvExportPanel}>
            <Button onClick={exportTableInCsv}>
                в CSV
            </Button>
        </div>
    );
};

const OPTIONS = [10, 20, 50];

const AdsTablePagerAndSize = (props) => {
    const needArrows = props.pagerSize > 4;

    return (
        <div className={adsStatsTable.tableSizePanel}>
            <div className={adsStatsTable.tableSizeLabel}>
                Размер таблицы
            </div>

            <div className={adsStatsTable.tableSize}>
                <Input
                    className={adsStatsTable.tableSizeSelect}
                    type="select"
                    onChange={props.setTableSize}
                    defaultValue={props.currentSize}
                >
                    {OPTIONS.map(SizeOption)}
                </Input>
            </div>

            {props.pagerSize
                ? (
                    <div className={adsStatsTable.tablePager}>
                        <Pagination
                            first={needArrows}
                            last={needArrows}
                            maxButtons={4}
                            items={props.pagerSize}
                            activePage={props.activePage}
                            onSelect={props.setTablePage}
                        />
                    </div>
                )
                : null
            }
        </div>
    );
};

const SizeOption = (option, i) => (
    <option
        key={i}
        value={option}
    >
        {option}
    </option>
);