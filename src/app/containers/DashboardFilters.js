import React from 'react';
import connectToStore from 'lib/connectToStore';
import DashboardPageHead from 'components/DashboardPageHead';
import {setFilter, applyCurrentFilters, applyWhenFiltersValid} from 'actions/filters';
import {fetchWidgets} from 'actions/widgets';
import {setAppContext} from 'modules/appContext/actions';
import AdsCampaignsDataContainer from 'modules/adsCampaigns/AdsCampaignsDataContainer';
import {findByProp} from 'lib/index';
import filter from 'ramda/src/filter';
import propEq from 'ramda/src/propEq';

class DashboardFilters extends React.Component {
    constructor(props) {
        super(props);

        this.setFromDate = this.setDate('from');
        this.setToDate = this.setDate('to');
        this.applyFilters = this.applyFilters.bind(this);
    }

    componentDidMount() {
        const {currentSite, actionForApplyFilters} = this.props;

        if (currentSite) {
            fetchWidgets(currentSite);
        }
        this.contextChangeHandler = setAppContext.map(fetchWidgets);
        this.dropWidgetOnSiteChange = setAppContext.map(() => setFilter({
            filterType: 'widget',
            widgetId: null
        }));
        this.requestStatsOnApplyFilters = applyWhenFiltersValid.map(actionForApplyFilters);
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
        this.dropWidgetOnSiteChange.end(true);
        this.requestStatsOnApplyFilters.end(true);
    }

    setDate(type) {
        return value => {
            setFilter({
                filterType: 'date',
                range: 'custom',
                [type]: value
            });
        };
    }

    setFilter(e) {
        const filter = e.target.dataset.filterId;

        setFilter({
            filterType: 'date',
            range: filter
        });
    }

    setWidgetFilter(e, widgetId) {
        setFilter({
            filterType: 'widget',
            widgetId
        });
    }

    setCampaignFilter(e, campaignId) {
        setFilter({
            filterType: 'campaign',
            campaignId
        });
    }

    applyFilters() {
        const siteId = this.props.currentSite;
        const site = findByProp(siteId, this.props.availableSites, 'id');

        if (site) {
            applyCurrentFilters({
                siteId: siteId,
                site: site.className === 'Site' ? site.get('name') : site.id,
                filters: this.props.filters,
                contextType: site.className
            });
        }
    }

    render() {
        let widgets;
        const {filters, currentSite, widgetsList, showWidgetSelect,
                contextType, campaignsList, widgetSelectWithIcon, allWidgetTypes} = this.props;

        if (!allWidgetTypes) {
            widgets = filter(w => w.get('format') === 'default', widgetsList);
        } else {
            widgets = widgetsList;
        }

        return (
            <div>
                <AdsCampaignsDataContainer />
                <DashboardPageHead
                    widgetSelectWithIcon={widgetSelectWithIcon}
                    filters={filters}
                    currentSite={currentSite}
                    widgetsList={widgets}
                    campaignsList={campaignsList}
                    contextType={contextType}
                    setFilter={this.setFilter}
                    setFromDate={this.setFromDate}
                    setToDate={this.setToDate}
                    setWidgetFilter={this.setWidgetFilter}
                    setCampaignFilter={this.setCampaignFilter}
                    applyFilters={this.applyFilters}
                    showWidgetSelect={showWidgetSelect}
                />
            </div>
        );
    }
}

export default connectToStore(DashboardFilters, ({app, filters, widgets, adsCampaigns}) => ({
    currentSite: app.selectedContext,
    availableSites: app.context,
    contextType: app.contextType,
    filters: filters,
    widgetsList: widgets.list,
    campaignsList: adsCampaigns.adsCampaignsList
}));
