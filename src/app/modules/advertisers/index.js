import Advertiser from 'models/Advertiser';
import {checkStatus} from 'lib/index';

export function isAdvertiserInstance(model) {
    return model instanceof Advertiser;
}

export function uploadStatsFile({advertisersStats, advertiser, campaign}) {
    const data = new FormData();

    data.append('advertisersStats', advertisersStats);
    data.append('advertiser', advertiser);
    data.append('campaign', campaign);

    const fetchProps = {
        method: 'POST',
        body: data
    };

    fetch('/api/advertisers/upload-stats', fetchProps)
        .then(checkStatus)
        .catch(error => ({uploaded: false}))
}