import Parse from 'parse';
import Advertiser from 'models/Advertiser';
import BaseModel from 'models/Base';

const className = 'AdsCampaign';

class AdsCampaign extends BaseModel {
    constructor(attrs) {
        super(className);

        if (attrs) {
            this.set(attrs);
        }
    }

    /**
     * @param name
     * @param advertiserId
     * @returns {Parse.Promise}
     */
    static spawn({name, advertiserId}) {
        const campaign = new AdsCampaign();
        const advertiser = new Advertiser();

        advertiser.id = advertiserId;
        campaign.set('name', name);
        campaign.set('advertiser', advertiser);

        return campaign.save(null);
    }

    /**
     * @param advertiserId
     * @returns {Parse.Promise}
     */
    static getByAdvertiser(advertiserId) {
        const advertiser = new Advertiser();
        const query = new Parse.Query(AdsCampaign);

        advertiser.id = advertiserId;
        query
            .equalTo('advertiser', advertiser)
            .notEqualTo('removed', true);

        return query.find();
    }
}

export default AdsCampaign;

Parse.Object.registerSubclass('AdsCampaign', AdsCampaign);
