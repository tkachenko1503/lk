import React from 'react';
import curry from 'ramda/src/curry';
import curryN from 'ramda/src/curryN';
import Table from 'react-bootstrap/lib/Table';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Input from 'react-bootstrap/lib/Input';
import Label from 'react-bootstrap/lib/Label';
import Modal from 'react-bootstrap/lib/Modal';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import ABTestSettindsEditFormContainer from 'containers/ABTestSettindsEditFormContainer';
import EscKeyClosable from 'components/EscKeyClosable';

import abTestListStyles from 'styles/abTestList.css';
import removeModalStyles from 'styles/removeModal.css';

const ABTestsList = (props) => {
    const {availableTests, setTestAction, currentAction} = props;
    const {renameTest, downloadTestCode, resetAction} = props;
    const {removeDialog, removeTest, closeRemoveTest, askRemoveTest} = props;

    const handlers = {
        actionHandler: makeActionHandler(setTestAction, resetAction),
        renameABTest: renameABTest(renameTest),
        removeTest: removeABTest(askRemoveTest),
        downloadTestCode,
        resetAction: resetActionByTestId(resetAction)
    };
    const rowWithAction = ABTestListRow(handlers, currentAction);

    return (
        <div>
            <Table striped condensed responsive
                className={abTestListStyles.mainTable}
            >
                <tbody>
                    {availableTests.map(rowWithAction)}
                </tbody>
            </Table>

            <Modal
                show={removeDialog.open}
                onHide={closeRemoveTest}
                animation={false}
            >
                <Modal.Body>
                    <h4 className={removeModalStyles.message}>
                        Вы уверенны что хотите удалить этот тест?
                    </h4>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={removeTest}>Да</Button>
                    <Button onClick={closeRemoveTest}>Нет</Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default ABTestsList;


const ABTestListRow = curry((handlers, currentAction, test) => {
    const testName = test.get('name');
    const isActiveTest = currentAction.id === test.id;

    return (
        <tr key={test.id}>
            <td className={abTestListStyles.mainTableCol}>
                <Table className={abTestListStyles.innerTable}>
                    <tbody>
                        <tr>
                                {isActiveTest && currentAction.name === 'rename'
                                    ? (
                                        <RenameABTestAction
                                            name={testName}
                                            renameTest={handlers.renameABTest(test.id)}
                                            resetAction={handlers.resetAction(test.id)}
                                        />
                                    )
                                    : [
                                        <td key="1" className={abTestListStyles.nameCol}>
                                            <span>{testName}</span>
                                        </td>,
                                        <td key="2" className={abTestListStyles.idCol}>
                                            ab-{test.id}
                                        </td>
                                    ]}

                            <td className={abTestListStyles.actionsCol}>
                                <Nav
                                    activeKey={null}
                                    bsStyle="pills"
                                    onSelect={handlers.actionHandler(test.id)}
                                >
                                    <NavItem eventKey="rename">
                                        Переименовать
                                    </NavItem>

                                    {
                                        isActiveTest && currentAction.name === 'download' ?
                                        (
                                            <NavItem>
                                                 Скрыть код теста
                                            </NavItem>
                                        ) :
                                        (
                                            <NavItem eventKey="download">
                                                Показать код теста
                                            </NavItem>
                                        )
                                    }

                                    <NavItem eventKey="editWidgetSettings">
                                        Управление виджетами в тесте
                                    </NavItem>
                                </Nav>
                            </td>
                            <td className={abTestListStyles.removeCol}>
                                <Button
                                    className={abTestListStyles.removeBtn}
                                    onClick={handlers.removeTest(test)}
                                >
                                    <Glyphicon glyph="trash"/>
                                </Button>
                            </td>
                        </tr>
                        {isActiveTest && currentAction.name !== 'rename'
                            ? (
                                <tr>
                                    <td colSpan={4}>
                                        {activeTestAction(currentAction.name, test, handlers)}
                                    </td>
                                </tr>
                            )
                            : null}
                    </tbody>
                </Table>
            </td>
        </tr>
    )
});



const ABTestAction = (props) => {
    return (
        <NavItem
            key={props.key}
            eventKey={props.key}
        >
            {props.title}
        </NavItem>
    );
};

class RenameABTestAction extends EscKeyClosable {
    constructor(props) {
        super(props);
        this.closeAction = props.resetAction;
    }

    render() {
        const {name, renameTest} = this.props;
        const onSubmit = extractAndPushValue(renameTest);

        return (
            <td
                className={abTestListStyles.renameActionCol}
                colSpan={2}
            >
                <form onSubmit={onSubmit}>
                    <Grid fluid={true}>
                        <Row>
                            <Col
                                className={abTestListStyles.testNameInput}
                                xs={7}
                            >
                                <Input
                                    className="qwe"
                                    type="text"
                                    name="abTestName"
                                    placeholder="Имя"
                                    defaultValue={name}
                                    autofocus
                                />
                            </Col>
                            <Col xs={5}>
                                <Button type="submit">
                                    Сохранить
                                </Button>
                            </Col>
                        </Row>
                    </Grid>
                </form>
            </td>
        );
    }
}

class DownloadTestCode extends EscKeyClosable {
    constructor(props) {
        super(props);
        this.closeAction = props.resetAction;
    }

    render() {
        const {test, downloadTestCode} = this.props;

        const code = `<ntvk-ab id="ab-${test.id}"></ntvk-ab>`;
        const download = _ => downloadTestCode({
            txt: code,
            name: `AB_Тест_${test.get('name')}.txt`
        });

        return (
            <div>
                <Label>
                    Код виджета
                </Label>
            <pre>
                {code}
            </pre>
                <Button onClick={download}>
                    Скачать
                </Button>
            </div>
        );
    }
}

const activeTestAction = (name, test, handlers) => {
    switch (name) {
        case 'download':
            return (
                <DownloadTestCode
                    test={test}
                    downloadTestCode={handlers.downloadTestCode}
                    resetAction={handlers.resetAction(test.id)}
                />
            );
            break;
        case 'editWidgetSettings':
            return (<ABTestSettindsEditFormContainer test={test} />);
            break;
    }
};

const makeActionHandler = curry((setTestAction, resetAction, testId, eventKey) => {
    switch (eventKey) {
        case 'rename':
        case 'download':
        case 'editWidgetSettings':
            setTestAction({
                id: testId,
                name: eventKey
            });
            break;
        default:
            resetAction(testId)
    }
});

const renameABTest = curry((renameTest, id, name) => {
    renameTest({
        id,
        name
    });
});

const removeABTest = curry((removeTest, test, e) => {
    removeTest(test);
});

const resetActionByTestId = curryN(3, (resetAction, id, _) => {
    resetAction(id);
});

const extractAndPushValue = f => e => {
    e.preventDefault();
    const {abTestName} = e.target.elements;
    f(abTestName.value);
};

