import React from 'react';
import classnames from 'classnames';
import Panel from 'react-bootstrap/lib/Panel';
import allPass from 'ramda/src/allPass';
import propEq from 'ramda/src/propEq';

import Chart from './Chart';
import StatsChart from './StatsChart';
import { monthlyChartConfig } from '../modules/dashboard/chartConfigs';

import styles from 'styles/dashboard.css';
import chartStyles from 'styles/chart.css';

class MonthlyViewChart extends React.Component {
    render() {
        return (
            <Panel
                bsClass={classnames(
                    styles.chartPanel,
                    "panel"
                )}
                className={styles.shortPanel}
            >
                <ul className={classnames(
                    "monthly-page-view",
                    chartStyles.monthlyViewChartContainer
                )}>
                    <li className="pull-left page-view-label">
                        <span className="page-view-value">{this.props.value}</span>
                        <span>{this.props.title}<br/></span>
                    </li>
                    <li className="pull-right">
                        <div className="chart">
                            <MonthlyChart {...this.props}/>
                        </div>
                    </li>
                </ul>
            </Panel>
        );
    }
}

export default MonthlyViewChart;

class MonthlyChart extends StatsChart {
    isNeedUpdate(newProps) {
        const {referrer, created} = this.props.columns;
        const predicates = [
            propEq('referrer', referrer),
            propEq('created', created)
        ];
        const testProps = allPass(predicates);

        return !testProps(newProps.columns);
    }

    render() {
        const {dateRange} = this.props;
        const withTime = this.isNeedShowTime(dateRange);
        const config = monthlyChartConfig(this.props, withTime);
        return (
            <Chart config={config} redraw={this.needRedraw}/>
        );
    }
}


