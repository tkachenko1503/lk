import React from 'react';
import classnames from 'classnames';
import curry from 'ramda/src/curry';
import Table from 'react-bootstrap/lib/Table';
import Button from 'react-bootstrap/lib/Button';
import Panel from 'react-bootstrap/lib/Panel';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Alert from 'react-bootstrap/lib/Alert';
import Slider from 'components/Slider';
import Layout from 'components/Layout';

import adsTable from 'styles/adsTable.css';
import panel from 'styles/panel.css';
import trafficSettings from 'styles/trafficSettings.css';
import {WIDGET_MAX_PLACES_NUMBER} from 'lib/constants';

const TrafficExchange = ({widgetsList, setExchangePlaces, saveAllChanges, currentSite}) => {
    const panelHeader = (<h4>Настройки обмена трафиком</h4>);

    return (
        <Layout>
            <div className="exchange-ads-container">
                <div className="wrapper">
                    <Panel
                        header={panelHeader}
                        className={panel.fixedHeader}
                    >
                        {currentSite
                            ? (
                                <TrafficExchangeContent
                                    widgetsList={widgetsList}
                                    setExchangePlaces={setExchangePlaces}
                                    saveAllChanges={saveAllChanges}
                                />
                            )
                            : "Выберите сайт"}
                    </Panel>
                </div>
            </div>
        </Layout>
    );
};

export default TrafficExchange;

const TrafficExchangeContent = ({widgetsList, setExchangePlaces, saveAllChanges}) => {
    if (!widgetsList.length) {
        return (
            <h6>Нет доступных виджетов</h6>
        );
    }

    return (
        <div>
            <Grid fluid={true}>
                <Row>
                    <Col xs={9}>
                        <h6>Выберите минимум один виджет для начала обмена</h6>
                    </Col>
                    <Col xs={3}>
                        <Panel
                            className={classnames(
                                panel.fixedHeader,
                                panel.noPadding
                            )}
                        >
                            <Alert
                                bsStyle="info"
                                className={trafficSettings.exchangeCourse}
                            >
                                <b>1 : 1</b>
                                <div>Текущий курс обмена</div>
                            </Alert>
                        </Panel>
                    </Col>
                </Row>
            </Grid>

            <div>
                <TrafficExchangeTable
                    widgetsList={widgetsList}
                    setExchangePlaces={setExchangePlaces}
                />

                <Button
                    className={adsTable.saveButton}
                    onClick={saveAllChanges}
                >
                    Сохранить настройки обмена трафиком
                </Button>
            </div>
        </div>
    );
};

const TrafficExchangeTable = ({widgetsList, setExchangePlaces}) => {
    return (
        <Table
            striped condensed responsive
            className={adsTable.table}
        >
            <thead>
            <tr>
                <th key="num"></th>
                <th key="name">Виджет</th>
                <th key="id">Виджет-ID</th>
                <th key="ads-places">
                    Количество мест для обмена в виджете
                    <i
                        className={classnames(
                            "fa fa-question-circle",
                            adsTable.question
                        )}
                    />
                </th>
            </tr>
            </thead>

            <tbody>
                {widgetsList.map(renderWidgetExchange(setExchangePlaces))}
            </tbody>
        </Table>
    );
};

const renderWidgetExchange = curry(function (setExchangePlaces, widget, i) {
    const exchangePlaces = widget.get('exchangePlaces') || 0;
    const maxPlaces = widget.get('maxPlacesNumber') || WIDGET_MAX_PLACES_NUMBER;

    return (
        <tr key={i}>
            <td className={adsTable.numCol}>{i + 1}</td>
            <td className={adsTable.nameCol}>
                <span className={adsTable.nameText}>
                    {widget.get('name')}
                </span>
            </td>
            <td className={adsTable.idCol}>{widget.id}</td>
            <td className={adsTable.placescol}>
                <Slider
                    step={maxPlaces}
                    value={exchangePlaces}
                    onChange={setExchangePlaces}
                    eventKey={widget.id}
                />
            </td>
        </tr>
    );
});
