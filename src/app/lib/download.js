/**
 * Универсальный хелпер для скачивания файлой по указанному url
 *
 * @param {String} url
 * @param {String} name
 */
export function download(url, name) {
    const link = document.createElement('a');

    link.setAttribute('href', url);
    link.setAttribute('download', name);

    link.click();
}

/**
 * Универсальный хелпер для скачивания текстовых файлов, сгенерированных на клиенте
 *
 * @param {String} name
 * @param {String} txt
 */
export function downloadTxtFile({ name, txt }) {
    const url = encodeURI(`data:text/plain;charset=utf-8,${txt}`);

    download(url, name);
}

/**
 * Формирует имя файла и запускает загрузку файла
 * @param name
 * @param range
 * @param url
 * @param widget
 */
export function downloadCSVFile({ name, range, url, widget }) {
    const fullName = `
        ${name}
        _с_${range.from.format('DD.MM.YYYY')}
        _по_${range.to.format('DD.MM.YYYY')}
        ${widget ? `_виджет_${widget}` : '_все_виджеты'}
        .csv
    `;
    download(url, fullName.replace(/\s/g, ''));
}
