import React from 'react';
import classnames from 'classnames';
import { withRouter } from 'react-router';
import nth from 'ramda/src/nth';
import connectToStore from '../lib/connectToStore';
import { toggleNavItem } from '../actions/sidebar';
import { checkIsAdmin } from '../actions/user';

const second = nth(1);

class App extends React.Component {
    componentDidMount() {
        const { location } = this.props;
        const topMenuItem = second(location.pathname.split('/'));

        if (topMenuItem) {
            toggleNavItem(`/${topMenuItem}`);
        }

        checkIsAdmin(true);
    }

    componentDidUpdate() {
        // Костыль! Должно тригериться через экшн
        const { user, location, router } = this.props;

        if (!user.current && !user.meta.error && location.pathname !== '/login' && location.pathname !== '/register') {
            router.push('/login');
        }
    }

    render() {
        return (
            <div
                className={classnames(
                    'sticky-header app-container',
                    (this.props.sidebar.collapsed ? 'sidebar-collapsed' : 'sidebar-open')
                )}
            >
                {this.props.children}
            </div>
        );
    }
}

export default connectToStore(withRouter(App), state => ({
    user: state.user,
    sidebar: state.sidebar
}));
