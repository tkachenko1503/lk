import React from 'react';
import curry from 'ramda/src/curry';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import ABTestsExistenItem from 'components/ABTestsExistenItem';
import EscKeyClosable from 'components/EscKeyClosable';

import abTestEditForm from 'styles/abTestEditForm.css';
import abTestCreateForm from 'styles/abTestCreateForm.css';

class ABTestSettindsEditForm extends EscKeyClosable {
    constructor(props) {
        super(props);
        this.closeAction = props.cancelChanges;
    }

    render() {
        const {addWidgetToExistenTest, saveExistenTest} = this.props;
        const {setFrequency, removeWidget} = this.props;
        const {testItems, availableWidgets} = this.props;

        const renderTestItemWithHandlers = renderTestItem({
            setFrequency,
            removeWidget
        });

        return (
            <div>
                <Grid fluid={true}>
                    <Row>
                        <Col xs="4">
                            <div className={abTestEditForm.title}>Имя виджета</div>
                        </Col>
                        <Col xs="8">
                            <div className={abTestEditForm.title}>Настройка частоты показов</div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            {testItems.map(renderTestItemWithHandlers)}
                        </Col>
                    </Row>
                    <Row className={abTestEditForm.controlsBlock}>
                        {availableWidgets.length > 0
                            ? [
                                <Col key="1" xs="3">
                                    <Button
                                        onClick={addWidgetToExistenTest}
                                    >
                                        Добавить ещё один виджет
                                    </Button>
                                </Col>,
                                <Col key="2"
                                    xs="1"
                                    className={abTestCreateForm.orBetweenControls}
                                >
                                    или
                                </Col>
                            ]
                            : null}

                        <Col xs="3">
                            <Button
                                onClick={saveExistenTest}
                            >
                                Сохранить A/B тест
                            </Button>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default ABTestSettindsEditForm;

const renderTestItem = curry((handlers, testItem, i, list) => {
    return (
        <ABTestsExistenItem
            key={i}
            index={i}
            testItem={testItem}
            widget={testItem.get('Widget')}
            frequency={testItem.get('frequency')}
            needRemoveButton={list.length > 2}
            setFrequency={handlers.setFrequency}
            removeWidget={handlers.removeAdsCampaign}
        />
    );
});
