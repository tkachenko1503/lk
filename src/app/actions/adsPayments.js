import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import actions$ from 'actions/index';
import {getSiteAdsPayments, getSiteAdsPaymentsCSV} from 'queries/index';
import {App, AdsPayments} from 'actions/types';

export const requestAdsPaymentsForSite = flyd.stream();
export const setAdsPaymentsForSite = flyd.stream();
export const exportAdsPaymentsInCSV = flyd.stream();

flyd.on(setAdsPaymentsForSite, requestAdsPaymentsForSite.map(getSiteAdsPayments));
flyd.on(getSiteAdsPaymentsCSV, exportAdsPaymentsInCSV);

flyd.on(
    pipe(App.AdsPayments, actions$),
    mergeAll([
        requestAdsPaymentsForSite
            .map(AdsPayments.RequestAdsPayments),

        setAdsPaymentsForSite
            .map(AdsPayments.SetAdsPayments)
    ])
);
