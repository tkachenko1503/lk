import React from 'react';
import Layout from 'components/Layout';
import moment from 'moment';
import DashboardFilters from 'containers/DashboardFilters';
import StatsSummaryPanels from 'components/StatsSummaryPanels';
import Table from 'react-bootstrap/lib/Table';
import Panel from 'react-bootstrap/lib/Panel';
import Button from 'react-bootstrap/lib/Button';

import styles from 'styles/AdsPaymentsPage.css';


const paymentsInfoEmail = 'info@natimatica.com';


class AdsPaymentsPage extends React.Component {

    render() {
        const {actionForApplyFilters, exportPaymentsInCsv} = this.props;
        const {summaries, payments} = this.props;
        const {total, daily} = payments;

        return (
            <Layout>
                <DashboardFilters
                    showWidgetSelect={false}
                    actionForApplyFilters={actionForApplyFilters}
                />

                <div className="wrapper">
                    <StatsSummaryPanels
                        summaries={summaries}
                    />

                    <Panel>
                        <div className={styles.paymentsPanel}>
                            <div>
                                <span>Для вывода денег напишите нам письмо с указанием суммы на адрес </span>
                                <a href="mailto:{paymentsInfoEmail}">{paymentsInfoEmail}</a>
                            </div>

                            <div>
                                <Button onClick={exportPaymentsInCsv}>
                                    в CSV
                                </Button>
                            </div>
                        </div>

                        <Table striped bordered>
                            <thead>
                                <tr>
                                    <th>Дата</th>
                                    <th>Начисление</th>
                                    <th>Вывод</th>
                                </tr>
                                <tr>
                                    <th>Всего за период</th>
                                    <th>{total.creditedWith}</th>
                                    <th>{total.cashedOut}</th>
                                </tr>
                            </thead>

                            <tbody>
                                {daily.map(renderDailyPayment)}
                            </tbody>
                        </Table>
                    </Panel>
                </div>
            </Layout>
        );
    }
};


const renderDailyPayment = (payment, index) => (
    <tr key={index}>
        <td>{formatDate(payment.date)}</td>
        <td>{payment.creditedWith}</td>
        <td>{payment.cashedOut}</td>
    </tr>
);


const formatDate = (date) => {
    return moment(date).format('DD/MM/YYYY');
};


export default AdsPaymentsPage;
