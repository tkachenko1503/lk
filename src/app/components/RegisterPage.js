import React from 'react';
import classNames from 'classnames';
import {Link} from 'react-router';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Button from 'react-bootstrap/lib/Button';
import Input from 'react-bootstrap/lib/Input';
import AuthPage from 'components/AuthPage';

import styles from 'styles/auth.css';

class LoginPage extends AuthPage {
    constructor(props) {
        super(props);

        this.title = 'Регистрация';
    }

    renderFormControls() {
        return [
            <FormGroup key="email">
                <Input type="email" name="email" placeholder="Почта" autofocus/>
            </FormGroup>,
            <FormGroup key="password">
                <Input type="password" name="password" placeholder="Пароль"/>
            </FormGroup>,
            <FormGroup key="submit">
                <Button
                    className="btn-block"
                    bsStyle="success"
                    bsSize="large"
                    type="submit"
                >
                    Зарегистрироваться
                </Button>
            </FormGroup>
        ];
    }

    renderFooter() {
        return (
            <div className={classNames(
                "registration",
                styles.toRegister
            )}>
                Уже зарегистрированы?
                <Link to="/login" className={classNames(
                    styles.registerLink
                )}>
                    Вход
                </Link>
            </div>
        );
    }
}

export default LoginPage;
