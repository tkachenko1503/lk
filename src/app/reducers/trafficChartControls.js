import curryN from 'ramda/src/curryN';
import {TrafficChartControls} from 'actions/types';

export function defaultTrafficChartControls() {
    return {
        first: 'ads_shows',
        second: 'ads_referrer'
    };
}

const reducefn = TrafficChartControls.caseOn({
    SetParams: (first, second, state) => ({
        first,
        second
    })
});

export default curryN(2, reducefn);
