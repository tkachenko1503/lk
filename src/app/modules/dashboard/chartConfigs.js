import moment from 'moment';

export function chartConfig({ created, first, second, secondTickFormat, col1, col2 }, withTime) {
    return {
        data: {
            x: 'x',
            xFormat: '%Y-%m-%dT%H:%M:%S.%Z',
            columns: [
                ['x'].concat(created),
                [first].concat(col1),
                [second].concat(col2)
            ],
            types: {
                [first]: 'area'
            },
            axes: {
                [first]: 'y',
                [second]: 'y2'
            },
            colors: {
                [first]: '#6ED594',
                [second]: '#179AD0'
            },
            unload: true
        },
        axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: withTime ? '%d.%m-%H:%M' : '%d.%m',
                    culling: {
                        max: 5
                    }
                }
            },
            y: {
                label: {
                    text: first,
                    position: 'outer-middle'
                },
                padding: {
                    top: 10,
                    bottom: 0
                },
                min: 0
            },
            y2: {
                label: {
                    text: second,
                    position: 'outer-middle'
                },
                tick: {
                    format: secondTickFormat
                },
                padding: {
                    top: 10,
                    bottom: 0
                },
                show: true,
                min: 0
            }
        },
        tooltip: {
            format: {
                value(value, ratio, id) {
                    return id === 'CTR' ? `${value}%` : value.toLocaleString('ru-RU');
                }
            }
        },
        size: {
            height: 268
        }
    };
}

export function monthlyChartConfig(props, withTime) {
    const { referrer, created } = props.columns;

    return {
        data: {
            x: 'x',
            xFormat: '%Y-%m-%dT%H:%M:%S.%Z',
            columns: [
                ['x'].concat(created),
                ['referrer'].concat(referrer)
            ],
            type: 'bar',
            axes: {
                referrer: 'y'
            },
            colors: {
                referrer: props.color
            },
            unload: true
        },
        bar: {
            width: {
                ratio: 0.81
            }
        },
        axis: {
            x: {
                type: 'timeseries',
                show: false
            },
            y: {
                show: false
            }
        },
        legend: {
            show: false
        },
        size: {
            width: 131,
            height: 43
        },
        tooltip: {
            contents: (data) => {
                const value = data[0].value.toLocaleString('ru-RU');
                const format = withTime ? 'DD.MM-HH:mm' : 'DD.MM';
                const created = moment(data[0].x).format(format);

                return `<div 
                        style="border-radius: 5px; 
                        background-color: rgba(0, 0, 0, 0.5); 
                        color: #FFF;
                        padding: 2px 5px;">
                            <div>${value}</div>
                            <div>${created}</div>
                        </div>`;
            }
        }
    };
}
