import React from 'react';
import T from 'ramda/src/T';

/**
 * Need override closeAction instanse method
 */
class EscKeyClosable extends React.Component {
    constructor(props) {
        super(props);
        this.closeAction = T;
        this.handleEscKeydown = this.handleEscKeydown.bind(this);
    }

    componentWillMount() {
        document.addEventListener('keydown', this.handleEscKeydown, false);
    }

    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleEscKeydown, false);
    }

    handleEscKeydown(e) {
        if (e.keyCode == 27) {
            this.closeAction(true);
        }
    }
}

export default EscKeyClosable;
