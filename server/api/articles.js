import express from "express";
import {getArticlesBySite} from '../models/articles';
import {getFromTo} from '../lib/index';

const articles = express.Router();

articles.get('/:site', function (req, res, next) {
    const datesRange = getFromTo(req.query);

    getArticlesBySite({
        site: req.params.site,
        from: datesRange.from.format('YYYY-MM-DD HH:mm:ss'),
        to: datesRange.to.format('YYYY-MM-DD HH:mm:ss'),
        widget: req.query.widget
    })
        .then(statsResult => {
            res.json({
                stats: statsResult
            });
        })
        .catch(next);
});

export default articles;
