import knex from '../lib/knex';
import {reduce, pipe, prop, map} from 'ramda';

const getPGString = reduce(({advertiser, campaign, created_at, values}, stat) => {
    const data = {
        NTVK: {
            shows: Number(stat.shows),
            clicks: Number(stat.clicks),
            ctr: Number(stat.ctr)
        }
    };

    return {
        advertiser,
        campaign,
        created_at,
        values: `${values ? values + ',' : ''} ('${advertiser}', '${campaign}', '${stat.since}', '${JSON.stringify(data)}', '${created_at}')`
    };
});

const statsToPGValues = pipe(
    getPGString,
    prop('values')
);

export function writeStatsToDB({stats, advertiser, campaign, created_at}) {
    const PGValues = statsToPGValues({
        advertiser,
        campaign,
        created_at,
        values: ''
    }, stats);

    return knex.raw(`
        INSERT INTO imported_stats (advertiser, campaign, since, data, created_at) 
        VALUES  ${PGValues} 
        ON CONFLICT (advertiser, since, campaign) 
        DO UPDATE SET 
            data = EXCLUDED.data,
            created_at = EXCLUDED.created_at
    `);
}

const calcStatsFields = (item) => {
    item.profit = Number(item.profit) || 0;
    item.ctr = Number(item.ctr) || 0;
    item.clickCost = Number(item.click_cost) || 0;

    return item;
};

const processAdsMaterials = pipe(prop('rows'), map(calcStatsFields));

export function getAdvertisersContent({site, from, to ,widgetId}) {
    var q = knex.raw(`
        SELECT 
            material_id,
            material_title,
            material_url,
            profit,
            round( ((100 * click) / NULLIF(impression, 0)), 1) AS ctr,
            round( (profit / NULLIF(click, 0)), 1) AS click_cost
        FROM (
            SELECT
                material_id,
                material_title,
                material_url,
                sum(coalesce((data->'ads_revenue')::text::decimal, 0)) AS profit,
                sum(coalesce((data->'ads_impression')::text::decimal, 0)) AS impression,
                sum(coalesce((data->'ads_click')::text::decimal, 0)) AS click
            FROM ad_material_stats
            WHERE site = '${site}' AND
                  widget_id ${widgetId ? ('= \'' + widgetId + '\'') : 'ISNULL'} AND
                  material_id != 'undefined' AND
                  date BETWEEN '${from}' AND '${to}'
            GROUP BY material_id, material_title, material_url
            ORDER BY impression
            DESC
        ) 
        AS st
    `);

    // console.log(q.toSQL());

    return q.then(processAdsMaterials)
            .catch(error => []);
}
