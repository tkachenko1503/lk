import React from 'react';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Dropdown from 'react-bootstrap/lib/Dropdown';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import {findByProp} from 'lib/index';

import widgetSelect from 'styles/widgetSelect.css';

const GLIPH_TYPES = {
    'default': 'th-large',
    'infeed': 'list',
    'intext': 'align-justify'
};

const WidgetSelectWithIcons = (props) => {
    const {widgetsList, choosenWidget, defaultTitle} = props;
    const {widgetSelectHandler, widgetName} = props;

    const widget = findByProp(choosenWidget, widgetsList, 'id');
    let title;

    if (widget) {
        title = widget.get('name');
    } else if (widgetName) {
        title = widgetName;
    } else {
        title = defaultTitle;
    }

    const tooltip = (<Tooltip id={title}>{title}</Tooltip>);
    const gliphType = widget ? GLIPH_TYPES[widget.get('format')] : GLIPH_TYPES['default'];

    return (
        <OverlayTrigger placement="top"
                        overlay={tooltip}>
            <Dropdown id="widgetSelect"
                      onSelect={widgetSelectHandler}>
                <Dropdown.Toggle className={widgetSelect.button}
                                 bsStyle="success"
                                 bsSize="small"
                                 disabled={widgetsList.length ? false : true}>
                    <Glyphicon glyph={gliphType}
                               className={widgetSelect.gliph} />
                    {title}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <MenuItem eventKey={null}
                              key='all'>
                        {defaultTitle}
                    </MenuItem>
                    {widgetsList.map(w => (
                        <MenuItem eventKey={w.id}
                                  key={w.id}>
                            <Glyphicon glyph={GLIPH_TYPES[w.get('format')]}
                                       className={widgetSelect.gliph} />
                            {w.get('name')}
                        </MenuItem>
                    ))}
                </Dropdown.Menu>
            </Dropdown>
        </OverlayTrigger>
    );
};

export default WidgetSelectWithIcons;
