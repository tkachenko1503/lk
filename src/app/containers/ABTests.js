import React from 'react';
import connectToStore from 'lib/connectToStore';
import {findByProp} from 'lib/index';
import {fetchWidgets} from 'actions/widgets';
import {setAppContext} from 'modules/appContext/actions';
import ABTestsPage from 'components/ABTestsPage';
import {makeAction, resetAction, setNewTestName,
        setWidgetId, setWidgetFrequency,
        addWidgetToTest, saveTest, toggleRemoveDialog,
        removeWidgetFromTest, fetchABTests,
        renameABTest, downloadTestCode, removeTest} from 'actions/abTests';

/**
 * Страница для CRUD операций с AB тестами
 * @constructor
 */
class ABTests extends React.Component {
    constructor(props) {
        super(props);

        this.saveABTest = this.saveABTest.bind(this);
        this.renameABTest = this.renameABTest.bind(this);
        this.removeTest = this.removeTest.bind(this);
    }

    componentWillMount() {
        const {currentSite} = this.props;

        if (currentSite) {
            fetchWidgets(currentSite);
        }
        if (currentSite) {
            fetchABTests(currentSite);
        }

        this._fetchWidgetsForCurrentSite = setAppContext.map(fetchWidgets);
        this._fetchABTestsForCurrentSite = setAppContext.map(fetchABTests);
        this._resetActionOnSiteChange = setAppContext.map(_ => resetAction(''));
    }

    componentWillUnmount() {
        this._fetchWidgetsForCurrentSite.end(true);
        this._fetchABTestsForCurrentSite.end(true);
        this._resetActionOnSiteChange.end(true);
    }

    setNewTestName(e) {
        const testName = e.target.value;
        setNewTestName(testName);
    }

    setWidget(data) {
        setWidgetId(data);
    }

    addWidget() {
        addWidgetToTest(true);
    }

    removeWidget(e) {
        const {index} = e.currentTarget.dataset;
        removeWidgetFromTest(Number(index));
    }

    setFrequency(data) {
        setWidgetFrequency(data)
    }

    createABTest() {
        makeAction({
            name: 'create'
        })
    }

    setTestAction({name, id}) {
        makeAction({
            name,
            id
        })
    }

    saveABTest(e) {
        e.preventDefault();
        const {currentSite, testName, testsList} = this.props;
        saveTest({
            siteId: currentSite,
            name: testName,
            items: testsList
        });
    }

    renameABTest({id, name}) {
        const {availableTests} = this.props;
        const test = findByProp(id, availableTests, 'id');

        renameABTest({test, name});
    }

    askRemoveTest(test) {
        toggleRemoveDialog({
            test,
            open: true
        });
    }

    closeRemoveTest() {
        toggleRemoveDialog({
            test: null,
            open: false
        });
    }

    removeTest() {
        const {test} = this.props.removeDialog;

        removeTest(test);
        this.closeRemoveTest()
    }

    render() {
        const {abAction, testsList, testName, canSaveTest} = this.props;
        const {widgetsList, availableTests, testError, removeDialog} = this.props;

        return (
            <ABTestsPage
                createABTest={this.createABTest}
                setNewTestName={this.setNewTestName}
                setWidget={this.setWidget}
                setFrequency={this.setFrequency}
                addWidget={this.addWidget}
                saveABTest={this.saveABTest}
                removeWidget={this.removeWidget}
                setTestAction={this.setTestAction}
                renameTest={this.renameABTest}
                closeRemoveTest={this.closeRemoveTest}
                askRemoveTest={this.askRemoveTest}
                removeTest={this.removeTest}
                resetAction={resetAction}
                downloadTestCode={downloadTestCode}
                currentAction={abAction}
                testsList={testsList}
                testName={testName}
                availableTests={availableTests}
                widgetsList={widgetsList}
                testError={testError}
                canSaveTest={canSaveTest}
                removeDialog={removeDialog}
            />
        );
    }
}

export default connectToStore(ABTests, ({app, widgets, user, abTests}) => ({
    currentSite: app.selectedContext,
    currentUser: user.current,
    abAction: abTests.action,
    testsList: abTests.newTest.items,
    testName: abTests.newTest.name,
    testError: abTests.newTest.error,
    canSaveTest: abTests.newTest.canSaveTest,
    widgetsList: widgets.list,
    availableTests: abTests.list,
    removeDialog: abTests.removeDialog
}));
