import React from 'react';
import classnames from 'classnames';
import Table from 'react-bootstrap/lib/Table';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import moment from 'moment';
import curry from 'ramda/src/curry';
import pluck from 'ramda/src/pluck';

import adsStatsTable from 'styles/adsStatsTable.css';

const StatsTable = (props) => {
    const {rows, rowsSummary, tableSort, setTableSort, datesRange, headers, fieldsOnly, withHeadClass} = props;
    const sortedHeaders = THead({sorter: tableSort, setSort: setTableSort, withHeadClass});
    const rowComponent = fieldsOnly ? TRowOnlyFields(headers) : TRow;

    return (
        <Table
            striped bordered condensed
            className={classnames(
                adsStatsTable.table
            )}
        >
            <thead>
                <tr>
                    {headers.map(sortedHeaders)}
                </tr>
            </thead>
            <tbody>
                {rowsSummary
                    ? (
                        <TSummaryRow
                            rowsSummary={rowsSummary}
                            datesRange={datesRange}
                        />
                    )
                    : null}
                {rows && rows.length ? rows.map(rowComponent) : null}
            </tbody>
        </Table>
    );
};

export default StatsTable;

const THead = curry(({sorter, setSort, withHeadClass}, {title, id}, i) => {
    const tooltip = (<Tooltip id={title}>{title}</Tooltip>);
    const isActive = sorter.id === id;
    const classes = [
        adsStatsTable.sorting,
        isActive && adsStatsTable.active,
        isActive && sorter.type === 'desc' ? adsStatsTable.desc : adsStatsTable.asc
    ];

    return (
        <th 
            className={classnames(classes)} 
            key={i}
            onClick={setSort}
            data-sort-id={id}
        >
            <OverlayTrigger placement="top" overlay={tooltip}>
                <span className={withHeadClass ? adsStatsTable.tHead : ''}>
                    {title}
                </span>
            </OverlayTrigger>
        </th>
    );
});

const TSummaryRow = ({rowsSummary, datesRange}) => {
    const from = datesRange.from.format('DD/MM/YYYY');
    const to = datesRange.to.format('DD/MM/YYYY');

    return (
        <tr className="info">
            <td className={adsStatsTable.nameCol}>
                <span className={adsStatsTable.nameText}>
                    Итого
                </span>
            </td>
            <td className={adsStatsTable.createdCol}>
                {from} - {to}
            </td>
            <td>{rowsSummary.widget_load}</td>
            <td>{rowsSummary.ads_shows}</td>
            <td>{rowsSummary.ads_referrer}</td>
            <td>{rowsSummary.ads_profit}</td>
            <td>{rowsSummary.ads_profit_thousand_shows}</td>
            <td>{rowsSummary.ads_profit_mean}</td>
        </tr>
    );
};

const TRow = (row, i) => {
    const name = row.widget_name || 'Все форматы';
    const created = moment(row.created).format('DD/MM/YYYY');

    return (
        <tr key={i}>
            <td>{name}</td>
            <td className={adsStatsTable.createdCol}>
                {created}
            </td>
            <td>{row.widget_load}</td>
            <td>{row.ads_shows}</td>
            <td>{row.ads_referrer}</td>
            <td>{row.ads_profit}</td>
            <td>{row.ads_profit_thousand_shows}</td>
            <td>{row.ads_profit_mean}</td>
        </tr>
    );
};

const TRowOnlyFields = curry((headers, row, i) => {
    return (
        <tr key={i}>
            {headers.map(({id, formatter}, j) => (
                <td key={j}>
                    {formatter ? formatter(row[id], row) : row[id]}
                </td>
            ))}
        </tr>
    );
});
