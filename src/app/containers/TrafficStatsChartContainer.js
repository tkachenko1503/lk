import React from 'react';
import connectToStore from 'lib/connectToStore';
import TrafficStatsChart from 'components/TrafficStatsChart';
import {setParams} from 'actions/trafficChartControls';
import {TRAFFIC_CHART_BUTTONS} from 'lib/constants';
import {findByProp} from 'lib/index';
import partialRight from 'ramda/src/partialRight';
import pipe from 'ramda/src/pipe';
import prop from 'ramda/src/prop';
import pluck from 'ramda/src/pluck';

/**
 * Граффик на странице статистики обмена
 * @constructor
 */
class TrafficStatsChartContainer extends React.Component {
    changeFields(fields) {
        setParams({
            first: fields.first,
            second: fields.second
        });
    }

    render() {
        const {controls, created, dateRange} = this.props;
        const {firstColunmData, secondColunmData} = this.props;
        const {firstColunmTitle, secondColunmTitle} = this.props;

        return (
            <div>
                <TrafficStatsChart
                    created={created}
                    first={firstColunmTitle}
                    second={secondColunmTitle}
                    col1={firstColunmData}
                    col2={secondColunmData}
                    dateRange={dateRange}
                    controls={controls}
                    changeFields={this.changeFields}
                />
            </div>
        );
    }
}

const getChartControl = partialRight(findByProp, [TRAFFIC_CHART_BUTTONS, 'id']);
const getTitle = pipe(getChartControl, prop('title'));

export default connectToStore(TrafficStatsChartContainer, ({trafficChartControls, adsStats, filters}) => ({
    controls: trafficChartControls,
    // // data
    firstColunmData: pluck(trafficChartControls.first, adsStats.statsList),
    secondColunmData: pluck(trafficChartControls.second, adsStats.statsList),
    // // titles
    firstColunmTitle: getTitle(trafficChartControls.first),
    secondColunmTitle: getTitle(trafficChartControls.second),
    // // dates
    created: pluck('created', adsStats.statsList),
    dateRange: filters.date
}));
