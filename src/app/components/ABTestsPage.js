import React from 'react';
import Panel from 'react-bootstrap/lib/Panel';
import Layout from 'components/Layout';
import ABTestCreateButton from 'components/ABTestCreateButton';
import ABTestsCreateForm from 'components/ABTestsCreateForm';
import ABTestsList from 'components/ABTestsList';

import panel from 'styles/panel.css';

const ABTestsPage = (props) => {
    const {testName, testsList, setNewTestName, testError} = props;
    const {currentAction, createABTest, widgetsList, canSaveTest} = props;
    const {setWidget, setFrequency, addWidget, saveABTest} = props;
    const {removeWidget, availableTests, setTestAction} = props;
    const {renameTest, downloadTestCode, removeTest, resetAction} = props;
    const {removeDialog, closeRemoveTest, askRemoveTest} = props;

    return (
        <Layout>
            <div className="ab-tests-container">
                <div className="wrapper">
                    <Panel
                        className={panel.fixedHeader}
                    >
                        <ABTestCreateButton
                            createABTest={createABTest}
                        />

                        {currentAction.name === 'create'
                            ? (
                                <ABTestsCreateForm
                                    testsList={testsList}
                                    testName={testName}
                                    testError={testError}
                                    widgetsList={widgetsList}
                                    setNewTestName={setNewTestName}
                                    setWidget={setWidget}
                                    setFrequency={setFrequency}
                                    addWidget={addWidget}
                                    saveABTest={saveABTest}
                                    removeWidget={removeWidget}
                                    resetAction={resetAction}
                                    canSaveTest={canSaveTest}
                                />
                            )
                            : null}

                        <ABTestsList
                            currentAction={currentAction}
                            availableTests={availableTests}
                            removeDialog={removeDialog}
                            closeRemoveTest={closeRemoveTest}
                            askRemoveTest={askRemoveTest}
                            setTestAction={setTestAction}
                            renameTest={renameTest}
                            downloadTestCode={downloadTestCode}
                            removeTest={removeTest}
                            resetAction={resetAction}
                        />
                    </Panel>
                </div>
            </div>
        </Layout>
    );
};

export default ABTestsPage;
