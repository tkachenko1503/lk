import React from 'react';
import curry from 'ramda/src/curry';
import find from 'ramda/src/find';
import propEq from 'ramda/src/propEq';
import without from 'ramda/src/without';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Input from 'react-bootstrap/lib/Input';
import Button from 'react-bootstrap/lib/Button';
import Alert from 'react-bootstrap/lib/Alert';
import EscKeyClosable from 'components/EscKeyClosable';
import ABTestsItem from 'components/ABTestsItem';

import abTestCreateForm from 'styles/abTestCreateForm.css';

class ABTestsCreateForm extends EscKeyClosable {
    constructor(props) {
        super(props);
        this.closeAction = this.resetAction;
    }

    resetAction() {
        this.props.resetAction('');
    }
    
    render() {
        const {widgetsList, setWidget} = this.props;
        const {testsList, testName, testError, setNewTestName, canSaveTest} = this.props;
        const {setFrequency, addWidget, saveABTest} = this.props;
        const {removeWidget} = this.props;

        const renderTest = renderTestWithWidgetsList({
            widgetsList,
            removeWidget,
            setWidget: setWidgetWithIndex(setWidget),
            setFrequency: setFrequencyWithIndex(setFrequency),
            needRemoveButton: testsList.length > 2
        });

        return (
            <div className={abTestCreateForm.main}>
                <form onSubmit={saveABTest}>
                    <Grid fluid={true} className={abTestCreateForm.noPadding}>
                        {testError && testError.message
                            ? (
                                <Row>
                                    <Col xs={6}>
                                        <Alert bsStyle="danger">{testError.message}</Alert>
                                    </Col>
                                </Row>
                            )
                            : null}
                        <Row>
                            <Col xs={4}>
                                <Input
                                    className="qwe"
                                    type="text"
                                    name="abTestName"
                                    placeholder="Введите название A/B теста"
                                    defaultValue={testName}
                                    onChange={setNewTestName}
                                    autofocus
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <h6>
                                    Выберите минимум два виджета для теста
                                    и задайте частоту их показов
                                </h6>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                {testsList.map(renderTest)}
                            </Col>
                        </Row>
                        <Row className={abTestCreateForm.controlsBlock}>
                            <Col xs={3}>
                                <Button
                                    onClick={addWidget}
                                >
                                    Добавить ещё один виджет
                                </Button>
                            </Col>
                            <Col
                                xs={1}
                                className={abTestCreateForm.orBetweenControls}
                            >
                                или
                            </Col>
                            <Col xs={3}>
                                <Button
                                    type="submit"
                                    disabled={!canSaveTest}
                                >
                                    Сохранить A/B тест
                                </Button>
                            </Col>
                        </Row>
                    </Grid>
                </form>
            </div>
        );
    }
}

export default ABTestsCreateForm;

const renderTestWithWidgetsList = curry((params, test, i) => {
    const {widgetsList, removeWidget, setWidget, setFrequency, needRemoveButton} = params;

    const selectedWidget = find(propEq('id', test.widgetId), widgetsList);
    const widgetsListWhithoutSelected = selectedWidget ? without([selectedWidget], widgetsList) : widgetsList;

    return (
        <ABTestsItem
            key={i}
            widgetsList={widgetsListWhithoutSelected}
            test={test}
            selectedName={selectedWidget ? selectedWidget.get('name') : null}
            setWidget={setWidget(i)}
            setFrequency={setFrequency(i)}
            needRemoveButton={needRemoveButton}
            removeWidget={removeWidget}
            index={i}
        />
    );
});

const setWidgetWithIndex = curry(function (handler, index, e, widgetId) {
    handler({
        index,
        widgetId
    });
});

const setFrequencyWithIndex = curry(function (handler, index, widgetId, frequency) {
    handler({
        index,
        frequency
    });
});
