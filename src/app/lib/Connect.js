import React from 'react';

/**
 * Определяет childContext в котором находится
 * стрим представляющий собой Store приложения
 * Доступ к этому стриму получают контейнеры
 * созданные с помощью функции connectToStore
 */
class Connect extends React.Component {
    static get propTypes() {
        return {
            children: React.PropTypes.element.isRequired,
            store$: React.PropTypes.func.isRequired
        };
    }

    static get childContextTypes() {
        return {
            store$: React.PropTypes.func
        };
    }

    getChildContext() {
        return {
            store$: this.store$
        };
    }

    constructor(props, context) {
        super(props, context);
        this.store$ = props.store$;
    }

    render() {
        let { children } = this.props;
        return React.Children.only(children);
    }
}

export default Connect;
