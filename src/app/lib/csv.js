import moment from 'moment';
import groupBy from 'ramda/src/groupBy';
import indexBy from 'ramda/src/indexBy';
import prop from 'ramda/src/prop';
import map from 'ramda/src/map';
import pipe from 'ramda/src/pipe';
import values from 'ramda/src/values';
import reduce from 'ramda/src/reduce';
import curry from 'ramda/src/curry';
import pluck from 'ramda/src/pluck';
import uniq from 'ramda/src/uniq';
import filter from 'ramda/src/filter';
import flatten from 'ramda/src/flatten';
import __ from 'ramda/src/__';
import queryString from 'query-string'
import json2csv from 'lib/json2csv';
import {findByProp} from 'lib/index';
import {getFromTo, isOneDay} from 'lib/index';

/**
 * Возвращает функцию для форматирования ctr
 * @param {String|null} name имя виджета если надо
 * @returns {Function}
 */
const formatCtrField = (name) => (v) => {
    // Заменяем точки на запятые и добавляем %
    const field = name ? `${name}/ctr` : 'ctr';
    const res = v[field] && v[field].toString
        ? v[field].toString().replace('.', ',')
        : '0';

    return res + '%';
};

const groupByDate = groupBy(prop('date'));
const toFlatStatsCurried = curry(toFlatStats);
// Получая данные мы должны закаррировать их в функции
// Которая будет аггрегатором в reduce
const flattingStats = pipe( toFlatStatsCurried, reduce(__, {}) );
const groupForCSV = pipe(groupByDate, values);
const extractWidgetsIds = pipe(pluck('widget_id'), uniq, filter(Boolean));
// Получаем массив с именами колонок с префиксом из имени виджета
const getFieldsWithWidgetName = (name) => [{
        label: `${name}/Загрузки`,
        value: `${name}/load`
    }, {
        label: `${name}/Показы`,
        value: `${name}/impression`
    }, {
        label: `${name}/Клики`,
        value: `${name}/clicks`
    }, {
        label: `${name}/CTR`,
        value: formatCtrField(name)
    }
];
const getWidgetFields = pipe( map(getFieldsWithWidgetName), flatten);

/**
 * Формирует квери строку для запросов статистики
 * @param params
 * @returns {String}
 */
export function statsQS(params) {
    const {range, from, to} = params.filters.date;
    const {widget, campaign} = params.filters;

    return queryString.stringify({
        widget,
        campaign,
        range,
        from: from.format(),
        to: to.format(),
        type: params.type || null,
        contextType: params.contextType
    });
}

/**
 * Формирует данные для генерации csv файла
 * @param params
 * @param data
 * @returns {Promise}
 */
export function getCSVParams({params, data}) {
    const {filters, widgetList, site,
            campaignList, contextType, context} = params;

    if (contextType === 'Site') {
        let widget = filters.widget && findByProp(filters.widget, widgetList, 'id');

        return generateCSV(params, data)
            .then(urlData => ({
                url: encodeURI("data:text/csv;charset=utf-8," + urlData),
                name: site,
                range: filters.date,
                widget: widget ? widget.get('name') : null
            }));
    } else {
        let advertiser = findByProp(site, context, 'id');
        let campaign = findByProp(filters.campaign, campaignList, 'id');

        return generateAdvertiserCSV(params, data)
            .then(urlData => ({
                url: encodeURI("data:text/csv;charset=utf-8," + urlData),
                name: advertiser.get('name'),
                range: filters.date,
                campaign: campaign ? campaign.get('name') : null
            }));
    }
}

/**
 * Функция для форматирования колонки с датой. Не показываем время если диапазон дат больше одного дня
 *
 * @param {Object} rangeDates
 * @param {Moment} rangeDates.from
 * @param {Moment} rangeDates.to
 * @param {Object} row
 * @returns {String}
 */
const formatDateField = curry((rangeDates, row) => {
    const formatString = !isOneDay(rangeDates) ? 'DD/MM/YYYY' : 'DD/MM/YYYY HH:mm';

    return moment(row.date).format(formatString)
});


/**
 * Генерит данные для csv
 * @param type
 * @param widgetList
 * @param filters
 * @param stats
 * @returns {Promise}
 */
function generateCSV({type, widgetList, filters}, stats) {
    const widgets = indexBy(prop('id'), widgetList);
    const statsData = map(flattingStats(widgets), groupForCSV(stats));
    const rangeParams = filters.date.range === 'custom' ? filters.date : {range: filters.date.range};
    const rangeDates = getFromTo(rangeParams);

    // Обычные колонки
    const simpleFields = [{
            label: 'Вебсайт',
            value: 'website'
        }, {
            label: 'Дата',
            value: formatDateField(rangeDates)
        }, {
            label:'Загрузки',
            value: 'load'
        }, {
            label: 'Показы',
            value: 'impression'
        }, {
            label: 'Клики',
            value: 'clicks'
        }, {
            label: 'CTR',
            value: formatCtrField(null)
        }
    ];
    // Полный набор колонок для админов
    const fullFields = simpleFields.concat(['our_load', 'ntvk_clicks']);

    let fields = type === 'simple' ? simpleFields : fullFields;

    // если не включен фильтр по виджету то генерим поля для всех виджетов
    if (!filters.widget) {
        let names = pipe(
            extractWidgetsIds,
            filter( id => Boolean(widgets[id]) ),
            map( id => widgets[id].get('name') )
        )(stats);
        let widgetFields = getWidgetFields(names);

        fields = fields.concat(widgetFields);
    }

    return json2csv({
        del: '.',
        defaultValue: 0,
        data: statsData,
        fields
    });
}

function generateAdvertiserCSV({filters}, stats) {
    const rangeParams = filters.date.range === 'custom' ? filters.date : {range: filters.date.range};
    const rangeDates = getFromTo(rangeParams);

    const fields = [
        {
            label: 'Рекламодатель',
            value: 'advertiser'
        },
        {
            label: 'Дата',
            value: formatDateField(rangeDates)
        },
        {
            label: 'Показы',
            value: 'impression'
        },
        {
            label: 'Клики',
            value: 'clicks'
        },
        {
            label: 'CTR',
            value: formatCtrField(null)
        }
    ];

    return json2csv({
        del: '.',
        defaultValue: 0,
        data: stats,
        fields
    });
}

/**
 * Объединяем статы в один объект
 * статы у которых есть widget_id записываем с префиксом из имени виджета
 * @param {Array} widgets
 * @param {Object} acc
 * @param {Object} stat
 * @returns {Object}
 */
function toFlatStats(widgets, acc, stat) {
    let newAcc;

    if (stat.widget_id) {
        let newStat = {};
        let widget = widgets[stat.widget_id];

        if (widget) {
            for (let prop in stat) {
                if (stat.hasOwnProperty(prop)) {
                    newStat[widget.get('name') + '/' + prop] = stat[prop];
                }
            }
        }

        newAcc = {
            ...acc,
            ...newStat
        };
    } else {
        newAcc = {
            ...acc,
            ...stat
        };
    }
    return newAcc;
}
