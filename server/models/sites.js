import knex from '../lib/knex';
import mongoConnectAsync from '../lib/mongoConnectAsync';

export function getSitesList() {
    return knex.table('stats').pluck('site').distinct();
}

export function getSiteNameById(siteId) {
    return mongoConnectAsync.then((db) => {
        const condition = {
            _id: siteId
        };
        const fields = {
            name: 1
        };

        return db
            .collection('Site')
            .find(condition, fields)
            .toArrayAsync()
            .then((result) => {
                if (result[0]) {
                    return result[0].name
                } else {
                    throw new Error(`Сайт ${siteId} не найден!`)
                }
            });
    })

}
