import ReactGA from 'react-ga';
import queryString from 'query-string';
import {App, Filters} from 'actions/types';
import T from 'ramda/src/T';

export function trackPage({currentSite}) {
    const params = queryString.stringify({
        site: currentSite
    });
    const {pathname} = window.location;
    const fullPath = `${pathname}?${params}`;

    ReactGA.set({
        page: fullPath
    });
    ReactGA.pageview(fullPath);
}

export const sendGAEvent = App.case({
    Filters: Filters.case({
        SetCurrentFilter: (filter) => {
            ReactGA.event({
                category: 'Filters',
                action: `set ${filter.filterType} filter`,
                label: queryString.stringify(filter)
            });
        },
        ApplyFilters: () => {
            ReactGA.event({
                category: 'Filters',
                action: `submit filters form`
            });
        }
    }),
    _: T
});
