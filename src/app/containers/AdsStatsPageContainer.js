import React from 'react';
import connectToStore from 'lib/connectToStore';
import AdsStatsPage from 'components/AdsStatsPage';
import {requestAdsStatsForSite} from 'actions/adsStats';
import {setAppContext} from 'modules/appContext/actions';
import {findByProp} from 'lib/index';

/**
 * Главный контейнер со статистикой по рекламе
 * @constructor
 */
class AdsStatsPageContainer extends React.Component {
    constructor(props) {
        super(props);
        this.requestAdsStats = this.requestAdsStats.bind(this);
    }

    componentDidMount() {
        const {currentSite} = this.props;

        if (currentSite) {
            this.requestAdsStats(currentSite);
        }
        this.contextChangeHandler = setAppContext.map((s) => {
            setTimeout(() => this.requestAdsStats(s), 0);
        });
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
    }

    requestAdsStats(siteId) {
        const {availableSites, filters} = this.props;
        const site = findByProp(siteId, availableSites, 'id');

        requestAdsStatsForSite({
            site: site && site.get('name'),
            filters: filters
        });
    }

    render() {
        return (
            <AdsStatsPage
                actionForApplyFilters={requestAdsStatsForSite}
            />
        );
    }
}

export default connectToStore(AdsStatsPageContainer, ({app, filters}) => ({
    currentSite: app.selectedContext,
    availableSites: app.context,
    filters
}));

