import { getIntervalString } from '../../lib/apiHelpers';
import { getFromTo } from '../../lib/index';

export function prepareRequestParams(req) {
    const datesRange = getFromTo(req.query);

    return {
        from: datesRange.from.format('YYYY-MM-DD HH:mm:ssZ'),
        to: datesRange.to.format('YYYY-MM-DD HH:mm:ssZ'),
        account: req.params.account || req.params.site,
        type: req.query.type || 'simple',
        interval: getIntervalString(datesRange),
        hoursOffset: datesRange.from.utcOffset() / 60,
        widgetId: req.query.widget,
        campaign: req.query.campaign,
        contextType: req.query.contextType
    };
}
