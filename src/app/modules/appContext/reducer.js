import curryN from 'ramda/src/curryN';
import {AppContext} from 'actions/types';

export function defaultContext() {
    return {
        isInited: false,
        selectedContext: null,
        context: [],
        contextType: null
    };
}

const reducefn = AppContext.caseOn({
    SetInited: (state) => ({
        ...state,
        isInited: true
    }),
    ResetContext: (context, state) => ({
        ...state,
        context: [...context]
    }),
    SetSelectedContext: (contextId, state) => ({
        ...state,
        selectedContext: contextId
    }),
    SetSelectedContextType: (contextType, state) => ({
        ...state,
        contextType: contextType
    })
});

export default curryN(2, reducefn);
