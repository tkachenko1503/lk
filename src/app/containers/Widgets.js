import React from 'react';
import connectToStore from 'lib/connectToStore';
import Layout from 'components/Layout';
import CreateWidget from 'components/CreateWidget';
import WidgetsListContainer from 'containers/WidgetsListContainer';
import Button from 'react-bootstrap/lib/Button';
import Panel from 'react-bootstrap/lib/Panel';
import {makeAction, saveWidget, setWidgetMeta, downloadWidgets} from 'actions/widgets';

import styles from 'styles/widget.css';

/**
 * Дёргаем экшн создания виджета
 */
const createWidget = () => {
    makeAction({
        id: null,
        name: 'create'
    });
};

/**
 * Страница для CRUD операций с виджетами
 * @constructor
 */
class Widgets extends React.Component {
    constructor(props) {
        super(props);
        this.saveWidget = this.saveWidget.bind(this);
        this.downloadWidgets = this.downloadWidgets.bind(this);
    }

    /**
     * Сохраняем изменения или создаём виджет
     * @param e
     */
    saveWidget(e) {
        e.preventDefault();
        const {widgetName} = e.target.elements;
        const {currentUser, currentSite} = this.props;
        const name = widgetName.value ? widgetName.value.trim() : '';

        if (!currentSite) {
            setWidgetMeta({
                code: 'NO_SITE'
            });
            return;
        }

        if (name) {
            saveWidget({
                name: name,
                user: currentUser,
                site: currentSite
            });
        }
    }

    downloadWidgets() {
        downloadWidgets({
            widgetsList: this.props.widgetsList
        });
    }

    /**
     * Отрисовываем форму создания виджета
     */
    renderCreateWidget() {
        return (
            <div className={styles.widgetCreateForm}>
                <CreateWidget
                    submitAction={this.saveWidget}
                    inputSize={5}
                    buttonSize={2}
                />
            </div>
        );
    }

    /**
     * Заголовок страницы с кнопкой создания
     * и формой создания (если есть action) виджета
     * @param action
     */
    renderWidgets(widgetsAction) {
        return (
            <Panel>
                <div className={styles.widgetBuildInTip}>
                    Чтобы всё заработало, вставьте сперва в {`<head>`} код
                    {`<script src="//p1.ntvk1.ru/nv.js" async="true"></script>`},
                    а потом код виджета там, где он должен показывать рекомендации
                </div>

                <Button onClick={createWidget}>
                    Добавить виджет
                </Button>
    
                {widgetsAction.name === 'create'
                    ? this.renderCreateWidget()
                    : null}
    
                <WidgetsListContainer />

                <Button onClick={this.downloadWidgets}>
                    Скачать коды всех виджетов в одном файле
                </Button>
            </Panel>
        );
    }
    
    render() {
        const {currentSite, widgetsAction} = this.props;

        return (
            <Layout>
                <div className="widgets-container">
                    <div className="wrapper">
                        {currentSite
                            ? this.renderWidgets(widgetsAction)
                            : 'Выберите сайт'}
                    </div>
                </div>
            </Layout>
        );
    }
}

export default connectToStore(Widgets, ({app, widgets, user}) => ({
    currentSite: app.selectedContext,
    widgetsAction: widgets.action,
    widgetsList: widgets.list,
    currentUser: user.current
}));
