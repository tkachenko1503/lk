import curryN from 'ramda/src/curryN';
import {AdsStats} from 'actions/types';

export function defaultAdsStats() {
    return {
        statsList: [],
        statsSummaries: {
            widget_load: 0,
            ads_shows: 0,
            ads_referrer: 0,
            click_cost: 0,
            ads_profit: 0,
            ads_profit_thousand_shows: 0,
            ads_profit_mean: 0,
            ctr: 0
        }
    };
}

const reducefn = AdsStats.caseOn({
    SetAdsStats: ({stats, overview}, state) => ({
        ...state,
        statsList: stats,
        statsSummaries: {
            ...overview,
            ctr:  overview.ads_shows
                ? Math.round((overview.ads_referrer / overview.ads_shows) * 100)
                : 0
        }
    }),
    RequestAdsStats: (params, state) => ({...state}),
    DownloadAdsCSV: (params, state) => ({...state})
});

export default curryN(2, reducefn);
