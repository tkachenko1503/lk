import React from 'react';
import {Route, IndexRedirect} from 'react-router';
import {redirectToLogin, redirectToDashboard, checkAdminRole} from 'lib/index';
import App from 'containers/App';
import Stats from 'containers/Stats';
import Login from 'containers/Login';
import Register from 'containers/Register';
import Widgets from 'containers/Widgets';
import AdsPage from 'components/AdsPage';
import AdsStatsPageContainer from 'containers/AdsStatsPageContainer';
import AdsPaymentsPageContainer from 'containers/AdsPaymentsPageContainer';
import TrafficExchangeContainer from 'containers/TrafficExchangeContainer';
import TrafficStatisticsPageContainer from 'containers/TrafficStatisticsPageContainer';
import ABTests from 'containers/ABTests';
import ContentAnalisisPageContainer from 'containers/ContentAnalisisPageContainer';
import AdvertisersStatsImportContainer from 'modules/advertisers/AdvertisersStatsImportContainer';
import AdvertisersContentPageContainer from 'modules/advertisers/AdvertisersContentPageContainer';
import AdsCampaigns from 'modules/adsCampaigns/AdsCampaignsContainer';
import NoMatch from 'components/NoMatch';

const routes = (
    <Route path="/" component={App}>
        <IndexRedirect to="/dashboard"/>

        <Route path="/" onEnter={redirectToLogin}>
            <Route path="dashboard" component={Stats}/>
            <Route path="widgets" component={Widgets}/>

            <Route path="ads">
                <Route path="settings" component={AdsPage}/>
                <Route path="statistics" component={AdsStatsPageContainer}/>
                <Route path="advertisers" component={AdvertisersContentPageContainer}/>
                <Route path="payments" component={AdsPaymentsPageContainer}/>
            </Route>

            <Route path="traffic">
                <Route path="settings" component={TrafficExchangeContainer}/>
                <Route path="statistics" component={TrafficStatisticsPageContainer}/>
            </Route>

            <Route path="ab-tests">
                <Route path="settings" component={ABTests}/>
            </Route>

            <Route path="analysis">
                <Route path="content" component={ContentAnalisisPageContainer}/>
            </Route>

            <Route path="advertisers">
                <Route path="import-stats" onEnter={checkAdminRole} component={AdvertisersStatsImportContainer}/>
            </Route>

            <Route path="ads-campaigns" component={AdsCampaigns}/>
        </Route>

        <Route path="login" component={Login} onEnter={redirectToDashboard}/>
        <Route path="register" component={Register} onEnter={redirectToDashboard}/>

        <Route path="*" component={NoMatch}/>
    </Route>
);

export default routes;
