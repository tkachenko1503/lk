import React from 'react';
import classnames from 'classnames';
import Panel from 'react-bootstrap/lib/Panel';
import Chart from 'components/Chart';
import StatsChart from 'components/StatsChart';
import TrafficChartControls from 'components/TrafficChartControls';

import panel from 'styles/panel.css';
import chart from 'styles/chart.css';
import trafficChart from 'styles/trafficChart.css';

const TrafficStatsChart = (props) => {
    const panelHeader = (
        <div>Объявления в ваших виджетах</div>
    );

    return (
        <Panel
            header={panelHeader}
            bsClass={classnames(
                panel.fixedHeader,
                "panel"
            )}
        >
            <div className={classnames(
                'earning-chart-space',
                chart.fixChartTooltipAlign,
                trafficChart.chartPanel
            )}>
                <div className={trafficChart.buttons}>
                    <TrafficChartControls
                        controls={props.controls}
                        changeFields={props.changeFields}
                    />
                </div>
                <div className={trafficChart.chart}>
                    <TrafficChart {...props} />
                </div>
            </div>
        </Panel>
    );
};

export default TrafficStatsChart;

class TrafficChart extends StatsChart {
    isNeedRedrawChart(newProps) {
        const defaultResult = StatsChart.prototype.isNeedRedrawChart.call(this, newProps);
        const isNewTitles = newProps.first !== this.props.first;

        return defaultResult || isNewTitles;
    }

    render() {
        const {dateRange} = this.props;
        const withTime = this.isNeedShowTime(dateRange);
        const config = chartConfig(this.props, withTime);

        return (
            <Chart config={config} redraw={this.needRedraw} />
        );
    }
}

function chartConfig({created, first, second, col1, col2}, withTime) {
    return {
        data: {
            x: 'x',
            xFormat: '%Y-%m-%dT%H:%M:%S.%Z',
            columns: [
                ['x'].concat(created),
                [first].concat(col1),
                [second].concat(col2)
            ],
            types: {
                [first]: 'area'
            },
            axes: {
                [first]: 'y',
                [second]: 'y2'
            },
            colors: {
                [first]: '#6ED594',
                [second]: '#179AD0'
            },
            unload: true
        },
        axis: {
            x: {
                type: 'timeseries',
                tick: {
                    format: withTime ? '%d.%m-%H:%M' : '%d.%m',
                    culling: {
                        max: 5
                    }
                }
            },
            y: {
                label: {
                    text: first,
                    position: 'outer-middle'
                },
                padding: {
                    top: 10,
                    bottom: 0
                },
                min: 0
            },
            y2: {
                label: {
                    text: second,
                    position: 'outer-middle'
                },
                padding: {
                    top: 10,
                    bottom: 0
                },
                min: 0,
                show: true
            }
        },
        tooltip: {
            format: {
                value: function (value, ratio, id) {
                    return id === 'CTR' ? `${value}%` : value.toLocaleString('ru-RU');
                }
            }
        },
        size: {height: 268}
    }
}
