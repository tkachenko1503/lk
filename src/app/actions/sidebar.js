import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import actions$ from 'actions/index';
import {App, Sidebar} from 'actions/types';

// Сворачивает или разворачивает левое навигационное меню
export const toggleSidebar = flyd.stream();
// Сворачивает или разворачивает навигацию второго уровня
export const toggleNavItem = flyd.stream();

flyd.on(
    pipe(App.Sidebar, actions$),
    mergeAll([
        toggleSidebar.map(() => Sidebar.ToggleSidebar()),
        toggleNavItem.map(Sidebar.ToggleNavMenu)
    ])
);
