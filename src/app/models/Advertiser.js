import Parse from 'parse';
import BaseModel from 'models/Base';

const className = 'Advertiser';

class Advertiser extends BaseModel {
    constructor(attrs) {
        super(className);

        if (attrs) {
            this.set(attrs);
        }
    }

    static getAvailableAdvertisers() {
        const query = new Parse.Query(Advertiser);
        return query.find();
    }
}

export default Advertiser;

Parse.Object.registerSubclass('Advertiser', Advertiser);
