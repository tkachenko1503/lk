import Parse from 'parse';
import Site from 'models/Site';
import ABTestItem from 'models/ABTestItem';
import BaseModel from 'models/Base';
import map from 'ramda/src/map';
import invoker from 'ramda/src/invoker';
import pluck from 'ramda/src/pluck';
import all from 'ramda/src/all';
import identity from 'ramda/src/identity';
import pipe from 'ramda/src/pipe';
import not from 'ramda/src/not';
import filter from 'ramda/src/filter';

const AB_TEST_VALIDATION_ERRORS = {
    EMPTY_NAME: {
        code: 'EMPTY_AB_TEST_NAME',
        message: 'Укажите имя теста'
    },
    CHOOSE_ALL_WIDGETS: {
        code: 'CHOOSE_ALL_WIDGETS',
        message: 'Необходимо указать все виджеты в тесте'
    }
};

const className = 'ABTest';
const revert = invoker(0, 'revert');
const revertAll = map(revert);

const isNew = invoker(0, 'isNew');
const oldItems = pipe(isNew, not);
const filterOldItems = filter(oldItems);

const validateFields = (name, items) => {
    const cleanName = name && name.trim();
    if (!cleanName || !cleanName.length) {
        return AB_TEST_VALIDATION_ERRORS.EMPTY_NAME;
    }

    const widgetIds = pluck('widgetId', items);
    const isAllIdsPresent = all(identity, widgetIds);
    if (!isAllIdsPresent) {
        return AB_TEST_VALIDATION_ERRORS.CHOOSE_ALL_WIDGETS;
    }
};

class ABTest extends BaseModel {
    constructor(attrs) {
        super(className);

        if (attrs) {
            this.set(attrs);
        }
    }

    static spawn({siteId, name, items}) {
        const error = validateFields(name, items);

        if (error) {
            return Parse.Promise.reject(error);
        }

        const test = new ABTest();
        const currentSite = new Site();
        const testItems = map(ABTestItem.createTestItem, items);
        const relation = test.relation('testItems');

        currentSite.id = siteId;
        test.set('name', name);
        test.set('site', currentSite);

        return Parse.Object.saveAll(testItems)
            .then(savedItems => {
                relation.add(savedItems);
                return test.save(null);
            });
    }

    static getTestsBySite(siteId) {
        const currentSite = new Site();
        const query = new Parse.Query(ABTest);

        currentSite.id = siteId;
        query
            .equalTo('site', currentSite)
            .notEqualTo('removed', true);

        return query.find();
    }

    static updateName({test, name}) {
        test.set('name', name);
        return test.save(null);
    }

    static getABTestItems(test) {
        const relation = test.relation('testItems');
        const query = relation.query();

        return query
            .include('Widget')
            .find()
            .then(testItems => ({test, testItems}));
    }
    
    static saveAllItems({test, testItems, itemsToRemove}) {
        const relation = test.relation('testItems');
        const promise = Parse.Promise.resolve();

        itemsToRemove.map(i => i.destroy());

        return promise
            .then(() => Parse.Object.saveAll(testItems))
            .then(savedItems => {
                relation.add(savedItems);
                return test.save(null);
            });
    }

    static cancelItemsChanges({test, testItems}) {
        revertAll(testItems);
        return {
            testId: test.id,
            testItems: filterOldItems(testItems)
        };
    }

    static removeTestItem(test, testItem) {
        const relation = test.relation('testItems');
        relation.remove(testItem);
    }
}

export default ABTest;

Parse.Object.registerSubclass('ABTest', ABTest);
