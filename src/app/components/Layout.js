import React from 'react';
import classNames from 'classnames';

import SidebarContainer from 'containers/SidebarContainer';
import Header from 'components/Header';

import styles from 'styles/layout.css';

/**
 * Основной лейоут приложения
 * содержит сайдбар хедер и футер
 * @param props
 * @constructor
 */
const Layout = (props) => {
    return (
        <section>
            <SidebarContainer />

            <div
                className={classNames(
                    styles.content,
                    'body-content'
                )}
            >
                <Header />

                {props.children}

                <footer>
                    2016 &copy; natimatica.com
                </footer>
            </div>
        </section>
    );
};

export default Layout;
