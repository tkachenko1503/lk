import React from 'react';
import classnames from 'classnames';
import zipWith from 'ramda/src/zipWith';
import merge from 'ramda/src/merge';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Panel from 'react-bootstrap/lib/Panel';
import {SUMMARIES_PARAMS} from 'lib/constants';

import styles from 'styles/dashboard.css';

/**
 * Четыре плитки с общей статистикой
 * @param props
 * @constructor
 */
const StatsSummaryPanels = ({summaries}) => {
    const panels = zipWith(merge, SUMMARIES_PARAMS, summaries);

    return (
        <Grid
            fluid={true}
            className={styles.dashboardGrid}
        >
            <Row className="state-overview">
                {panels.map(SummaryPanel)}
            </Row>
        </Grid>
    );
};

export default StatsSummaryPanels;

/**
 * Плитка со статистикой
 * @param props
 */
const SummaryPanel = (props, i) => {
    const {panelClass, symbolClass, icon, valueColor} = props;
    const {value, title, format} = props;

    return (
        <Col sm={6} lg={3} key={i}>
            <Panel
                className={classnames(panelClass)}
                bsClass={classnames(
                    styles.overviewPanel,
                    styles.dashboardPanel
                )}
            >
                <div className={classnames("symbol", symbolClass)}>
                    <i className={classnames("fa", icon)}/>
                </div>
                <div 
                    className={classnames(
                        "value",
                        valueColor,
                        styles.overviewPanelValue
                    )}
                >
                    <h1>
                        {format ? format(value) : value}
                    </h1>
                    <p>
                        {title}
                    </p>
                </div>
            </Panel>
        </Col>
    );
};
