import flyd from 'flyd';
import flip from 'ramda/src/flip';
import actions$ from 'actions/index';
import reducer, {initialState} from 'reducers/index';
import {sendGAEvent} from 'lib/analitics';

export const store$ = flyd.scan(flip(reducer), initialState(), actions$);

// @todo extract to middleware
if (window._INITIAL_PROPS.MODE === 'develop') {
    // logger
    flyd.on((action) => {
        console.log(
            action.name,
            action[0] ? ('> ' + action[0].name) : ''
        );
    }, actions$);

    flyd.on(() => {
        console.dir(store$());
    }, store$);
} else {
    // triggers GA events
    flyd.on(sendGAEvent, actions$);
}
