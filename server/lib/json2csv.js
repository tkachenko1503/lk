import Promise from 'bluebird';
import json2csv from 'json2csv';

export default Promise.promisify(json2csv);
