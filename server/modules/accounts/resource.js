/* eslint max-len: "off" */

import moment from 'moment';
import knex from '../../lib/knex';
import { map } from 'ramda';
import siteSettings from '../../../configs/siteSettings';

function formatResults(interval, hoursOffset) {
    return function ({ website, created, ctr, impression, load, referrer }) {
        const dateFormatSchema = interval === 'hour' ? 'DD/MM/YYYY HH:mm' : 'DD/MM/YYYY';

        return {
            website,
            ctr,
            date: moment.utc(created)
                        .utcOffset(hoursOffset)
                        .subtract(hoursOffset, 'hours')
                        .format(dateFormatSchema),
            impression: Number(impression),
            load: Number(load),
            referrer: Number(referrer)
        };
    };
}

function formatAdvertiserResults(interval, hoursOffset) {
    return function ({created, ctr, impression, load, referrer, advertiser, campaign}) {
        const dateFormatSchema = interval === 'hour' ? 'DD/MM/YYYY HH:mm' : 'DD/MM/YYYY';

        return {
            advertiser,
            campaign,
            date: moment.utc(created)
                        .utcOffset(hoursOffset)
                        .subtract(hoursOffset, 'hours')
                        .format(dateFormatSchema),
            ctr: Number(ctr),
            impression: Number(impression),
            load: Number(load),
            clicks: Number(referrer)
        };
    };
}

function wrapAccountRequest(request, formatRows) {
    return request
        .then(data => ({
            error: null,
            result: map(formatRows, data.rows)
        }))
        .catch(error => ({
            error,
            result: []
        }));
}

export function getSiteAccountCsv({ interval, hoursOffset, account, from, to }) {
    const settings = siteSettings[account] || siteSettings.common;
    const formatRows = formatResults(interval, hoursOffset);

    const q = knex.raw(`
        SELECT
            sum( coalesce((impression)::text::int, 0) ) AS impression,
            sum( coalesce((referrer)::text::int, 0) ) AS referrer,
            sum( coalesce((load)::text::int, 0) ) AS load,
            round( 100 * (sum( coalesce((referrer)::text::decimal, 0) ) / sum( coalesce((impression)::text::decimal, 1) )), 1 ) AS ctr,
            date_trunc('${interval}', (created + '${hoursOffset} hour')::timestamp with time zone) AS created,
            website
        FROM (
            SELECT 
                data->'NTVK'->'impression' AS impression, 
                data->'load' AS load, 
                ${settings.clicksField} AS referrer,
                since AS created,
                site AS website,
                widget_id AS widget_id
            FROM stats 
            WHERE site = '${account}'
                AND widget_id IS NULL
                AND since 
                    BETWEEN ('${from}'::timestamp - interval '${hoursOffset} hours')
                    AND ('${to}'::timestamp - interval '${hoursOffset} hours')
            ORDER BY created
            ) AS sq
        GROUP BY date_trunc('${interval}', (created + '${hoursOffset} hour')::timestamp with time zone), website
        ORDER BY created
    `);

    return wrapAccountRequest(q, formatRows);
}

export function getAdvertiserAccountCsv({ interval, hoursOffset, advertiser, campaign, from, to }) {
    const formatRows = formatAdvertiserResults(interval, hoursOffset);

    const q = knex.raw(`
        SELECT 
            data->'NTVK'->'shows' AS impression, 
            data->'NTVK'->'clicks' AS referrer,
            data->'NTVK'->'ctr' AS ctr,
            since AS created,
            advertiser AS advertiser,
            campaign AS campaign
        FROM imported_stats 
        WHERE advertiser = '${advertiser}' 
            ${campaign ? `AND campaign = '${campaign}'` : ''} 
            AND since BETWEEN '${from}'::timestamp AND '${to}'::timestamp
        ORDER BY ${campaign ? '' : 'campaign,'} created
    `);

    return wrapAccountRequest(q, formatRows);
}
