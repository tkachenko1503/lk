import React from 'react';
import connectToStore from 'lib/connectToStore';
import RegisterPage from 'components/RegisterPage';
import {userRegister} from 'actions/user';
import {withRouter} from 'react-router';

class Register extends React.Component {
    loginSubmit(e) {
        e.preventDefault();
        
        const {password, email} = e.target.elements;
        const name = email.value && email.value.trim();
        const pass = password.value && password.value.trim();

        userRegister({
            username: name,
            password: pass,
            email: name
        });
    }

    render() {
        return (
            <RegisterPage
                authSubmit={this.loginSubmit}
                user={this.props.user}
                router={this.props.router}
                location={this.props.location}
            />
        );
    }
}

export default connectToStore(withRouter(Register), state => ({
    user: state.user
}));
