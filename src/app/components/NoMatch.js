import React from 'react';

const NoMatch = (props) => {
    return (
        <div>
            Sorry! Maybe next time.
        </div>
    );
}

export default NoMatch;
