import curryN from 'ramda/src/curryN';
import omit from 'ramda/src/omit';

import {Filters} from '../actions/types';
import {getFromTo} from '../lib/index';

export function defaultFilters() {
    return {
        date: getFromTo({range: 'month'}),
        widget: null,
        campaign: null,
        meta: {
            error: false,
            pending: false
        }
    };
}

const omitFilterType = omit(['filterType']);

const reducefn = Filters.caseOn({
    SetCurrentFilter: (filter, state) => {
        switch (filter.filterType) {
            case 'date':
                let isCustomFilter = filter.range === 'custom';
                let dateRange;

                if (isCustomFilter) {
                    dateRange = getFromTo({
                        ...state.date,
                        ...(omitFilterType(filter))
                    });
                } else {
                    dateRange = getFromTo({ range: filter.range });
                }

                return {
                    ...state,
                    date: dateRange,
                    meta: {
                        error: false,
                        pending: false
                    }
                };
                break;

            case 'widget':
                return {
                    ...state,
                    widget: filter.widgetId,
                    meta: {
                        error: false,
                        pending: false
                    }
                };
                break;

            case 'campaign':
                return {
                    ...state,
                    campaign: filter.campaignId,
                    meta: {
                        error: false,
                        pending: false
                    }
                };
                break;
        }
    },
    ApplyFilters: (isValid$, params, state) => {
        const {date} = state;

        if (date.range === 'custom' && !date.from) {
            return {
                ...state,
                meta: {
                    ...state.meta,
                    error: true,
                    validationError: {
                        field: 'from',
                        message: 'Выберите начальную дату'
                    }
                }
            };
        }

        isValid$(true);
        return state;
    }
});

export default curryN(2, reducefn);
