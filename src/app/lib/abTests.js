import pipe from 'ramda/src/pipe';
import filter from 'ramda/src/filter';
import not from 'ramda/src/not';

const isNotRemoved = pipe(isRemoved, not);

/**
 * Проверяет удалён ли виджет который юзается в тесте
 * @param {ABTestItem} item
 * @returns {Boolean}
 */
function isRemoved(item) {
    const widget = item.get('Widget');
    return widget.get('removed');
}

export const withoutRemoved = filter(isNotRemoved);
