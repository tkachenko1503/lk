import React from 'react';
import classnames from 'classnames';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Table from 'react-bootstrap/lib/Table';
import AdsCampaignRenameAction from 'modules/adsCampaigns/AdsCampaignRenameAction';

import styles from 'styles/widget.css';
import table from 'styles/widgetTable.css';

class AdsCampaignListItem extends React.Component {
    constructor(props) {
        super(props);

        this.makeAction = this.makeAction.bind(this);
        this.removeAdsCampaign = this.removeAdsCampaign.bind(this);
    }

    removeAdsCampaign() {
        const {openRemoveDialog, adsCampaign} = this.props;

        openRemoveDialog(adsCampaign);
    }

    makeAction(action) {
        this.props.makeAction({
            id: this.props.adsCampaign.id,
            name: action
        });
    }

    renderAction(action) {
        return (
            <NavItem
                className={styles.actionItem}
                key={action.key}
                eventKey={action.key}
            >
                {action.title}
            </NavItem>
        );
    }

    renderActiveAction(actionName) {
        let actionForm;

        switch (actionName) {
            case 'rename':
                actionForm = <AdsCampaignRenameAction adsCampaign={this.props.adsCampaign}/>;
                break;
        }

        return actionForm;
    }

    renderNameIdNormal(name, id) {
        return [
            <td
                className={classnames(
                    styles.widgetName,
                    table.nameCol
                )}
                key="name"
            >
                <span alt={name}>
                    {name}
                </span>
            </td>,
            <td
                className={table.idCol}
                key="id"
            >
                {`ID_${id}`}
            </td>
        ];
    }

    renderRenameAction(action) {
        return (
            <td
                className={styles.noPadTop}
                colSpan={2}
            >
                {action}
            </td>
        );
    }

    render() {
        const {adsCampaign, action} = this.props;
        const {name, objectId} = adsCampaign.toJSON();

        const activeAction = action && objectId === action.id
            ? this.renderActiveAction(action.name)
            : null;

        return (
            <tr className="ads-campaigns">
                <td colSpan={4} className={table.noPad}>
                    <Table className={styles.widgetInnerTable}>
                        <tbody>
                            <tr>
                                {action && action.name === 'rename'
                                    ? this.renderRenameAction(activeAction)
                                    : this.renderNameIdNormal(name, objectId)}

                                <td key="actions" className={table.actionsCol}>
                                    <Nav
                                        activeKey={action ? action.name : null}
                                        bsStyle="pills"
                                        onSelect={this.makeAction}
                                    >
                                        {this.renderAction({key: 'rename', title: 'Переименовать'})}
                                    </Nav>
                                </td>

                                <td key="remove" className={table.removeCol}>
                                    <Button
                                        className={styles.removeBtn}
                                        onClick={this.removeAdsCampaign}
                                    >
                                        <Glyphicon glyph="trash"/>
                                    </Button>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </td>
            </tr>
        );
    }
}

export default AdsCampaignListItem;
