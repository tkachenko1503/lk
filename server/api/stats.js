import express from "express";
import Promise from 'bluebird';
import {getStatsBySite, getOverviewData, getStatsForCSVAllWidgets,
        getStatsByAdvertiser, getAdvertiserOverviewData, getAdvertiserStatsForCSVAllWidgets} from '../models/stats';
import {getIntervalString} from '../lib/apiHelpers';
import {getFromTo} from '../lib/index';

const stats = express.Router();
const SITE_CONTEXT = 'Site';
const ADVERTISER_CONTEXT = 'Advertiser';

stats.get('/:site', function (req, res, next) {
    let stats, overview;
    const datesRange = getFromTo(req.query);
    const interval = getIntervalString(datesRange);
    const hoursOffset = datesRange.from.utcOffset() / 60;

    if (req.query.contextType === SITE_CONTEXT) {
        stats = getStatsBySite({
            site: req.params.site,
            from: datesRange.from.format('YYYY-MM-DD HH:mm:ssZ'),
            to: datesRange.to.format('YYYY-MM-DD HH:mm:ssZ'),
            widgetId: req.query.widget,
            interval: interval,
            hoursOffset: hoursOffset
        });
        overview = getOverviewData({
            site: req.params.site,
            ...datesRange,
            widget: req.query.widget
        });
    } else if (req.query.contextType === ADVERTISER_CONTEXT) {
        stats = getStatsByAdvertiser({
            advertiser: req.params.site,
            from: datesRange.from.format('YYYY-MM-DD HH:mm:ssZ'),
            to: datesRange.to.format('YYYY-MM-DD HH:mm:ssZ'),
            campaign: req.query.campaign,
            hoursOffset: hoursOffset
        });
        overview = getAdvertiserOverviewData({
            advertiser: req.params.site,
            ...datesRange,
            campaign: req.query.campaign
        });
    }

    Promise.all([stats, overview])
        .then(result => {
            const [statsResult, overviewResult] = result;
            res.json({
                stats: statsResult,
                overview: overviewResult[0]
            });
        })
        .catch(next);
});

stats.get('/csv/:site', function (req, res, next) {
    const type = req.query.type || 'simple';
    const datesRange = getFromTo(req.query);
    const interval = getIntervalString(datesRange);
    const hoursOffset = datesRange.from.utcOffset() / 60;
    const widgetId = req.query.widget;

    let statsQuery;

    if (req.query.contextType === SITE_CONTEXT) {
        statsQuery = getStatsForCSVAllWidgets({
            type: type,
            site: req.params.site,
            from: datesRange.from.format('YYYY-MM-DD HH:mm:ssZ'),
            to: datesRange.to.format('YYYY-MM-DD HH:mm:ssZ'),
            widgetId: widgetId,
            interval: interval,
            hoursOffset: hoursOffset
        });
    } else if (req.query.contextType === ADVERTISER_CONTEXT) {
        statsQuery = getAdvertiserStatsForCSVAllWidgets({
            advertiser: req.params.site,
            from: datesRange.from.format('YYYY-MM-DD HH:mm:ssZ'),
            to: datesRange.to.format('YYYY-MM-DD HH:mm:ssZ'),
            campaign: req.query.campaign,
            hoursOffset: hoursOffset
        });
    }

    statsQuery
        .then(stats => res.json(stats))
        .catch(next);
});

export default stats;
