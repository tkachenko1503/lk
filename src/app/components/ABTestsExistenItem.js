import React from 'react';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Slider from 'components/Slider';
import ABTestItemWidgetSelectContainer from 'containers/ABTestItemWidgetSelectContainer';

import abTestCreateForm from 'styles/abTestCreateForm.css';

const ABTestsItem = (props) => {
    const {setFrequency, removeWidget} = props;
    const {widget, frequency, testItem, needRemoveButton, index} = props;

    return (
        <Row className={abTestCreateForm.testItemRow}>
            <Col
                xs={4}
                className={abTestCreateForm.widgetSelect}
            >
                {testItem.isNew()
                    ? (
                        <ABTestItemWidgetSelectContainer
                            testItem={testItem}
                            widget={widget}
                        />
                    )
                    : widget.get('name')}
            </Col>
            <Col xs={7}>
                {widget && widget.get('removed')
                    ? (
                        <span className={abTestCreateForm.rmWidgetMessage}>
                            Виджет был удалён. Выберите другой виджет
                        </span>
                    )
                    : (
                        <Slider
                            percent={true}
                            value={frequency || 0}
                            onChange={setFrequency}
                            eventKey={index}
                        />
                    )
                }
            </Col>
            {needRemoveButton
                ? (
                    <Col xs={1}>
                        <Button
                            className={abTestCreateForm.removeBtn}
                            onClick={removeWidget}
                            data-index={index}
                        >
                            <Glyphicon glyph="trash"/>
                        </Button>
                    </Col>
                )
                : null}
        </Row>
    );
};

export default ABTestsItem;
