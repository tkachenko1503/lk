import React from 'react';
import Select from 'components/SitesSelect';
import {findByProp} from 'lib/index';
import {appContext} from 'modules/appContext';

const toPlainObject = model => model.toJSON();

class SitesSelect extends React.Component {
    constructor(props) {
        super(props);

        this.chooseSite = this.chooseSite.bind(this);
    }

    chooseSite(e, site) {
        this.props.setAppContext(site);
    }
    
    render() {
        const {appContext, selectedAppContext} = this.props;
        const current = findByProp(selectedAppContext, appContext, 'id');

        return (
            <Select
                availableSites={appContext.map(toPlainObject)}
                currentSite={selectedAppContext}
                currentSiteName={current && current.get('name')}
                chooseSite={this.chooseSite}
            />
        );
    }
}

export default appContext(SitesSelect);
