import curryN from 'ramda/src/curryN';
import {Articles} from 'actions/types';

export function defaultArticles() {
    return {
        articlesList: [],
        articlesTableControls: {
            sort: {
                type: 'desc',
                id: null
            },
            page: 1,
            size: 10
        }
    };
}

const reducefn = Articles.caseOn({
    SetArticles: (articles, state) => ({
        ...state,
        articlesList: articles
    }),
    RequestArticlesStats: (params, state) => state,
    SetControlValue: ({control, value}, state) => {
        const newState = {
            ...state,
            articlesTableControls: {
                ...state.articlesTableControls,
                [control]: value
            }
        };

        // если меняем размер таблицы сбрасываем пейджинг
        if (control === 'size') {
            newState.articlesTableControls.page = 1;
        }

        return newState;
    }
});

export default curryN(2, reducefn);
