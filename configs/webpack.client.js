const path = require('path');
const webpack = require('webpack');
const precss = require('precss');
const autoprefixer = require('autoprefixer');

const buildPath = path.resolve('./dist/app');
const src = path.resolve('./src/app');

const jsLoaders = ['babel'];

const plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.ProvidePlugin({
        fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch'
    }),
    new webpack.NoErrorsPlugin()
];
const alias = {
    lib: path.join(src, 'lib'),
    containers: path.join(src, 'containers'),
    components: path.join(src, 'components'),
    styles: path.join(src, 'styles'),
    actions: path.join(src, 'actions'),
    reducers: path.join(src, 'reducers'),
    queries: path.join(src, 'queries'),
    models: path.join(src, 'models'),
    modules: path.join(src, 'modules')
};

const entry = [path.resolve('./src/app/index.js')];

let devtool = 'source-map';

if (process.env.NODE_ENV === 'production') {
    plugins.unshift(new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify('production')
        }
    }));
    plugins.unshift(new webpack.optimize.UglifyJsPlugin({
        mangle: false,
        minimize: true,
        compress: { warnings: false }
    }));

    alias.react = 'react/dist/react.min.js';
    alias['react-dom'] = 'react-dom/dist/react-dom.min.js';

    devtool = 'eval';
} else {
    entry.unshift('webpack-hot-middleware/client');
    plugins.unshift(new webpack.HotModuleReplacementPlugin());
    jsLoaders.unshift('react-hot');
}

plugins.unshift(new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js'));


module.exports = {
    devtool,
    plugins,
    target: 'web',
    entry: {
        app: entry,
        vendor: ['react', 'react-dom', 'flyd', 'moment', 'c3', 'parse']
    },
    output: {
        path: buildPath,
        publicPath: '/',
        filename: '[name].bundle.js'
    },
    resolve: {
        extensions: ['', '.js'],
        alias
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                exclude: /(node_modules)/,
                loaders: jsLoaders
            },
            {
                test: /\.css$/,
                exclude: /(node_modules)|ion-slider|jquery.dataTables/,
                loader: 'style!css-loader?modules&importLoaders=1&' +
                        'localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file'
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'url-loader?limit=10000&minetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=image/svg+xml'
            },
            {
                test: /\.gif/,
                loader: 'url-loader?limit=10000&mimetype=image/gif'
            },
            {
                test: /\.jpg/,
                loader: 'url-loader?limit=10000&mimetype=image/jpg'
            },
            {
                test: /\.png/,
                loader: 'url-loader?limit=10000&mimetype=image/png'
            }
        ]
    },
    postcss: () => [precss, autoprefixer]
};
