import React from 'react';
import range from 'ramda/src/range';
import map from 'ramda/src/map';
import flyd from 'flyd';
import flatMap from 'flyd/module/flatmap';
import takeUntil from 'flyd/module/takeuntil';
import {throttle} from 'lib/streams';

import 'style-loader!raw!styles/ion-slider/ion.rangeSlider.css';
import 'style-loader!raw!styles/ion-slider/ion-custom-slicklab.css';

class Slider extends React.Component {
    /**
     * Слайдер с ленивой инициализацией
     * расчитываем базовые характеристики
     * stepPercent - длина одного деления в процентах
     * stepRange - массив с делениями [0, 1, 2, ..., step]
     * @constructor
     * @param props
     */
    constructor(props) {
        super(props);

        const {step, percent} = props;
        let stepPercent;
        let stepRange;

        if (percent) {
            stepPercent = 10;
            stepRange = map(x => x * 10, range(0, 11));
            this.percent = percent;
        } else {
            stepPercent = 100 / step;
            stepRange = range(0, step);
        }

        this.sliderSpec = {
            stepPercent,
            stepRange
        };
        this.inited = false;
        this.eventKey = props.eventKey;

        this.initSlider = this.initSlider.bind(this);
        this.renderGridItem = this.renderGridItem.bind(this);
    }

    componentWillReceiveProps(newProps) {
        if (this.eventKey !== newProps.eventKey) {
            this.eventKey = newProps.eventKey;
        }
    }

    componentWillUnmount() {
        if (this.inited) {
           this.cleanUp()
        }
    }

    /**
     * Инициализация слайдера по первому клику на него
     * @param firstClickEvent
     */
    initSlider(firstClickEvent) {
        if (this.inited) {
            return;
        }

        const {stepPercent, stepRange} = this.sliderSpec;
        const {slider, caret, bar, line} = this.refs;
        const {onChange} = this.props;
        const percent = this.percent;
        // создаём стримы
        const mousedown = flyd.stream();
        const mousemove = flyd.stream();
        const mouseup =  flyd.stream();
        const setImmediate =  flyd.stream();
        // определяем константы
        const sliderPosition = slider.getBoundingClientRect();
        const divider = stepPercent;
        const minSliderVal = stepRange[0];
        const maxSliderVal = percent ? 10 : stepRange.length;

        /**
         * Обработчик движения курсора или клика по активной части слайдера
         * расчитывает текущее значение слайдера
         * @param {Object} moveEvent
         * @returns {Number} номер выбранного деления в диапазоне от minSliderVal до maxSliderVal
         */
        const calcNewSliderValue = function (moveEvent) {
            moveEvent.preventDefault();

            const lineLength = moveEvent.clientX - sliderPosition.left;
            const currentSliderVal =  Math.round(( (lineLength * 100) / slider.clientWidth) / divider);
            let result;

            if (currentSliderVal < minSliderVal) {
                result = minSliderVal;
            } else if (currentSliderVal > maxSliderVal) {
                result = maxSliderVal;
            } else {
                result = currentSliderVal;
            }

            return percent ? result * 10 : result;
        };

        // стрим в который пушатся значения при дрэге
        const mousedrag = flatMap(function () {
            const move = mousemove.map(calcNewSliderValue);
            return takeUntil(throttle(100, move), mouseup);
        }, mousedown);

        /**
         * Функция пушит событие в два стрима
         * чтобы при клике установить текущее значение и начать слушать дрэг
         * @param {Object} e event
         */
        this.setAndMousedown = e => setImmediate(e) && mousedown(e);
        // стрим значений слайдера от кликов и от дрэга
        const valueProviders = flyd.merge(mousedrag, setImmediate.map(calcNewSliderValue));

        // навешиваем обработчики
        caret.addEventListener('mousedown', mousedown);
        bar.addEventListener('mousedown', this.setAndMousedown);
        line.addEventListener('mousedown', this.setAndMousedown);
        document.addEventListener('mousemove', mousemove);
        document.addEventListener('mouseup', mouseup);

        // пушим значение на верх
        flyd.on(newValue => onChange(this.eventKey, newValue), valueProviders);

        // сохраняем стримы для отписки
        this.slider$ = {mousedown, mousemove, mouseup, mousedrag, setImmediate};
        this.inited = true;

        // обрабатываем первое событие
        this.setAndMousedown(firstClickEvent);
    }

    /**
     * Убиваем стримы и отписываемся от событий
     */
    cleanUp() {
        const {caret, bar, line} = this.refs;
        const {mousedown, mousemove, mouseup, mousedrag, setImmediate} = this.slider$;

        caret.removeEventListener('mousedown', mousedown);
        bar.removeEventListener('mousedown', this.setAndMousedown);
        line.removeEventListener('mousedown', this.setAndMousedown);
        document.removeEventListener('mousemove', mousemove);
        document.removeEventListener('mouseup', mouseup);

        mousedown.end(true);
        mousemove.end(true);
        mouseup.end(true);
        mousedrag.end(true);
        setImmediate.end(true);
    }

    renderGridItem(stepNumber) {
        const {stepPercent} = this.sliderSpec;
        const currentPoint = this.percent ? stepNumber : stepNumber * stepPercent;

        return (
            <span key={stepNumber}>
                <span
                    className="irs-grid-text js-grid-text-0"
                    style={{left: `${currentPoint}%`, marginLeft: '-1%'}}
                >
                    {stepNumber}
                </span>
            </span>
        );
    }

    render() {
        const {step = 10, value} = this.props;
        const {stepPercent, stepRange} = this.sliderSpec;
        const caretPosition = this.percent ? `${value}%` : `${value * stepPercent}%`;

        return (
            <div
                ref="slider"
                className="primary"
                style={{width: '100%'}}
            >
                <span className="irs irs-with-grid">
                    <span className="irs">
                        <span
                            ref="line"
                            className="irs-line"
                            tabindex="-1"
                            style={{cursor: 'pointer'}}
                            onMouseDown={this.initSlider}
                        >
                            <span className="irs-line-left"/>
                            <span className="irs-line-mid"/>
                            <span className="irs-line-right"/>
                        </span>
                        <span
                            className="irs-single"
                            style={{
                                left: caretPosition,
                                marginLeft: this.percent ? '-12px' : '-5.5px',
                                width: this.percent ? '30px' : '16px',
                                textAlign: 'center',
                                padding: '1px 4px'
                            }}
                        >
                            {this.percent ? caretPosition : value}
                        </span>
                    </span>

                    <span
                        className="irs-grid"
                        style={{width: '100%', height: '16px'}}
                    >
                        {stepRange.map(this.renderGridItem)}
                        <span
                            className="irs-grid-text js-grid-text-0"
                            style={{left: `${step * stepPercent}%`, marginLeft: '-1%'}}
                        >
                            {step}
                        </span>
                    </span>

                    <span
                        ref="bar"
                        className="irs-bar"
                        style={{width: caretPosition, cursor: 'pointer'}}
                        onMouseDown={this.initSlider}
                    />
                    <span
                        className="irs-bar-edge"
                        onMouseDown={this.initSlider}
                    />
                    <span 
                        ref="caret"
                        style={{left: caretPosition}}
                        className="irs-slider single"
                        onMouseDown={this.initSlider}
                    />
                </span>
            </div>
        );
    }
}

export default Slider;
