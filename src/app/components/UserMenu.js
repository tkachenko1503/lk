import React from 'react';
import Dropdown from 'components/Dropdown';
import MenuItem from 'react-bootstrap/lib/MenuItem';

const UserMenu = (props) => {
    const {username, logOut} = props;
    
    return  (
        <div className="right-notification">
            <ul className="notification-menu">
                <li>
                    <Dropdown
                        id="sites-select"
                        title={username}
                        className="dropdown-menu dropdown-usermenu purple pull-right"
                    >
                        <MenuItem onSelect={logOut}>
                            <i className="fa fa-sign-out pull-right"></i>
                            Выход
                        </MenuItem>
                    </Dropdown>
                </li>
            </ul>
        </div>
    );
};

export default UserMenu;
