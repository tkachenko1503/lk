import {getUser} from 'lib/auth';
import curryN from 'ramda/src/curryN';
import {User} from 'actions/types';

const meta = {
    error: false,
    pending: false
};

export const defaultUser = () => ({
    current: getUser(),
    isAdmin: false,
    meta
});

const reducefn = User.caseOn({
    ResetUser: state => defaultUser(),

    LogIn: (user, state) => ({
        ...state,
        meta: meta,
        ...user
    }),

    SetAdminRights: (rights, state) => ({
        ...state,
        ...rights
    })
});

export default curryN(2, reducefn);
