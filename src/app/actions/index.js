import flyd from 'flyd';

const actions$ = flyd.stream();

export default actions$;