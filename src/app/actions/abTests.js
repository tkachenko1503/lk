import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import actions$ from 'actions/index';
import {App, ABTests} from 'actions/types';
import ABTest from 'models/ABTest';
import ABTestItem from 'models/ABTestItem';
import {downloadTxtFile} from 'lib/download';

export const makeAction = flyd.stream();
export const resetAction = flyd.stream();
export const setNewTestName = flyd.stream();
export const setWidgetId = flyd.stream();
export const setWidgetFrequency = flyd.stream();
export const addWidgetToTest = flyd.stream();
export const saveTest = flyd.stream();
export const removeWidgetFromTest = flyd.stream();
export const fetchABTests = flyd.stream();
export const renameABTest = flyd.stream();
export const downloadTestCode = flyd.stream();
export const removeTest = flyd.stream();
export const fetchABTestItems = flyd.stream();
export const saveABTestSettings = flyd.stream();
export const setWidgetFrequencyToExistenTest = flyd.stream();
export const cancelTestItemsChanges = flyd.stream();
export const addWidgetToExistenTest = flyd.stream();
export const setWidgetToNewTestItem = flyd.stream();
export const removeTestItem = flyd.stream();
export const toggleRemoveDialog = flyd.stream();

flyd.on(downloadTxtFile, downloadTestCode);

flyd.on(
    pipe(App.ABTests, actions$),
    mergeAll([
        makeAction.map(ABTests.MakeABTestsAction),
        resetAction.map(ABTests.ResetABTestsAction),
        setNewTestName.map(ABTests.SetTestName),
        setWidgetId.map(ABTests.SetWidgetId),
        setWidgetFrequency.map(ABTests.SetWidgetFrequency),
        addWidgetToTest.map(_ => ABTests.AddTestItem()),
        saveTest.map(dispatchFromPromise),
        removeWidgetFromTest.map(ABTests.RemoveTestItem),
        fetchABTests
            .map(ABTest.getTestsBySite)
            .map(ABTests.SetAvailableTests),
        renameABTest
            .map(ABTest.updateName)
            .map(ABTests.UpdateTest),
        removeTest
            .map(ABTest.removeFromAccount)
            .map(ABTests.RemoveTest),
        fetchABTestItems
            .map(ABTest.getABTestItems)
            .map(setTestItems),
        saveABTestSettings
            .map(ABTest.saveAllItems)
            .map(test => ABTests.ResetABTestsAction(test.id)),
        setWidgetFrequencyToExistenTest
            .map(ABTestItem.setFrequency)
            .map(() => ABTests.UpdateState()),
        cancelTestItemsChanges
            .map(ABTest.cancelItemsChanges)
            .map(ABTests.CancelABTestsChanges),
        addWidgetToExistenTest.map(createNewTestItem),
        setWidgetToNewTestItem.map(setWidget),
        removeTestItem.map(removeOrBackupTestItem),
        toggleRemoveDialog.map(ABTests.ToggleRemoveDialog)
    ])
);

function setTestItems({test, testItems}) {
    return ABTests.SetTestItems(test.id, testItems);
}

function dispatchFromPromise(data) {
    return ABTest
        .spawn(data)
        .then(t => t.fetch())
        .then(ABTests.SaveTest)
        .catch(ABTests.SetError);
}

function createNewTestItem(test) {
    const newItem = new ABTestItem();
    return ABTests.AddItemToExistenTest(test.id, newItem);
}

function setWidget({testItem, widget}) {
    testItem.set('Widget', widget);
    return ABTests.UpdateState();
}

function removeOrBackupTestItem({test, testItem}) {
    if (testItem.isNew()) {
        return ABTests.RemoveTestItemFromExistedTest(test.id, testItem);
    }

    ABTest.removeTestItem(test, testItem);
    return ABTests.RemoveAndBackupTestItemFromExistedTest(test.id, testItem);
}
