import React from 'react';
import Table from 'react-bootstrap/lib/Table';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';
import AdsCampaignListItem from 'modules/adsCampaigns/AdsCampaignListItem';

import styles from 'styles/widgetList.css';
import table from 'styles/widgetTable.css';
import removeModal from 'styles/removeModal.css';

class AdsCampaignsList extends React.Component {
    constructor(props) {
        super(props);

        this.renderAdsCampaign = this.renderAdsCampaign.bind(this);
    }

    renderAdsCampaign(adsCampaign) {
        const {openRemoveDialog, action, makeAction} = this.props;

        return (
            <AdsCampaignListItem
                key={adsCampaign.id}
                adsCampaign={adsCampaign}
                action={action.id === adsCampaign.id ? action : null}
                openRemoveDialog={openRemoveDialog}
                makeAction={makeAction}
            />
        );
    }

    render() {
        const {adsCampaignsList, removeDialog, removeAdsCampaign, closeRemoveAdsCampaign} = this.props;

        return (
            <div className="ads-campaigns-list">
                <Table
                    striped condensed responsive
                    className={styles.widgetList}
                >
                    <thead>
                        <tr>
                            <th key="name" className={table.nameCol}>Имя</th>
                            <th key="id" className={table.idCol}>ID</th>
                            <th key="actions" className={table.actionsCol}>Действия</th>
                            <th key="remove" className={table.removeCol}>Удалить</th>
                        </tr>
                    </thead>

                    <tbody>
                        {adsCampaignsList.map(this.renderAdsCampaign)}
                    </tbody>
                </Table>

                <Modal
                    show={removeDialog.open}
                    onHide={closeRemoveAdsCampaign}
                    animation={false}
                >
                    <Modal.Body>
                        <h4 className={removeModal.message}>
                            Вы уверенны что хотите удалить эту рекламную кампанию?
                        </h4>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={removeAdsCampaign}>Да</Button>
                        <Button onClick={closeRemoveAdsCampaign}>Нет</Button>
                    </Modal.Footer>
                </Modal>
            </div>

        );
    }
}

export default AdsCampaignsList;
