import pipe from 'ramda/src/pipe';

const updateAdsCampaign = ({adsCampaign, attrs}) => {
    adsCampaign.set(attrs);
    return adsCampaign;
};
const saveAdsCampaign = (adsCampaign) => adsCampaign.save(null);

export const updateAndSaveAdsCampaign = pipe(updateAdsCampaign, saveAdsCampaign);