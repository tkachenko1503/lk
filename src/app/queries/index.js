import queryString from 'query-string';
import { download } from '../lib/download';
import { statsQS } from '../lib/csv';

function buildAdsPaymentsUrl(baseUrl, params) {
    const siteId = params.siteId;
    const { from, to } = params.filters.date;

    const queryParams = queryString.stringify({
        siteId,
        from: from.format(),
        to: to.format()
    });

    return `${baseUrl}?${queryParams}`;
}


function fetchStats(params, baseUrl) {
    const query = statsQS(params);
    const url = `${baseUrl}/${params.site}?${query}`;

    return fetch(url)
        .then(res => res.json());
}

export function getSitesList() {
    return fetch('/api/sites')
        .then(res => res.json());
}

export function getSiteStats(params) {
    return fetchStats(params, '/api/stats');
}

export function getSiteAdsStats(params) {
    return fetchStats(params, '/api/blurb');
}

export function getSiteArticlesStats(params) {
    return fetchStats(params, '/api/articles')
        .then(({ stats }) => stats);
}

export function getAdvertisersContent(params) {
    return fetchStats(params, '/api/advertisers/content')
        .then(({ content }) => content);
}

export function getSiteStatsCSV(params) {
    return fetchStats(params, '/api/stats/csv')
        .then(data => ({ params, data }));
}


export function getSiteAdsPayments(params) {
    const url = buildAdsPaymentsUrl('/api/blurb/payments', params);

    return fetch(url)
        .then(res => res.json());
}

export function getSiteAdsPaymentsCSV(params) {
    const url = buildAdsPaymentsUrl('/api/csv/blurb/payments', params);

    download(url);
}

export function getDashboardStatisticsPdf(params) {
    const query = statsQS(params);
    const url = `/api/statistics/pdf/${params.site}?${query}`;
    const from = params.filters.date.from;
    const to = params.filters.date.to;
    const dates = `с_${from.format('DD-MM-YY')}_до_${to.format('DD-MM-YY')}`;
    const name = `statistics__${params.site}__${dates}.pdf`;

    download(url, name);
}
