import React from 'react';
import connectToStore from 'lib/connectToStore';
import {updateWidget, fetchWidgets, resetWidgetsChanges, saveAllWidgets} from 'actions/widgets';
import {setAppContext} from 'modules/appContext/actions';
import {findByProp} from 'lib/index';
import TrafficExchange from 'components/TrafficExchange';

class TrafficExchangeContainer extends React.Component {
    constructor(props) {
        super(props);

        this.setExchangePlaces = this.setExchangePlaces.bind(this);
        this.saveAllChanges = this.saveAllChanges.bind(this);
    }

    componentWillMount() {
        const {currentSite, widgetsList} = this.props;

        if (currentSite && !widgetsList.length) {
            fetchWidgets(currentSite);
        }

        this.contextChangeHandler = setAppContext.map(fetchWidgets);
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
        resetWidgetsChanges(this.props.widgetsList);
    }

    setExchangePlaces(widgetId, exchangePlaces) {
        const {widgetsList} = this.props;

        updateWidget({
            widget: findByProp(widgetId, widgetsList, 'id'),
            attrs: {
                exchangePlaces
            }
        });
    }

    saveAllChanges() {
        saveAllWidgets(this.props.widgetsList);
    }

    render() {
        const {widgetsList, currentSite} = this.props;

        return (
            <div>
                <TrafficExchange
                    widgetsList={widgetsList}
                    setExchangePlaces={this.setExchangePlaces}
                    saveAllChanges={this.saveAllChanges}
                    currentSite={currentSite}
                />
            </div>

        );
    }
}

export default connectToStore(TrafficExchangeContainer, ({app, widgets}) => ({
    currentSite: app.selectedContext,
    widgetsList: widgets.list
}));
