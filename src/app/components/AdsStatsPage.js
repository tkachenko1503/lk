import React from 'react';
import Layout from 'components/Layout';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import DashboardFilters from 'containers/DashboardFilters';
import AdsStatsOverview from 'containers/AdsStatsOverview';
import AdsStatsChartContainer from 'containers/AdsStatsChartContainer';
import AdsStatsTableContainer from 'containers/AdsStatsTableContainer';

import styles from 'styles/dashboard.css';

const AdsStatsPage = (props) => {
    return (
        <Layout>
            <div className="ads-statistic-container">
                <DashboardFilters
                    actionForApplyFilters={props.actionForApplyFilters}
                    widgetSelectWithIcon={true}
                    allWidgetTypes={true}
                />
                <div className="wrapper">
                    <AdsStatsOverview />

                    <Grid fluid={true} className={styles.dashboardGrid}>
                        <Row>
                            <Col xs={12}>
                                <AdsStatsChartContainer />
                            </Col>
                            <Col xs={12}>
                                <AdsStatsTableContainer />
                            </Col>
                        </Row>
                    </Grid>
                </div>
            </div>
        </Layout>
    );

};

export default AdsStatsPage;
