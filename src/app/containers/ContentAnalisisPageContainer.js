import React from 'react';
import connectToStore from 'lib/connectToStore';
import {setAppContext} from 'modules/appContext/actions';
import {requestArticlesStatsForSite} from 'actions/articlesStats';
import {findByProp} from 'lib/index';
import ContentAnalisisPage from 'components/ContentAnalisisPage';

class ContentAnalisisPageContainer extends React.Component {
    constructor(props) {
        super(props);
        this.requestStats = this.requestStats.bind(this);
    }

    componentDidMount() {
        const {selectedContext} = this.props;

        if (selectedContext) {
            this.requestStats(selectedContext);
        }
        this.contextChangeHandler = setAppContext.map((s) => {
            setTimeout(() => this.requestStats(s), 0);
        });
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
    }

    getSiteById(siteId) {
        return findByProp(siteId, this.props.context, 'id');
    }

    requestStats(siteId) {
        const site = this.getSiteById(siteId);

        requestArticlesStatsForSite({
            site: site && site.get('name'),
            filters: this.props.filters
        });
    }

    render() {
        return (
            <ContentAnalisisPage
                actionForApplyFilters={requestArticlesStatsForSite}
            />
        );
    }
}

export default connectToStore(ContentAnalisisPageContainer, ({app, filters, widgets}) => ({
    context: app.context,
    selectedContext: app.selectedContext,
    filters: filters,
    widgetList: widgets.list
}));
