import React from 'react';
import Label from 'react-bootstrap/lib/Label';
import EscKeyClosable from 'components/EscKeyClosable';
import {closeWidgetAction} from 'actions/widgets';
import {getWidgetSourceCode} from 'lib/widgets';

import styles from 'styles/widget.css';

class WidgetDownloadAction extends EscKeyClosable {
    constructor(props) {
        super(props);

        this.closeAction = closeWidgetAction;
    }

    widgetHTML() {
        const {widget} = this.props;
        return getWidgetSourceCode(widget);
    }

    render() {
        return (
            <div>
                <Label>
                    Код виджета
                </Label>
                <pre className={styles.downloadPreview}>
                    {this.widgetHTML()}
                </pre>
            </div>
        );
    }
}

export default WidgetDownloadAction;
