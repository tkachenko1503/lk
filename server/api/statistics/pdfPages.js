import pdf from 'html-pdf';
import moment from 'moment';
import Promise from 'bluebird';
import { statsByContext } from './resource';
import { render } from '../../lib/renderer';
import { formatStatsForStore, defaultStats } from '../../../src/app/reducers/stats';
import { chartConfig } from '../../../src/app/modules/dashboard/chartConfigs';
import { getFromTo } from '../../lib/index';
import { summariesBuildFn } from '../../../src/app/lib/index';
import { formatNumber, DASHBOARD_ADVERTISER_STATS_SUMMARIES } from '../../../src/app/lib/constants';
import { prepareRequestParams } from '../../modules/accounts/utils';
import { getAccountHtmlData } from '../../modules/accounts/data';

const fields = ['load', 'shows', 'referrer', 'ctr'];

function getSummariesByContextType(contextType) {
    return contextType === 'Site'
        ? [
            {
                title: 'Загрузки виджета',
                format: formatNumber
            },
            {
                title: 'Показы виджета',
                format: formatNumber
            },
            {
                title: 'Клики по виджету',
                format: formatNumber
            },
            {
                title: 'CTR виджета',
                format: (s) => `${s}%`
            }
        ]
        : DASHBOARD_ADVERTISER_STATS_SUMMARIES;
}

const makePdf = res =>
    html => pdf.create(html).toStream((error, stream) => {
        if (error) {
            console.log(error.message);
        } else {
            stream.pipe(res);
        }
    });


const renderStats = (site, contextType, filters) =>
    ([statsData, htmlReportData]) => {
        const hoursOffset = filters.date.from.utcOffset() / 60;

        const stats = formatStatsForStore(statsData, defaultStats(), hoursOffset);
        const efficiencyConfig = chartConfig({
            first: contextType === 'Site' ? 'Показы виджета' : 'Показы объявлений',
            second: 'Клики',
            col1: stats.efficiency.impression,
            col2: stats.efficiency.referrer,
            created: stats.efficiency.created,
            dateRange: filters.date
        });

        const additionalRedirects = chartConfig({
            first: 'Загрузки виджета',
            second: 'CTR',
            col1: stats.efficiency.load,
            col2: stats.efficiency.ctr,
            created: stats.efficiency.created,
            dateRange: filters.date,
            secondTickFormat: n => n.toFixed(1)
        });

        const summariesByContext = getSummariesByContextType(contextType);
        const summaries = summariesBuildFn(fields, summariesByContext)(stats.overview);

        return render({
            data: {
                stats,
                filters,
                contextType
            },
            prettyDates: {
                from: moment(filters.date.from).format('DD/MM/YYYY'),
                to: moment(filters.date.to).format('DD/MM/YYYY')
            },
            htmlReportData,
            site,
            efficiencyConfig,
            additionalRedirects,
            summaries: summaries.map(({ value, title, format }) => ({
                value: format ? format(value) : value,
                title
            }))
        });
    };

/**
 * Генерит pdf с граффиками с главной страницы
 *
 * @param req
 * @param res
 * @param next
 */
export function dashboardStats(req, res, next) {
    const site = req.params.site;
    const contextType = req.query.contextType;
    const filters = {
        date: getFromTo(req.query)
    };
    const requestParams = prepareRequestParams(req);

    Promise.all([
        statsByContext({
            site,
            params: {
                ...req.query,
                widget: undefined
            }
        }),
        getAccountHtmlData(requestParams)
    ])
        .then(renderStats(site, contextType, filters))
        .then(makePdf(res))
        .catch(next);
}
