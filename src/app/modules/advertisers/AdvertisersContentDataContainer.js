import React from 'react';
import connectToStore from 'lib/connectToStore';
import {fetchAdvertisersContent} from 'modules/advertisers/actions';
import {setAppContext} from 'modules/appContext/actions';
import {findByProp} from 'lib/index';

class AdvertisersContentDataContainer extends React.Component {
    componentWillMount() {
        const {currentContext, filters} = this.props;

        if (currentContext) {
            fetchAdvertisersContent({
                site: currentContext.get('name'),
                filters:  filters
            });
        }
        this.contextChangeHandler = setAppContext.map(id => fetchAdvertisersContent({
            site: findByProp(id, this.props.appContext, 'id').get('name'),
            filters:  filters
        }));
    }

    componentWillUnmount() {
        this.contextChangeHandler.end(true);
    }

    render() {
        return null;
    }
}

export default connectToStore(AdvertisersContentDataContainer, ({advertisers, app, filters}) => ({
    currentContext: app.selectedContext && findByProp(app.selectedContext, app.context, 'id'),
    appContext: app.context,
    filters: filters,
}));
