import Promise from 'bluebird';

// Оборачиваем методы json2csv в промисы
const json2csv = Promise.promisify(require("json2csv"));

export default json2csv;
