import React from 'react';
import classnames from 'classnames';
import curry from 'ramda/src/curry';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import Button from 'react-bootstrap/lib/Button';
import {ADS_CHART_BUTTONS} from 'lib/constants';

import chartControls from 'styles/adsChartControls.css';

const AdsStatsChartControls = ({params, handler}) => {
    const buildFirstParam = chartControlsButton(params.first, params.second, chartControlsHandler(handler, 'first'));
    const buildSecondParam = chartControlsButton(params.second, params.first, chartControlsHandler(handler, 'second'));

    return (
        <div>
            <h6>
                Выберите по одному параметру из каждой строки
                для одновременного показа на графике
            </h6>

            <div className={chartControls.controls}>
                <div className={chartControls.parametrRow}>
                    <span className={classnames(
                            "label label-success",
                            chartControls.parametrLabel
                        )}
                    >
                        Параметр 1
                    </span>
                    <ButtonGroup>
                        {ADS_CHART_BUTTONS.map(buildFirstParam)}
                    </ButtonGroup>
                </div>

                <div className={chartControls.parametrRow}>
                    <span className={classnames(
                            "label label-info",
                            chartControls.parametrLabel
                        )}
                    >
                        Параметр 2
                    </span>
                    <ButtonGroup>
                        {ADS_CHART_BUTTONS.map(buildSecondParam)}
                    </ButtonGroup>
                </div>
            </div>

        </div>
    );
};

export default AdsStatsChartControls;

const chartControlsButton = curry((activeButtonId, disabledButtonId, handler, {id, title}, i) => {
    const active = activeButtonId === id;
    const isActive = {
        default: !active,
        primary: active
    };

    return (
        <Button
            key={i}
            bsStyle={classnames(isActive)}
            onClick={handler}
            data-button-id={id}
            disabled={id === disabledButtonId}
        >
            {title}
        </Button>
    );
});

const chartControlsHandler = curry((handler, paramId, e) => {
    const buttonId = e.target.dataset.buttonId;
    handler({
        buttonId,
        paramId
    });
});
