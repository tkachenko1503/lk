import moment from 'moment';
import json2csv from 'lib/json2csv';
import {downloadCSVFile} from 'lib/download';

const TRAFFIC_CSV_FIELDS = [
    {label: 'Дата', value: createdFormat},
    {label: 'Показы', value: 'ads_shows'},
    {label: 'Клики', value: 'ads_referrer'},
    {label: 'Загрузки', value: 'widget_load'},
    {label: 'CTR', value: formatCtr}
];

function createdFormat(row) {
    return moment(row.created).format('DD/MM/YYYY');
}

function formatCtr(row) {
    return `${row.ctr}%`;
}

function generateCSVData(rows, currentSite, currentWidget) {
    const siteAndWidget = [
        {label: 'Вебсайт', value: 'site', default: currentSite},
        {label: 'Виджет', value: 'widget', default: (currentWidget || 'All')}
    ];

    return json2csv({
        del: '.',
        defaultValue: 0,
        data: rows,
        fields: siteAndWidget.concat(TRAFFIC_CSV_FIELDS)
    });
}

export function downloadTrafficCSV({rows, datesRange, currentSite, currentWidget}) {
    generateCSVData(rows, currentSite, currentWidget)
        .then(data => downloadCSVFile({
            name: currentSite,
            range: datesRange,
            url: encodeURI("data:text/csv;charset=utf-8," + data),
            widget: currentWidget
        }));
}
