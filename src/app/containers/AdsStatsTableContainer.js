import React from 'react';
import connectToStore from 'lib/connectToStore';
import AdsStatsTablePanel from 'components/AdsStatsTablePanel';
import {setAdsControl, exportAdsCsvFile} from 'actions/adsStatsControls';
import {setAdsStatsForSite} from 'actions/adsStats';
import {findByProp, applyControls} from 'lib/index';

class AdsStatsTableContainer extends React.Component {
    constructor(props) {
        super(props);

        this.setTableSort = this.setTableSort.bind(this);
        this.exportTableInCsv = this.exportTableInCsv.bind(this);
    }

    componentDidMount() {
        this.resetPagerOnNewStats = setAdsStatsForSite.map(() => {
            this.setTablePage(null, {eventKey: 1})
        });
    }

    componentWillUnmount() {
        this.resetPagerOnNewStats.end(true);
    }

    setTableSize(e) {
        const value = e.target.value;
        setAdsControl({
            control: 'size',
            value: Number(value)
        });
    }
    
    setTablePage(e, {eventKey}) {
        setAdsControl({
            control: 'page',
            value: Number(eventKey)
        });
    }

    setTableSort(e) {
        const {id, type} = this.props.tableSort;
        const sortId = e.currentTarget.dataset.sortId;
        const value = {
            id: sortId,
            type: id === sortId && type === 'desc' ? 'asc' : 'desc'
        };

        setAdsControl({
            control: 'sort',
            value
        });
    }
    
    exportTableInCsv() {
        const {fullAdsList, datesRange} = this.props;
        const {currentSiteId, availableSites} = this.props;
        const {currentWidgetId, widgetsList} = this.props;

        const site = findByProp(currentSiteId, availableSites, 'id');
        const widget = currentWidgetId && findByProp(currentWidgetId, widgetsList, 'id');

        exportAdsCsvFile({
            rows: fullAdsList,
            datesRange, 
            currentSite: site && site.get('name'),
            currentWidget: widget && widget.get('name')
        });
    }

    render() {
        const {rows, rowsSummary, tableSort, datesRange} = this.props;
        const {pagerSize, activePage, currentSize} = this.props;

        return (
            <AdsStatsTablePanel
                rows={rows}
                rowsSummary={rowsSummary}
                tableSort={tableSort}
                pagerSize={pagerSize}
                activePage={activePage}
                currentSize={currentSize}
                datesRange={datesRange}
                setTableSort={this.setTableSort}
                setTableSize={this.setTableSize}
                setTablePage={this.setTablePage}
                exportTableInCsv={this.exportTableInCsv}
            />
        );
    }
}

export default connectToStore(AdsStatsTableContainer, ({adsStats, adsStatsControls, filters, app, widgets}) => ({
    rows: applyControls(adsStatsControls, adsStats.statsList),
    rowsSummary: adsStats.statsSummaries,
    tableSort: adsStatsControls.sort,
    pagerSize: Math.ceil(adsStats.statsList.length / adsStatsControls.size),
    activePage: adsStatsControls.page,
    currentSize: adsStatsControls.size,
    datesRange: filters.date,
    currentSiteId: app.selectedContext,
    availableSites: app.context,
    currentWidgetId: filters.widget,
    fullAdsList: adsStats.statsList,
    widgetsList: widgets.list
}));
