import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import takeUntil from 'flyd/module/takeuntil';
import flatMap from 'flyd/module/flatmap';
import afterSilence from 'flyd/module/aftersilence';
import pipe from 'ramda/src/pipe';
import invoker from 'ramda/src/invoker';
import map from 'ramda/src/map';
import T from 'ramda/src/T';
import actions$ from 'actions/index';
import Widget from 'models/Widget';
import {logger} from 'lib/index';
import {App, Widgets} from 'actions/types';
import {downloadWidgetsExportCodes} from 'lib/widgets';

export const makeAction = flyd.stream();
export const saveWidget = flyd.stream();
export const fetchWidgets = flyd.stream();
export const removeWidget = flyd.stream();
export const updateAndSaveWidget = flyd.stream();
export const updateWidget = flyd.stream();
export const downloadWidgets = flyd.stream();
export const closeWidgetAction = flyd.stream();
export const setWidgetMeta = flyd.stream();
export const resetWidgetsChanges = flyd.stream();
export const saveAllWidgets = flyd.stream();
export const initSavableListener = flyd.stream();
export const toggleRemoveDialog = flyd.stream();

const resetAction = mergeAll([
    saveWidget,
    updateAndSaveWidget,
    downloadWidgets
]);

const setAdsSavable = flatMap(function () {
    const savable = updateWidget.map(T);
    return takeUntil(savable, afterSilence(0, savable));
}, initSavableListener);

// @todo вынести в модуль widgets
const update = ({widget, attrs}) => {
    widget.set(attrs);
    return widget;
};
const save = (widget) => widget.save(null);
const updateAndSave = pipe(update, save);
const revertWidget = invoker(0, 'revert');
const revertAllWidgets = map(revertWidget);

// @todo можно оптимизировать и не каждый раз создавать новый стрим
flyd.on(() => flyd.endsOn(resetAction, closeWidgetAction.map(resetAction)), makeAction);

flyd.on(downloadWidgetsExportCodes, downloadWidgets);

flyd.on(
    pipe(App.Widgets, actions$),
    mergeAll([
        makeAction.map(Widgets.MakeWidgetAction),

        removeWidget
            .map(Widget.removeFromAccount)
            .map(widget => Widgets.RemoveWidget(widget.id)),

        saveWidget
            .map(Widget.spawn)
            .map(Widgets.SaveWidget),

        fetchWidgets
            .map(Widget.getBySite)
            .map(Widgets.SetWidgets),

        updateWidget
            .map(update)
            .map(Widgets.UpdateWidget),

        updateAndSaveWidget
            .map(updateAndSave)
            .map(Widgets.UpdateAndSaveWidget),

        resetAction
            .map(() => Widgets.ResetWidgetAction()),

        setWidgetMeta
            .map(err => ({error: true, ...err}))
            .map(Widgets.SetWidgetMeta),

        resetWidgetsChanges
            .map(revertAllWidgets)
            .map(Widgets.UpdateWidget),

        saveAllWidgets
            .map(Widget.saveAll)
            .map(Widgets.SetWidgets),

        setAdsSavable
            .map(() => Widgets.SetWidgetsAdsSavable()),

        toggleRemoveDialog
            .map(Widgets.ToggleRemoveDialog)
    ])
);
