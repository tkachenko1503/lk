import React from 'react';
import connectToStore from 'lib/connectToStore';
import AdvertisersContentTable from 'modules/advertisers/AdvertisersContentTable';
import AdvertisersContentDataContainer from 'modules/advertisers/AdvertisersContentDataContainer';
import {setAdvertisersContent, setAdvertisersContentControl, exportAdvertisersContentCsvFile} from 'modules/advertisers/actions';
import {applyControls, findByProp} from 'lib/index';

class AdvertisersContentTableContainer extends React.Component {
    constructor(props) {
        super(props);

        this.setTableSort = this.setTableSort.bind(this);
        this.exportTableInCsv = this.exportTableInCsv.bind(this);
    }

    componentDidMount() {
        this.resetPagerOnNewStats = setAdvertisersContent.map(() => {
            this.setTablePage(null, {eventKey: 1})
        });
    }

    componentWillUnmount() {
        this.resetPagerOnNewStats.end(true);
    }

    setTablePage(e, {eventKey}) {
        setAdvertisersContentControl({
            control: 'page',
            value: Number(eventKey)
        });
    }

    setTableSize(e) {
        const value = e.target.value;
        setAdvertisersContentControl({
            control: 'size',
            value: Number(value)
        });
    }

    setTableSort(e) {
        const {id, type} = this.props.tableSort;
        const sortId = e.currentTarget.dataset.sortId;
        const value = {
            id: sortId,
            type: id === sortId && type === 'desc' ? 'asc' : 'desc'
        };

        setAdvertisersContentControl({
            control: 'sort',
            value
        });
    }

    exportTableInCsv() {
        const {fullAdsList, datesRange} = this.props;
        const {currentSiteId, availableSites} = this.props;
        const {currentWidgetId, widgetsList} = this.props;

        const site = findByProp(currentSiteId, availableSites, 'id');
        const widget = currentWidgetId && findByProp(currentWidgetId, widgetsList, 'id');

        exportAdvertisersContentCsvFile({
            rows: fullAdsList,
            datesRange,
            currentSite: site && site.get('name'),
            currentWidget: widget && widget.get('name')
        });
    }
    render() {
        const {rows, tableSort, datesRange} = this.props;
        const {pagerSize, activePage, currentSize} = this.props;

        return (
            <div>
                <AdvertisersContentDataContainer />
                <AdvertisersContentTable rows={rows}
                                         tableSort={tableSort}
                                         pagerSize={pagerSize}
                                         activePage={activePage}
                                         currentSize={currentSize}
                                         datesRange={datesRange}
                                         setTableSort={this.setTableSort}
                                         setTableSize={this.setTableSize}
                                         setTablePage={this.setTablePage}
                                         exportTableInCsv={this.exportTableInCsv}/>
            </div>
        )
    }
}

export default connectToStore(AdvertisersContentTableContainer, ({advertisers, filters, app}) => ({
    rows: applyControls(advertisers.contentTable, advertisers.contentList),
    tableSort: advertisers.contentTable.sort,
    pagerSize: Math.ceil(advertisers.contentList.length / advertisers.contentTable.size),
    activePage: advertisers.contentTable.page,
    currentSize: advertisers.contentTable.size,
    datesRange: filters.date,
    currentSiteId: app.selectedContext,
    availableSites: app.context,
    currentWidgetId: filters.widget,
    fullAdsList: advertisers.contentList
}));
