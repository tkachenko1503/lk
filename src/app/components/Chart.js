import React from 'react';
import { findDOMNode } from 'react-dom';
import c3 from 'c3/c3.min';

import 'style-loader!raw!c3/c3.min.css';

export default class Chart extends React.Component {
    componentWillReceiveProps(newProps) {
        const {config, redraw} = newProps;

        this.updateChart(config, redraw);
    }

    componentWillUnmount() {
        this.destroyChart();
    }

    componentDidMount() {
        const {config, redraw} = this.props;

        this.updateChart(config, redraw);
    }

    generateChart(mountNode, config) {
        return c3.generate({
            bindto: mountNode,
            ...config
        });
    }

    destroyChart() {
        try {
            this.chart = this.chart.destroy();
        } catch (err) {
            throw new Error('Internal C3 error', err);
        }
    }

    updateChart(config, redraw = false) {
        if (this.chart && redraw) {
            this.destroyChart();
        } else if (this.chart) {
            this.chart.load(config.data);
            return;
        }

        this.chart = this.generateChart(findDOMNode(this), config);
    }

    render() {
        const className = this.props.className ? ` ${this.props.className}` : '';
        const style = this.props.style ? this.props.style : {};
        return <div className={className} style={style} />;
    }
}
