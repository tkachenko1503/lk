import React from 'react';
import classNames from 'classnames';
import Alert from 'react-bootstrap/lib/Alert';
import {resetError} from 'actions/user';
import {AUTH_ERROR_MESSAGES} from 'lib/constants';

import styles from 'styles/auth.css';

/**
 * Абстрактный класс для страниц логина и регистрации
 * содержит общие для них методы
 * @constructor
 */
class AuthPage extends React.Component {
    constructor(props) {
        super(props);

        this.title = '';
    }

    componentDidUpdate() {
        const {user, router} = this.props;

        // Костыль! Должно тригериться через экшн
        // Но сейчас нет провязки с роутером
        // после успешной регистрации редиректим на главную страницу
        if (user.current && !user.meta.error) {
            setTimeout(() => router.replace('/'), 0);
        }
    }

    componentWillUnmount() {
        const {meta} = this.props.user;

        // Убираем ошибки авторизации при переходе на другую страницу
        if (meta.error) {
            resetError(true);
        }
    }

    /**
     * Отрисовываем форму логина/регистрации
     */
    renderFrom() {
        const {meta} = this.props.user;

        return (
            <form
                className="form-signin"
                onSubmit={this.props.authSubmit}
            >
                <div className="login-wrap">
                    {meta.error
                        ? this.getAlert(meta.errorCode)
                        : null}

                    {this.renderFormControls()}

                    {this.renderFooter()}
                </div>
            </form>
        );
    }

    /**
     * Заглушка, переопределяется в наследниках
     * @returns {Array}
     */
    renderFormControls() {
        return [];
    }

    /**
     * Заглушка, переопределяется в наследниках
     * @returns {Array}
     */
    renderFooter() {
        return [];
    }

    /**
     * Если знаем как показать ошибку выводим алерт с текстом ошибки
     * @param {Number} code - код ошибки
     */
    getAlert(code) {
        const message = AUTH_ERROR_MESSAGES[code];

        if (message) {
            return (
                <Alert bsStyle="warning">
                    {message}
                </Alert>
            );
        }
        return null;
    }

    render() {
        return (
            <div className={classNames(
                "login-body",
                styles.authPage
            )}>
                <div className="login-logo">
                    <img
                        src="/vendor/img/Natimatica-logo.png"
                        className={styles.logo}
                    />
                </div>

                <h2 className="form-heading">
                    {this.title}
                </h2>
                <div className="container log-row">
                    {this.renderFrom()}
                </div>
            </div>
        );
    }
}

export default AuthPage;
