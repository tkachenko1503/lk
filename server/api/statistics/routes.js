import express from 'express';
import { dashboardStats } from './pdfPages';

const r = express.Router();

r.get('/pdf/:site', dashboardStats);

export default r;
