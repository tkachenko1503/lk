import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import identity from 'ramda/src/identity';
import actions$ from 'actions';
import {App, User} from 'actions/types';
import {login, register, loguot, isAdmin} from 'lib/auth';

export const userLogin = flyd.stream();
export const userRegister = flyd.stream();
export const userLogout = flyd.stream();
export const resetError = flyd.stream();
export const checkIsAdmin = flyd.stream();

const authResponse = flyd.merge(
    userLogin.map(data => ({data, method: login})),
    userRegister.map(data => ({data, method: register})),
);

const resetUser = flyd.merge(
    resetError,
    userLogout.map(loguot)
);

const auth = authResponse
    .map(({data, method}) => {
        return method(data)
            .then(user => ({
                current: user,
                meta: {
                    error: false
                }
            }))
            .catch(error => ({
                meta: {
                    error: true,
                    errorCode: error.code
                }
            }))
            .then(identity)
    });

flyd.on(() => actions$(App['ResetAll!']()), userLogout);

flyd.on(
    pipe(App.User, actions$),
    mergeAll([
        auth.map(User.LogIn),

        resetUser.map(() => User.ResetUser()),

        checkIsAdmin
            .map(isAdmin)
            .map(User.SetAdminRights)
    ])
);
