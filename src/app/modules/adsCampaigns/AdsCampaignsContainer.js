import React from 'react';
import connectToStore from 'lib/connectToStore';
import AdsCampaigns from 'modules/adsCampaigns/AdsCampaigns';
import {isAdvertiserInstance} from 'modules/advertisers';
import {findByProp} from 'lib/index';
import {makeAction} from 'modules/adsCampaigns/actions';

class AdsCampaignsContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    createAdsCampaign() {
        makeAction({
            id: null,
            name: 'create'
        });
    }

    render() {
        const {currentContext, adsCampaignsAction} = this.props;
        const isAdvertiserContext = currentContext && isAdvertiserInstance(currentContext);

        return (
            <AdsCampaigns
                createAdsCampaign={this.createAdsCampaign}
                isAdvertiserContext={isAdvertiserContext}
                adsCampaignsAction={adsCampaignsAction}
            />
        );
    }
}

export default connectToStore(AdsCampaignsContainer, ({app, adsCampaigns}) => ({
    currentContext: app.selectedContext && findByProp(app.selectedContext, app.context, 'id'),
    adsCampaignsAction: adsCampaigns.action
}));