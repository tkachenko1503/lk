import sum from 'ramda/src/sum';
import reduce from 'ramda/src/reduce';
import splitEvery from 'ramda/src/splitEvery';
import map from 'ramda/src/map';
import curryN from 'ramda/src/curryN';
import head from 'ramda/src/head';
import moment from 'moment';

import {Stats} from '../actions/types';

export const defaultStats = () => ({
    overview: {
        shows: 0,
        referrer: 0,
        deepView: 0,
        ctr: 0
    },
    efficiency: {
        impression: [],
        referrer: [],
        ctr: [],
        load: [],
        created: []
    },
    splitedClicks: null,
    meta: {
        error: false,
        pending: false
    }
});

const reduceStatsFn = offset =>
    (memo, { impression, referrer, load, ctr, date }) => {
        const created = offset
            ? moment.utc(date).utcOffset(offset).add(offset, 'hours')
            : moment(date);

        memo.impression.push(impression || 0);
        memo.referrer.push(referrer || 0);
        memo.load.push(load || 0);
        memo.created.push(created.format('YYYY-MM-DDTHH:mm:ss.ZZ'));
        memo.ctr.push(ctr || 0);

        return memo;
    };

const getSplitedClicks = (referrer, created) => {
    let parts;

    switch (true) {
        case (referrer.length < 40):
            return {
                referrer,
                created
            };
            break;
        default:
            parts = referrer.length / 4;
            break;
    }

    return {
        referrer: map(sum, splitEvery(parts, referrer)),
        created: map(head, splitEvery(parts, created))
    };
};

const emptyStats = () => ({
    impression: [],
    referrer: [],
    load: [],
    ctr: [],
    created: []
});

var options = { year: 'numeric', month: 'long', day: 'numeric' };
var dateTimeFormat = new Intl.DateTimeFormat('ru-RU', options);

// offset change the timezone
export const formatStatsForStore = (data, state, offset) => {
    if (data.stats.length) {
        const efficiency = reduce(reduceStatsFn(offset), emptyStats(), data.stats);

        const sumImpression = sum(efficiency.impression);
        const overview = {
            ...data.overview,
            createdAt: dateTimeFormat.format(new Date(data.overview.created_at)),
            ctr: (sumImpression
                ? Number(((sum(efficiency.referrer) / sumImpression) * 100).toFixed(1))
                : 0)
        };

        return {
            ...state,
            meta: {
                ...state.meta,
                pending: false
            },
            overview: {
                ...overview,
                deepView: 0
            },
            efficiency: {
                ...efficiency
            },
            splitedClicks: getSplitedClicks(efficiency.referrer, efficiency.created)
        };
    }
    return defaultStats();
};

const reducefn = Stats.caseOn({
    RequestStats: (params, state) => ({
        ...state,
        meta: {
            ...state.meta,
            pending: true
        }
    }),

    SetStats: formatStatsForStore,

    _: (...args) => {
        return args[args.length - 1];
    }
});

export default curryN(2, reducefn);
