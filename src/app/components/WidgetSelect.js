import React from 'react';
import DropdownButton from 'react-bootstrap/lib/DropdownButton';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import {findByProp} from 'lib/index';

import widgetSelect from 'styles/widgetSelect.css';

const WidgetSelect = (props) => {
    const {widgetsList, choosenWidget, defaultTitle} = props;
    const {widgetSelectHandler, widgetName} = props;
    
    const widget = findByProp(choosenWidget, widgetsList, 'id');
    let title;

    if (widget) {
        title = widget.get('name');
    } else if (widgetName) {
        title = widgetName;
    } else {
        title = defaultTitle;
    }

    const tooltip = (<Tooltip id={title}>{title}</Tooltip>);

    return (
        <OverlayTrigger
            placement="top"
            overlay={tooltip}
        >
            <DropdownButton
                className={widgetSelect.button}
                bsStyle="success"
                title={title}
                id="widgetSelect"
                bsSize="small"
                disabled={widgetsList.length ? false : true}
                onSelect={widgetSelectHandler}
            >
                <MenuItem
                    eventKey={null}
                    key='all'
                >
                    {defaultTitle}
                </MenuItem>

                {widgetsList.map(w => (
                    <MenuItem
                        eventKey={w.id}
                        key={w.id}
                    >
                        {w.get('name')}
                    </MenuItem>
                ))}
            </DropdownButton>
        </OverlayTrigger>
    );
};

export default WidgetSelect;
