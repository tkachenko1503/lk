import React from 'react';
import Table from 'react-bootstrap/lib/Table';
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';

import styles from 'styles/widgetList.css';
import table from 'styles/widgetTable.css';
import removeModal from 'styles/removeModal.css';

const WidgetsList = (props) => {
    const {widgets, removeDialog, renderWidget, removeWidget, closeRemoveWidget} = props;
    
    return (
        <div>
            <Table
                striped condensed responsive
                className={styles.widgetList}
            >
                <thead>
                    <tr>
                        <th key="name" className={table.nameCol}>Имя</th>
                        <th key="id" className={table.idCol}>ID</th>
                        <th key="actions" className={table.actionsCol}>Действия</th>
                        <th key="remove" className={table.removeCol}>Удалить</th>
                    </tr>
                </thead>

                <tbody>
                    {widgets.map(renderWidget)}
                </tbody>
            </Table>

            <Modal
                show={removeDialog.open}
                onHide={closeRemoveWidget}
                animation={false}
            >
                <Modal.Body>
                    <h4 className={removeModal.message}>
                        Вы уверенны что хотите удалить этот виджет?
                    </h4>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={removeWidget}>Да</Button>
                    <Button onClick={closeRemoveWidget}>Нет</Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
};

export default WidgetsList;
