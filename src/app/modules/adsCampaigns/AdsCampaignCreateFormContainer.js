import React from 'react';
import connectToStore from 'lib/connectToStore';
import CreateWidget from 'components/CreateWidget';
import {findByProp} from 'lib/index';
import {saveAdsCampaign, closeAdsCampaignsAction} from 'modules/adsCampaigns/actions';

import widget from 'styles/widget.css';


class CreateAdsCampaign extends CreateWidget {
    constructor(props) {
        super(props);

        this.closeAction = closeAdsCampaignsAction;
    }
}

class AdsCampaignCreateFormContainer extends React.Component {
    constructor(props) {
        super(props);

        this.saveAdsCampaign = this.saveAdsCampaign.bind(this);
    }

    saveAdsCampaign(e) {
        e.preventDefault();

        const {widgetName} = e.target.elements;
        const {currentContext} = this.props;
        const name = widgetName.value ? widgetName.value.trim() : '';

        if (name) {
            saveAdsCampaign({
                name: name,
                advertiserId: currentContext.id
            });
        }
    }

    render() {
        return (
            <div className={widget.widgetCreateForm}>
                <CreateAdsCampaign
                    submitAction={this.saveAdsCampaign}
                    inputSize={5}
                    buttonSize={2}
                />
            </div>
        );
    }
}

export default connectToStore(AdsCampaignCreateFormContainer, ({advertisers, app, user}) => ({
    currentContext: app.selectedContext && findByProp(app.selectedContext, app.context, 'id')
}));