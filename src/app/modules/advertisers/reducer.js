import curryN from 'ramda/src/curryN';
import {Advertisers} from 'actions/types';

export function defaultAdvertisers() {
    return {
        file: null,
        campaign: null,
        contentTable: {
            sort: {
                type: 'desc',
                id: null
            },
            page: 1,
            size: 10
        },
        contentList: []
    };
}

const reducefn = Advertisers.caseOn({
    SetStatsFile: (file, state) => ({
        ...state,
        file
    }),
    ClearStatsFile: state => ({
        ...state,
        file: null
    }),
    SetAdsCampaign: (AdsCampaignId, state) => ({
        ...state,
        campaign: AdsCampaignId
    }),
    SetContentTableControl: ({control, value}, state) => {
        const newState = {
            ...state,
            contentTable: {
                ...state.contentTable,
                [control]: value
            }
        };

        // если меняем размер таблицы сбрасываем пейджинг
        if (control === 'size') {
            newState.contentTable.page = 1;
        }

        return newState;
    },
    SetAdvertisersContent: (content, state) => ({
        ...state,
        contentList: content
    })
});

export default curryN(2, reducefn);
