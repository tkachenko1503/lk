import moment from 'moment';

const hour = moment.duration(1, 'hours');
const fourHours = moment.duration(4, 'hours');
const oneDay = moment.duration(1, 'days');
const twoDays = moment.duration(2, 'days');
const fourDays = moment.duration(4, 'days');

export function getInterval({from, to}) {
    const diff = to.valueOf() - from.valueOf();

    switch (true) {
        case (diff <= twoDays):
            return hour;
            break;
        case (diff > twoDays && diff < fourDays):
            return fourHours;
            break;
        default:
            return oneDay;
            break;
    }
}

export function getIntervalString({from, to}) {
    const diff = to.valueOf() - from.valueOf();

    switch (true) {
        case (diff <= twoDays):
            return 'hour';
        default:
            return 'day';
    }
}

export function getCSVGroup({from, to}) {
    const diff = to.valueOf() - from.valueOf();

    switch (true) {
        case (diff <= oneDay):
            return hour;
            break;
        default:
            return oneDay;
            break;
    }
}

export function logger(data) {
    console.log('LOGGER -> ', data);
    return data
}
