/* eslint max-len: "off" */

import moment from 'moment';
import knex from '../lib/knex';
import siteSettings from '../../configs/siteSettings';
import { pipe, prop, map } from 'ramda';

function formatResults(hoursOffset) {
    return function ({ created, ctr, impression, load, referrer }) {
        return {
            ctr,
            date: moment.utc(created)
                .utcOffset(hoursOffset)
                .subtract(hoursOffset, 'hours')
                .valueOf(),
            impression: Number(impression),
            load: Number(load),
            referrer: Number(referrer)
        };
    };
}

const processStatsBySite = (hoursOffset) => {
    const formatResultsWithOffset = formatResults(hoursOffset);

    return pipe(prop('rows'), map(formatResultsWithOffset));
};

export function getStatsBySite({ interval, hoursOffset, site, widgetId, from, to }) {
    const settings = siteSettings[site] || siteSettings.common;
    const processStatsBySiteWithOffset = processStatsBySite(hoursOffset);

    const q = knex.raw(`
        SELECT 
            sum( coalesce((impression)::text::int, 0) ) AS impression,
            sum( coalesce((referrer)::text::int, ${widgetId ? '(click)::text::int,' : ''} 0) ) AS referrer,
            sum( coalesce((load)::text::int, 0) ) AS load,
            round( 100 * (sum( coalesce((referrer)::text::decimal, ${widgetId ? '(click)::text::int,' : ''} 0) ) / sum( coalesce((impression)::text::decimal, 1) )), 1 ) AS ctr,
            date_trunc('${interval}', (created + '${hoursOffset} hour')::timestamp with time zone) AS created
        FROM (
            SELECT 
                data->'NTVK'->'impression' AS impression, 
                data->'load' AS load, 
                ${settings.clicksField} AS referrer,
                data->'NTVK'->'click' AS click,
                since AS created
            FROM stats 
            WHERE site = '${site}' 
                AND widget_id ${widgetId ? `= '${widgetId}'` : 'ISNULL'} 
                AND since 
                    BETWEEN ('${from}'::timestamp - interval '${hoursOffset} hours')
                    AND ('${to}'::timestamp - interval '${hoursOffset} hours')
            ORDER BY created
            ) AS sq
        GROUP BY date_trunc('${interval}', (created + '${hoursOffset} hour')::timestamp with time zone)
        ORDER BY created
    `);

    return q.then(processStatsBySiteWithOffset)
            .catch(() => []);
}

export function getStatsByAdvertiser({ advertiser, from, to, campaign, hoursOffset }) {
    const processStatsBySiteWithOffset = processStatsBySite(hoursOffset);

    return knex.raw(`
        SELECT 
            data->'NTVK'->'shows' AS impression, 
            data->'NTVK'->'clicks' AS referrer,
            data->'NTVK'->'ctr' AS ctr,
            since::timestamp with time zone AS created
        FROM imported_stats 
        WHERE advertiser = '${advertiser}' 
            ${campaign ? `AND campaign = '${campaign}'` : ''}
            AND since 
                BETWEEN ('${from}'::timestamp - interval '${hoursOffset} hours')
                AND ('${to}'::timestamp - interval '${hoursOffset} hours')
        ORDER BY created
    `)
        .then(processStatsBySiteWithOffset)
        .catch(() => []);
}

// ------------------------------------------------
// Overview stats

function getDateWhereFunc({ from, to }) {
    return function () {
        this.whereBetween(knex.raw('since::timestamp with time zone'), [
            // даты должны быть в тайм зоне клиента
            from.format('YYYY-MM-DD HH:mm:ssZ'),
            to.format('YYYY-MM-DD HH:mm:ssZ')
        ]);
    };
}

const getFieldsBySiteSettings = (fields, site) => {
    const settings = siteSettings[site] || siteSettings.common;

    return [
        ...fields,
        knex.raw(`${settings.clicksField} as referrer`),
        knex.raw('data->\'NTVK\'->\'click\' AS click')
    ];
};

const overviewFields = [
    knex.raw('sum( coalesce((impression)::text::int, 0) ) AS shows'),
    knex.raw('sum( coalesce((load)::text::int, 0) ) AS load'),
    knex.raw('max(created_at) AS created_at')
];
const overviewSubFields = [
    knex.raw('extract(epoch from created_at::timestamp with time zone) * 1000 AS created_at'),
    knex.raw('data->\'NTVK\'->\'impression\' as impression'),
    knex.raw('data->\'load\' as load')
];

export function getOverviewData(params) {
    const fields = getFieldsBySiteSettings(overviewSubFields, params.site);
    const overviewFieldsBySettings = [
        ...overviewFields,
        knex.raw(`sum( coalesce((referrer)::text::int, ${params.widget ? '(click)::text::int,' : ''} 0) ) AS referrer`)
    ];

    return knex
        .select.apply(knex, overviewFieldsBySettings)
        .from(function () {
            this
                .select.apply(this, fields)
                .from('stats')
                .where({
                    site: params.site,
                    widget_id: params.widget || null
                })
                .andWhere(getDateWhereFunc(params))
                .as('statsSubQuery');
        });
}

const advertiserOverviewFields = [
    knex.raw('sum(impression::text::int) AS shows'),
    knex.raw('sum(referrer::text::int) AS referrer'),
    knex.raw('max(created_at) AS created_at')
];
const advertiserOverviewSubFields = [
    knex.raw('extract(epoch from created_at::timestamp with time zone) * 1000 AS created_at'),
    knex.raw('data->\'NTVK\'->\'shows\' as impression'),
    knex.raw('data->\'NTVK\'->\'clicks\' as referrer')
];

export function getAdvertiserOverviewData({ advertiser, campaign, from, to }) {
    const where = { advertiser };

    if (campaign) {
        where.campaign = campaign;
    }

    const q = knex
        .select.apply(knex, advertiserOverviewFields)
        .from(function () {
            this
                .select.apply(this, advertiserOverviewSubFields)
                .from('imported_stats')
                .where(where)
                .andWhere(getDateWhereFunc({ from, to }))
                .as('statsSubQuery');
        });

    return q;
}


// ------------------------------------------------
// CSV stats

function formatCSVResults(hoursOffset) {
    return function ({ created, ctr, impression, load, referrer, website, widget_id, advertiser, campaign }) {
        return {
            date: moment.utc(created)
                .utcOffset(hoursOffset)
                .subtract(hoursOffset, 'hours')
                .valueOf(),
            ctr: Number(ctr),
            impression: Number(impression),
            load: Number(load),
            clicks: Number(referrer),
            website,
            advertiser,
            widget_id,
            campaign
        };
    };
}

const processCSVStatsBySite = (hoursOffset) => {
    const formatCSVResultsWithOffset = formatCSVResults(hoursOffset);

    return pipe(prop('rows'), map(formatCSVResultsWithOffset));
};
export function getStatsForCSVAllWidgets({ interval, hoursOffset, site, widgetId, from, to }) {
    const settings = siteSettings[site] || siteSettings.common;
    const processCSVStatsBySiteWithOffset = processCSVStatsBySite(hoursOffset);

    const q = knex.raw(`
        SELECT 
            sum( coalesce((impression)::text::int, 0) ) AS impression,
            sum( coalesce((referrer)::text::int, ${widgetId ? '(click)::text::int,' : ''} 0) ) AS referrer,
            sum( coalesce((load)::text::int, 0) ) AS load,
            round( 100 * (sum( coalesce((referrer)::text::decimal, ${widgetId ? '(click)::text::int,' : ''} 0) ) / sum( coalesce((impression)::text::decimal, 1) )), 1 ) AS ctr,
            date_trunc('${interval}', (created + '${hoursOffset} hour')::timestamp with time zone) AS created,
            website
            ${widgetId ? '' : ', widget_id'}
        FROM (
            SELECT 
                data->'NTVK'->'impression' AS impression, 
                data->'load' AS load, 
                ${settings.clicksField} AS referrer,
                data->'NTVK'->'click' AS click,
                since AS created,
                site AS website,
                widget_id AS widget_id
            FROM stats 
            WHERE site = '${site}' 
                ${widgetId ? `AND widget_id = '${widgetId}'` : 'AND widget_id IS DISTINCT FROM \'error\''} 
                AND since 
                    BETWEEN ('${from}'::timestamp - interval '${hoursOffset} hours')
                    AND ('${to}'::timestamp - interval '${hoursOffset} hours')
            ORDER BY created
            ) AS sq
        GROUP BY date_trunc('${interval}', (created + '${hoursOffset} hour')::timestamp with time zone), website ${widgetId ? '' : ', widget_id'}
        ORDER BY ${widgetId ? '' : 'widget_id,'} created
    `);

    return q.then(processCSVStatsBySiteWithOffset)
            .catch(() => []);
}

export function getAdvertiserStatsForCSVAllWidgets({ hoursOffset, advertiser, campaign, from, to }) {
    const processCSVStatsBySiteWithOffset = processCSVStatsBySite(hoursOffset);

    return knex.raw(`
        SELECT 
            data->'NTVK'->'shows' AS impression, 
            data->'NTVK'->'clicks' AS referrer,
            data->'NTVK'->'ctr' AS ctr,
            since AS created,
            advertiser AS advertiser,
            campaign AS campaign
        FROM imported_stats 
        WHERE advertiser = '${advertiser}' 
            ${campaign ? `AND campaign = '${campaign}'` : ''} 
            AND since BETWEEN '${from}'::timestamp AND '${to}'::timestamp
        ORDER BY ${campaign ? '' : 'campaign,'} created
    `)
        .then(processCSVStatsBySiteWithOffset)
        .catch(() => []);
}
