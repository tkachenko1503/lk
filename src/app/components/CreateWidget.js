import React from 'react';
import EscKeyClosable from 'components/EscKeyClosable';
import {closeWidgetAction} from 'actions/widgets';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Input from 'react-bootstrap/lib/Input';
import Button from 'react-bootstrap/lib/Button';

import styles from 'styles/widget.css';

class CreateWidget extends EscKeyClosable {
    constructor(props) {
        super(props);

        this.closeAction = closeWidgetAction;
    }

    render() {
        const {submitAction, name, inputSize, buttonSize} = this.props;

        return (
            <form onSubmit={submitAction}>
                <Grid fluid={true}>
                    <Row>
                        <Col
                            className={styles.widgetNameInput}
                            xs={inputSize}
                        >
                            <Input
                                className="qwe"
                                type="text"
                                name="widgetName"
                                placeholder="Имя"
                                defaultValue={name}
                                autofocus
                            />
                        </Col>
                        <Col xs={buttonSize}>
                            <Button type="submit">
                                Сохранить
                            </Button>
                        </Col>
                    </Row>
                </Grid>
            </form>
        );
    }
}

export default CreateWidget;
