import curryN from 'ramda/src/curryN';
import {Sites} from 'actions/types';

export function defaultSites() {
    return {
        currentSite: null,
        availableSites: [],
        meta: {
            error: false,
            pending: false
        }
    };
}

const reducefn = Sites.caseOn({
    RequestSites: (state) => ({
        ...state,
        meta: {
            ...state.meta,
            pending: true
        }
    }),

    SetSites: (sites, state) => ({
        ...state,
        availableSites: sites,
        meta: {
            ...state.meta,
            pending: false
        }
    }),

    ChangeSite: (site, state) => ({
        ...state,
        currentSite: site
    })
});

export default curryN(2, reducefn);
