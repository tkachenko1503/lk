import React from 'react';
import {Link} from 'react-router';
import Collapse from 'react-bootstrap/lib/Collapse';
import {If, Then, Else} from 'react-if';
import classNames from 'classnames';
import curry from 'ramda/src/curry';
import {DASHBOARD, WIDGETS,
    ADS, ADS_SUBLINKS,
    TRAFFIC, TRAFFIC_SUBLINKS,
    AB_TESTS, AB_TESTS_SUBLINKS,
    ANALYSIS, ANALYSIS_SUBLINKS,
    ADS_CAMPAIGNS, ADVERTISER_IMPORT
} from 'lib/constants';

import styles from 'styles/sidebar.css';

/**
 * Сайдбар с навигационным меню
 * @param props
 * @constructor
 */
const Sidebar = (props) => {
    const {router, navItem, toggleNavItem, contextType, isAdmin} = props;

    return (
        <div className={classNames(
            styles.sidebar,
            'sidebar-left'
        )}>
            <div className="sidebar-left-info">
                <ul className={classNames(
                    styles.sidebarList,
                    "nav nav-pills nav-stacked side-navigation"
                )}>
                    <li>
                        <h3 className="navigation-title">
                            Navigation
                        </h3>
                    </li>

                    <SimpleNavItem
                        navId={DASHBOARD}
                        title="Сводка"
                        icon="fa-home"
                        router={router}
                    />
                </ul>

                <If condition={contextType === "Advertiser"}>
                    <Then>
                        <ul className="nav nav-pills nav-stacked side-navigation">
                            <SimpleNavItem
                                navId={ADS_CAMPAIGNS}
                                title="Рекламные кампании"
                                icon="fa-bullhorn"
                                router={router}
                            />
                            <If condition={isAdmin}>
                                <Then>
                                    <SimpleNavItem
                                        navId={ADVERTISER_IMPORT}
                                        title="Статистика для рекламодателей"
                                        icon="fa-upload"
                                        router={router}
                                    />
                                </Then>
                            </If>
                        </ul>
                    </Then>
                    <Else>
                        <ul className="nav nav-pills nav-stacked side-navigation">
                            <ColapsedNavItem
                                navId={ANALYSIS}
                                title="Статистика"
                                icon="fa-bar-chart-o"
                                collapsedItem={navItem}
                                toggleNavItem={toggleNavItem}
                                links={ANALYSIS_SUBLINKS}
                                renderSubNav={renderSimpleNav(router)}
                            />
                            <SimpleNavItem
                                navId={WIDGETS}
                                title="Виджет"
                                icon="fa-cogs"
                                router={router}
                            />
                            <If condition={isAdmin}>
                                <Then>
                                    <ColapsedNavItem
                                        navId={ADS}
                                        title="Реклама"
                                        icon="fa-hand-o-up"
                                        collapsedItem={navItem}
                                        toggleNavItem={toggleNavItem}
                                        links={ADS_SUBLINKS}
                                        renderSubNav={renderSimpleNav(router)}
                                    />
                                </Then>
                            </If>
                            <ColapsedNavItem
                                navId={TRAFFIC}
                                title="Биржа трафика"
                                icon="fa-exchange"
                                collapsedItem={navItem}
                                toggleNavItem={toggleNavItem}
                                links={TRAFFIC_SUBLINKS}
                                renderSubNav={renderSimpleNav(router)}
                            />
                            <ColapsedNavItem
                                navId={AB_TESTS}
                                title="A/B тесты"
                                icon="fa-check-square-o"
                                collapsedItem={navItem}
                                toggleNavItem={toggleNavItem}
                                links={AB_TESTS_SUBLINKS}
                                renderSubNav={renderSimpleNav(router)}
                            />
                        </ul>
                    </Else>
                </If>
            </div>
        </div>
    );
};

export default Sidebar;

/**
 * Одноуровневая ссылка в сайдбаре
 * @param router - объект представляющий роутер
 * @param navId - идентификатор ссылки
 * @param title - текст
 * @param icon - иконка
 * @constructor
 */
const SimpleNavItem = ({router, navId, title, icon}) => {
    const isActive = router.isActive(navId);

    return (
        <li
            key={navId}
            className={classNames({
                active: isActive
            })}
        >
            <Link to={navId}>
                {icon
                    ? <i className={classNames('fa', icon)}></i>
                    : null}

                <span>
                    {title}
                </span>
            </Link>
        </li>
    );
};

/**
 * Кнопка открывающая навигационные ссылки второго уровня
 * @param navId - идентификатор кнопки
 * @param collapsedItem - объект из стейта
 * @param title - текст
 * @param icon - иконка
 * @param links - коллекция вложенных ссылок
 * @param toggleNavItem - коллбэк для скрытия/открытия
 * @param renderSubNav - функция для отрисовки ссылок второго уровня
 * @constructor
 */
const ColapsedNavItem = ({navId, collapsedItem, title, icon, links, toggleNavItem, renderSubNav}) => {
    const isActive = collapsedItem.id === navId && collapsedItem.opened;

    return (
        <li
            key={navId}
            className={classNames(
                'menu-list',
                {'nav-active': isActive}
            )}
        >
            <a
                onClick={toggleNavItem}
                data-nav-id={navId}
            >
                <i className={classNames("fa", icon)}/>
                <span>
                    {title}
                </span>
            </a>

            <Collapse in={isActive}>
                <ul className="child-list">
                    {links.map(renderSubNav)}
                </ul>
            </Collapse>
        </li>
    );
};

/**
 * Функция для отрисовки ссылок второго уровня
 * с помощью каррирования пробрасываем роутер
 */
const renderSimpleNav = curry((router, nav, i) => (
    <SimpleNavItem key={i} router={router} {...nav} />
));
