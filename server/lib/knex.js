import knex from 'knex';
import constants from '../../configs/constants.json';

export default knex({
    client: 'pg',
    connection: constants['pgDB'],
    searchPath: 'knex,public'
});
