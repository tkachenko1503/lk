import express from 'express';
import { ParseServer } from 'parse-server';
import ParseDashboard from 'parse-dashboard';
import path from 'path';

import api from './api';
import constants from '../configs/constants.json';

const PORT = process.env.PORT || 3000;
const CLOUD_PATH = path.resolve('./server/cloud/index.js');
const MONGODB_URI = process.env.MONGODB_URI || constants.devDBUrl;
const SERVER_URL = process.env.SERVER_URL
                        ? `${process.env.SERVER_URL}parse`
                        : `http://localhost:${PORT}/parse`;

const app = express();


// initialise parse server
const parse = new ParseServer({
    databaseURI: MONGODB_URI,
    serverURL: SERVER_URL,
    appId: constants.appId,
    masterKey: constants.masterKey,
    javascriptKey: constants.javascriptKey,
    restAPIKey: constants.restAPIKey,
    cloud: CLOUD_PATH
});

// initialise parse dashboard
const dashboard = new ParseDashboard({
    apps: [
        {
            serverURL: SERVER_URL,
            appId: constants.appId,
            masterKey: constants.masterKey,
            javascriptKey: constants.javascriptKey,
            appName: 'Nativka',
            production: true
        }
    ],
    users: [
        constants.godUser
    ]
}, true);

// set middleware
app.use('/parse', parse);
app.use('/admin/dashboard', dashboard);
app.use('/api', api);

export default app;
