import React from 'react';
import Pagination from 'react-bootstrap/lib/Pagination';
import Button from 'react-bootstrap/lib/Button';
import Input from 'react-bootstrap/lib/Input';
import StatsTable from 'components/StatsTable';
import {If, Then, Else} from 'react-if';

import adsStatsTable from 'styles/adsStatsTable.css';

const HEADERS = [
    {id: 'material_id', title: 'Рекламодатель', formatter: (v, row) => (
        <If condition={Boolean(row['material_title']) && Boolean(row['material_url'])}>
            <Then>
                <a href={row['material_url']}>
                    {row['material_title']}
                </a>
            </Then>
            <Else>
                <span>{v}</span>
            </Else>
        </If>
    )},
    {id: 'profit', title: 'Доход'},
    {id: 'clickCost', title: 'Цена клика'},
    {id: 'ctr', title: 'CTR', formatter: v => `${v}%`}
];

class AdvertisersContentTable extends React.Component {
    render() {
        return (
            <div>
                <ExportAdvertisersPanel
                    exportTableInCsv={this.props.exportTableInCsv}
                />

                <StatsTable
                    rows={this.props.rows}
                    rowsSummary={false}
                    tableSort={this.props.tableSort}
                    datesRange={this.props.datesRange}
                    headers={HEADERS}
                    fieldsOnly={true}
                    setTableSort={this.props.setTableSort}
                    withHeadClass={false}
                />

                <AdvertisersTablePagerAndSize
                    pagerSize={this.props.pagerSize}
                    activePage={this.props.activePage}
                    currentSize={this.props.currentSize}
                    setTableSize={this.props.setTableSize}
                    setTablePage={this.props.setTablePage}
                />
            </div>
        );
    }
}

export default AdvertisersContentTable;

const ExportAdvertisersPanel = ({exportTableInCsv}) => {
    return (
        <div className={adsStatsTable.csvExportPanel}>
            <Button onClick={exportTableInCsv}>
                в CSV
            </Button>
        </div>
    );
};

const OPTIONS = [10, 20, 50];

const AdvertisersTablePagerAndSize = (props) => {
    const needArrows = props.pagerSize > 4;

    return (
        <div className={adsStatsTable.tableSizePanel}>
            <div className={adsStatsTable.tableSizeLabel}>
                Размер таблицы
            </div>

            <div className={adsStatsTable.tableSize}>
                <Input
                    className={adsStatsTable.tableSizeSelect}
                    type="select"
                    onChange={props.setTableSize}
                    defaultValue={props.currentSize}
                >
                    {OPTIONS.map(SizeOption)}
                </Input>
            </div>

            {props.pagerSize
                ? (
                    <div className={adsStatsTable.tablePager}>
                        <Pagination
                            first={needArrows}
                            last={needArrows}
                            maxButtons={4}
                            items={props.pagerSize}
                            activePage={props.activePage}
                            onSelect={props.setTablePage}
                        />
                    </div>
                )
                : null
            }
        </div>
    );
};

const SizeOption = (option, i) => (
    <option
        key={i}
        value={option}
    >
        {option}
    </option>
);
