import React from 'react';
import connectToStore from 'lib/connectToStore';
import AdsStatsChartPanel from 'components/AdsStatsChartPanel';
import {setParam} from 'actions/adsChartControls';
import {ADS_CHART_BUTTONS} from 'lib/constants';
import {findByProp} from 'lib/index';
import partialRight from 'ramda/src/partialRight';
import pipe from 'ramda/src/pipe';
import prop from 'ramda/src/prop';
import pluck from 'ramda/src/pluck';

/**
 * Большой граффик на странице статистики по рекламе
 * @constructor
 */
class AdsStatsChartContainer extends React.Component {
    render() {
        const {params, created, dateRange} = this.props;
        const {firstColunmData, secondColunmData} = this.props;
        const {firstColunmTitle, secondColunmTitle} = this.props;

        return (
            <div>
                <AdsStatsChartPanel
                    params={params}
                    switchParam={setParam}
                    firstColunmData={firstColunmData}
                    secondColunmData={secondColunmData}
                    firstColunmTitle={firstColunmTitle}
                    secondColunmTitle={secondColunmTitle}
                    created={created}
                    dateRange={dateRange}
                />
            </div>
        );
    }
}

const getChartControl = partialRight(findByProp, [ADS_CHART_BUTTONS, 'id']);
const getTitle = pipe(getChartControl, prop('title'));

export default connectToStore(AdsStatsChartContainer, ({adsChartControls, adsStats, filters}) => ({
    params: adsChartControls,
    // data
    firstColunmData: pluck(adsChartControls.first, adsStats.statsList),
    secondColunmData: pluck(adsChartControls.second, adsStats.statsList),
    // titles
    firstColunmTitle: getTitle(adsChartControls.first),
    secondColunmTitle: getTitle(adsChartControls.second),
    // dates
    created: pluck('created', adsStats.statsList),
    dateRange: filters.date
}));
