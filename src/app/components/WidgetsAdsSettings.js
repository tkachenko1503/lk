import React from 'react';
import classnames from 'classnames';
import Table from 'react-bootstrap/lib/Table';
import Button from 'react-bootstrap/lib/Button';
import Slider from 'components/Slider';
import curry from 'ramda/src/curry';
import {WIDGET_MAX_PLACES_NUMBER} from 'lib/constants';

import adsTable from 'styles/adsTable.css';

const WidgetsAdsSettings = (props) => {
    const {widgetsList, setAdsPlaces, saveAllChanges, canSaveAdsSettings} = props;

    return (
        <div>
            <Table
                striped condensed responsive
                className={adsTable.table}
            >
                <thead>
                    <tr>
                        <th key="num"></th>
                        <th key="name">Виджет</th>
                        <th key="id">Виджет-ID</th>
                        <th key="ads-places">
                            Количество рекламных мест в виджете
                            <i
                                className={classnames(
                                    "fa fa-question-circle",
                                    adsTable.question
                                )}
                            />
                        </th>
                    </tr>
                </thead>

                <tbody>
                    {widgetsList.map(renderWidgetAds(setAdsPlaces))}
                </tbody>
            </Table>

            <Button
                className={adsTable.saveButton}
                onClick={saveAllChanges}
                disabled={!canSaveAdsSettings}
            >
                Сохранить настройки показа рекламы
            </Button>
        </div>
    );
};

export default WidgetsAdsSettings;

const renderWidgetAds = curry(function (setAdsPlaces, widget, i) {
    const adsPlaces = widget.get('adsPlacesNumber') || 0;
    const maxPlaces = widget.get('maxPlacesNumber') || WIDGET_MAX_PLACES_NUMBER;

    return (
        <tr key={i}>
            <td className={adsTable.numCol}>{i + 1}</td>
            <td className={adsTable.nameCol}>
                <span className={adsTable.nameText}>
                    {widget.get('name')}
                </span>
            </td>
            <td className={adsTable.idCol}>ID_{widget.id}</td>
            <td className={adsTable.placescol}>
                <Slider
                    step={maxPlaces}
                    value={adsPlaces}
                    onChange={setAdsPlaces}
                    eventKey={widget.id}
                />
            </td>
        </tr>
    );
});
