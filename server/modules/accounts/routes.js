import { Router } from 'express';
import { getAccountCsvReport } from './controller';

const routes = Router();

routes.get('/csv/:account', getAccountCsvReport);

export default routes;
