import json2csv from '../lib/json2csv';
import moment from 'moment';
import {map, sum, prop, pick, zipObj, isEmpty} from 'ramda';
import mongoConnectAsync from '../lib/mongoConnectAsync';

export const getAdsPaymentsBySite  = (siteId, from, to) => {

    const summaries = getSummaries(siteId);
    const payments = getPayments(siteId, from ,to);

    return Promise.all([summaries, payments])
        .then((result) => {
            const [summaries, payments] = result;

            return {
                summaries,
                payments
            }
        });
};

export const getAdsPaymentsBySiteCSV = (siteId, from, to) => {

    const payments = getPayments(siteId, from ,to);

    return payments
        .then((result) => {

            const {total, daily} = result;

            const fields = [{
                label: 'Дата',
                value: 'date'
            }, {
                label: 'Начисление',
                value: 'creditedWith'
            }, {
                label: 'Вывод',
                value: 'cashedOut'
            }];

            const data = [{
                date: 'Всего за период',
                creditedWith: total.creditedWith,
                cashedOut: total.cashedOut
            }].concat(daily);

            return json2csv({
                fields,
                data
            });
        });
};


/**
 * Возвращает суммарную статистику по платежам
 *
 * @param {String} siteId
 * @returns {Promise}
 */
function getSummaries(siteId) {

    return mongoConnectAsync.then((db) => {

        const condition = {
            siteId
        };

        const fields = ['availableForCashing', 'incomAll', 'incomLastMonth', 'incomLastThreeMonths'];

        return db
            .collection('adsPaymentsSummaries')
            .find(condition)
            .toArrayAsync()
            .then((result) => {
                if (!isEmpty(result)) {
                    return pick(fields, result[0]);
                } else {
                    const zeros = new Array(4).fill(0);

                    return zipObj(fields, zeros);
                }
            });
    });
}

/**
 * Возвращает платежи за указанный период
 *
 * @param {String} siteId
 * @param {Moment} from - дата в таймзоне клиента
 * @param {Moment} to - дата в таймзоне клиента
 * @returns {Promise}
 */
function getPayments(siteId, from, to) {

    const SORT_DESC = -1;
    const utcOffset = from.utcOffset();

    return mongoConnectAsync.then((db) => {

        const timeZonedDateExpression = {$add: ['$date', utcOffset * 60000]};
        const creditedWithExpression = {$gte: ['$value', 0]};

        return db
            .collection('adsPayments')
            .aggregateAsync(
                [
                    {
                        $match: {
                            siteId,
                            date: {
                                $gte: from.toDate(),
                                $lte: to.toDate()
                            }
                        }
                    },
                    {
                        $group: {
                            _id: {
                                year: {$year: timeZonedDateExpression},
                                month: {$month: timeZonedDateExpression},
                                day: {$dayOfMonth: timeZonedDateExpression}
                            },
                            date: {
                                $first: '$date'
                            },
                            creditedWith: {
                                $sum: {
                                    $cond: { if: creditedWithExpression, then: '$value', else: 0}
                                }
                            },
                            cashedOut: {
                                $sum: {
                                    $cond: { if: creditedWithExpression, then: 0, else: '$value'}
                                }
                            }
                        }
                    },
                    {
                        $sort: {
                            _id: SORT_DESC
                        }
                    }
                ]
            )
            .then((result) => {

                const total = {
                    creditedWith: sum(map(prop('creditedWith'), result)),
                    cashedOut: sum(map(prop('cashedOut'), result))
                };

                let daily = map(pick(['date', 'creditedWith', 'cashedOut']), result);

                // Форматириуем дату в таймзоне клиента
                daily = daily.map((payment) => {
                    payment.date = moment(payment.date).utcOffset(utcOffset).format();

                    return payment;
                });

                return {
                    total,
                    daily
                };
            });
    });
}


