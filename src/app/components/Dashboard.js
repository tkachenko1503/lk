import React from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import { If, Then } from 'react-if';
import Layout from '../components/Layout';
import DashboardFilters from '../containers/DashboardFilters';
import LoadInfo from '../components/LoadInfo';
import MonthlyViewChart from '../components/MonthlyViewChart';
import DashboardOverview from '../containers/DashboardOverview';
import EfficiencyPanel from '../components/EfficiencyPanel';
import AdditionalRedirects from '../components/AdditionalRedirects';

import styles from '../styles/dashboard.css';

/**
 * Компонент для отрисовки страницы дашборда
 * @constructor
 */
const Dashboard = (props) => {
    const { overview, efficiency, splitedClicks } = props.stats;
    const { contextType, isAdmin } = props;
    const firstTitle = contextType === 'Site' ? 'Показы виджета' : 'Показы объявлений';

    return (
        <Layout>
            <div className="dashboard-container">
                <DashboardFilters
                    actionForApplyFilters={props.actionForApplyFilters}
                />
                <div className="wrapper">
                    <DashboardOverview />

                    <Grid fluid={true} className={styles.dashboardGrid}>
                        <Row>
                            <Col md={8}>
                                <EfficiencyPanel
                                    columns={efficiency}
                                    dateRange={props.filters.date}
                                    firstTitle={firstTitle}
                                    secondTitle="Клики"
                                />
                            </Col>
                            <Col md={4}>
                                <LoadInfo
                                    load={overview.load}
                                    createdAt={overview.createdAt}
                                    contextType={overview.contextType}
                                    isAdmin={isAdmin}
                                    loadCSV={props.loadCSV}
                                    loadStatisticsPdf={props.loadStatisticsPdf}
                                />
                            </Col>
                        </Row>
                        <If condition={contextType === "Site"}>
                            <Then>
                                <Row>
                                    <Col md={8}>
                                        <AdditionalRedirects
                                            columns={efficiency}
                                            dateRange={props.filters.date}
                                        />
                                    </Col>
                                    <Col md={4}>
                                        {splitedClicks
                                            ? <MonthlyViewChart
                                                key="nativkaClicks"
                                                color="#57C8F1"
                                                title="Клики natimatica.com"
                                                value={overview.referrer}
                                                columns={splitedClicks}
                                                dateRange={props.filters.date}
                                            />
                                            : null}
                                    </Col>
                                </Row>
                            </Then>
                        </If>
                    </Grid>
                </div>
            </div>
        </Layout>
    );
};

export default Dashboard;
