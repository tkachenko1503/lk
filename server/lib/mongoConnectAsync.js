import Promise from 'bluebird';
import constants from '../../configs/constants.json';

const MongoDB = Promise.promisifyAll(require('mongodb'));
const mongoDBUri = process.env.MONGODB_URI || constants['devDBUrl'];


export default MongoDB.MongoClient.connectAsync(mongoDBUri);
