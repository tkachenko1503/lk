import React from 'react';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import WidgetSelect from 'components/WidgetSelect';
import Slider from 'components/Slider';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';

import abTestCreateForm from 'styles/abTestCreateForm.css';

const ABTestsItem = (props) => {
    const {widgetsList, setWidget, setFrequency, index} = props;
    const {test, selectedName, needRemoveButton, removeWidget} = props;
    const {widgetId, frequency} = test;

    return (
        <Row className={abTestCreateForm.testItemRow}>
            <Col
                xs={4}
                className={abTestCreateForm.widgetSelect}
            >
                <WidgetSelect
                    widgetsList={widgetsList}
                    widgetSelectHandler={setWidget}
                    choosenWidget={widgetId}
                    defaultTitle="Нет"
                    widgetName={selectedName}
                />
            </Col>
            <Col xs={7}>
                <Slider
                    percent={true}
                    value={frequency}
                    onChange={setFrequency}
                    eventKey={widgetId}
                />
            </Col>
            {needRemoveButton
                ? (
                    <Col xs={1}>
                        <Button
                            className={abTestCreateForm.removeBtn}
                            onClick={removeWidget}
                            data-index={index}
                        >
                            <Glyphicon glyph="trash"/>
                        </Button>
                    </Col>
                )
                : null}
        </Row>
    );
};

export default ABTestsItem;
