import React from 'react';
import classnames from 'classnames';
import BotstrapDropdown from 'react-bootstrap/lib/Dropdown';
import Button from 'react-bootstrap/lib/Button';

import controls from 'styles/controls.css';

const Dropdown = (props) => {
    return (
        <BotstrapDropdown id={props.id} className={controls.dropdownInlineHelper}>
            <Button className={controls.dropdown} bsRole="toggle">
                {props.title}
                <b className={classnames(
                    "fa fa-angle-down",
                    controls.dropdownInlineHelper,
                    controls.dropdownArrow
                )} />
            </Button>

            <BotstrapDropdown.Menu className={props.className}>
                {props.children}
            </BotstrapDropdown.Menu>
        </BotstrapDropdown>
    );
};

export default Dropdown;
