import Parse from 'parse';
import Site from 'models/Site';
import BaseModel from 'models/Base';
import {WIDGET_MAX_PLACES_NUMBER} from 'lib/constants';

const DEFAULT = 'default';

const className = 'Widget';

class Widget extends BaseModel {
    constructor(attrs) {
        super(className);

        if (attrs) {
            this.set(attrs);
        }
    }

    /**
     * Создаём новый виджет
     * @param name
     * @param site
     * @param user
     * @returns {Parse.Promise}
     */
    static spawn({name, site, user}) {
        const widget = new Widget();
        const currentSite = new Site();

        currentSite.id = site;
        widget.set('name', name);
        widget.set('site', currentSite);
        widget.set('user', user);
        widget.set('adsPlacesNumber', 0);
        widget.set('exchangePlaces', 0);
        widget.set('maxPlacesNumber', WIDGET_MAX_PLACES_NUMBER);
        widget.set('format', DEFAULT);

        return widget.save(null);
    }

    /**
     * Получаем виджеты привязанные к сайту
     * @param siteId
     * @returns {Parse.Promise}
     */
    static getBySite(siteId) {
        const currentSite = new Site();
        const query = new Parse.Query(Widget);

        currentSite.id = siteId;
        query
            .equalTo('site', currentSite)
            .notEqualTo('removed', true);

        return query.find();
    }
}

export default Widget;

Parse.Object.registerSubclass('Widget', Widget);
