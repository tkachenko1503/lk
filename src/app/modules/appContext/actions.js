import flyd from 'flyd';
import mergeAll from 'flyd/module/mergeall';
import pipe from 'ramda/src/pipe';
import head from 'ramda/src/head';
import prop from 'ramda/src/prop';
import Site from 'models/Site';
import Advertiser from 'models/Advertiser';
import actions$ from 'actions/index';
import {App, AppContext} from 'actions/types';
import {once} from 'lib/streams';

export const getAppContext = flyd.stream();
export const setAppContext = flyd.stream();
export const setAppContextType = flyd.stream();
export const contextRecieved = flyd.stream();

const extractContextType = pipe(head, prop('className'));
const extractContextId = pipe(head, prop('id'));
const combineSitesAndAdvertisers = (sites, advertisers) => ([...sites(), ...advertisers()]);

const sitesCollection = getAppContext.map(Site.getAvailableSites);
const advertisersCollection = getAppContext.map(Advertiser.getAvailableAdvertisers);

const context = flyd.combine(
    combineSitesAndAdvertisers,
    [sitesCollection, advertisersCollection]
);

flyd.on(contextRecieved, context);

flyd.on(
    setAppContext,
    contextRecieved
        .map(extractContextId)
);

flyd.on(
    setAppContextType,
    contextRecieved
        .map(extractContextType)
);

flyd.on(
    pipe(App.AppContext, actions$),
    mergeAll([
        context.map(AppContext.ResetContext),
        setAppContext.map(AppContext.SetSelectedContext),
        contextRecieved.map(() => AppContext.SetInited()),
        setAppContextType.map(AppContext.SetSelectedContextType)
    ])
);
