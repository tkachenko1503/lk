import React from 'react';
import connectToStore from 'lib/connectToStore';
import {getAppContext, setAppContext, setAppContextType} from 'modules/appContext/actions';
import {findByProp} from 'lib/index';

const mapStateToProps = ({app}) => ({
    appContext: app.context,
    selectedAppContext: app.selectedContext,
    isContextInited: app.isInited
});

export function appContext(Component) {
    return connectToStore(
        class extends React.Component {
            constructor(props) {
                super(props);

                this.setAppContext = this.setAppContext.bind(this);
            }

            get displayName() {
                return 'AppContext';
            }

            componentDidMount() {
                const {isContextInited} = this.props;

                if (!isContextInited) {
                    getAppContext(true);
                }
            }

            setAppContext(context) {
                const contextModel = findByProp(context, this.props.appContext, 'id');

                setAppContext(context);
                setAppContextType(contextModel.className);
            }

            render() {
                return (
                    <Component
                        {...this.props}
                        appContext={this.props.appContext}
                        selectedAppContext={this.props.selectedAppContext}
                        setAppContext={this.setAppContext}
                    />
                );
            }
        },
        mapStateToProps
    );
}
