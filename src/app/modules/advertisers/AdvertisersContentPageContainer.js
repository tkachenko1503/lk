import React from 'react';
import connectToStore from 'lib/connectToStore';
import AdvertisersContent from 'modules/advertisers/AdvertisersContent';
import {fetchAdvertisersContent} from 'modules/advertisers/actions';

class AdvertisersContentPageContainer extends React.Component {
    render() {
        return (
            <AdvertisersContent actionForApplyFilters={fetchAdvertisersContent} />
        )
    }
}

export default connectToStore(AdvertisersContentPageContainer, () => ({

}));
