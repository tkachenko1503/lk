import flyd from 'flyd';

/**
 * Возвращает стрим в который пушатся заначения из
 * оригинального стрима s не более чем раз в dur милисекунд
 * @param {Number} dur колличество милисекунд
 * @param {Function} s стрим
 * @type {Function}
 */
export const throttle = flyd.curryN(2, function (dur, s) {
    var isThrottled = false;
    var savedArgs;

    return flyd.combine(function (s, self) {
        if (isThrottled) {
            savedArgs = s();
            return;
        }

        isThrottled = true;

        setTimeout(function () {
            isThrottled = false;
            if (savedArgs) {
                self(savedArgs);
                savedArgs = null;
            }
        }, dur);

        return s();
    }, [s]);
});


export const once = function (s) {
    var inited = false;

    return flyd.combine(function (s, self) {
        if (!inited) {
            inited = true;
            return s();
        } else {
            self.end(true);
        }
    }, [s]);
};