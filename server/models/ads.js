import knex from '../lib/knex';
import {pipe, prop, map, head} from 'ramda';

const CLICK_COST = 20;

const calcStatsFields = (item) => {
    item.ads_shows = Number(item.ads_shows) || 0;
    item.ads_referrer = Number(item.ads_referrer) || 0;
    item.ctr = Number(item.ctr) || 0;
    item.widget_load = Number(item.widget_load) || 0;
    item.click_cost = Number(item.click_cost) || 0;
    item.created = item.created * 1;

    item.ads_profit = Number(item.ads_profit) || 0;
    item.ads_profit_thousand_shows = item.widget_load ? ((item.ads_profit * 1000) / item.widget_load).toFixed(2) : 0;
    item.ads_profit_mean = item.ads_referrer ? (item.ads_profit / item.ads_referrer).toFixed(2) : 0;

    return item;
};

const proccessAds = pipe(prop('rows'), map(calcStatsFields));
const proccessAdsOverview = pipe(proccessAds, head);

export function getAdsBySite({interval, hoursOffset, widgetId, site, from, to}) {
    const q = knex.raw(`
        SELECT
            sum( coalesce((click)::text::int, 0) ) AS ads_referrer,
            sum( coalesce((show)::text::int, 0) ) AS ads_shows,
            sum( coalesce((profit)::text::int, 0) ) AS ads_profit,
            sum( coalesce((load)::text::int, 0) ) AS widget_load,
            round( ((sum(coalesce((click)::text::decimal, 0)) / sum(coalesce((show)::text::decimal, 1))) * 100), 1 ) AS ctr,
            ${CLICK_COST} AS click_cost,
            date_trunc('${interval}', (created + '${hoursOffset} hour')::timestamp with time zone) AS created
        FROM (
            SELECT
                data->'exchange_incoming_click' AS click,
                data->'exchange_incoming_impression' AS show,
                data->'ads_revenue' AS profit,
                data->'exchange_outgoing_impression' AS load,
                since AS created
            FROM stats 
            WHERE site = '${site}' 
                AND widget_id ${widgetId ? ('= \'' + widgetId + '\'') : 'ISNULL'}
                AND since 
                    BETWEEN ('${from}'::timestamp - interval '${hoursOffset} hours')
                    AND ('${to}'::timestamp - interval '${hoursOffset} hours')
            ORDER BY created
        ) AS sq
        GROUP BY date_trunc('${interval}', (created + '${hoursOffset} hour')::timestamp with time zone)
        ORDER BY created
    `);

    // console.log(q.toSQL());

    return q.then(proccessAds)
            .catch(error => []);
}

export function getOverviewData({interval, hoursOffset, widgetId, site, from, to}) {
    const q = knex.raw(`
        SELECT
            sum( coalesce((click)::text::int, 0) ) AS ads_referrer,
            sum( coalesce((show)::text::int, 0) ) AS ads_shows,
            sum( coalesce((profit)::text::int, 0) ) AS ads_profit,
            sum( coalesce((load)::text::int, 0) ) AS widget_load,
            ${CLICK_COST} AS click_cost
        FROM (
            SELECT
                data->'exchange_incoming_click' AS click,
                data->'exchange_incoming_impression' AS show,
                data->'ads_revenue' AS profit,
                data->'exchange_outgoing_impression' AS load
            FROM stats 
            WHERE site = '${site}' 
                AND widget_id ${widgetId ? ('= \'' + widgetId + '\'') : 'ISNULL'}
                AND since 
                    BETWEEN ('${from}'::timestamp - interval '${hoursOffset} hours')
                    AND ('${to}'::timestamp - interval '${hoursOffset} hours')
        ) AS sq
    `);

    // console.log(q.toSQL());

    return q.then(proccessAdsOverview)
        .catch(error => {});
}
