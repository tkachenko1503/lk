import React from 'react';
import allPass from 'ramda/src/allPass';
import propEq from 'ramda/src/propEq';

class StatsChart extends React.Component {
    constructor(props) {
        super(props);

        this.needRedraw = false;
    }

    shouldComponentUpdate(newProps) {
        const needUpdate = this.isNeedUpdate(newProps);

        if (!this.needRedraw) {
            this.needRedraw = this.isNeedRedrawChart(newProps);
        }

        return needUpdate;
    }

    componentDidUpdate() {
        this.needRedraw = false;
    }

    isNeedRedrawChart(newProps) {
        return this.isNeedShowTime(newProps.dateRange) !== this.isNeedShowTime(this.props.dateRange);
    }

    isNeedUpdate(newProps) {
        const {first, second, created, col1, col2} = this.props;
        const predicates = [
            propEq('first', first),
            propEq('second', second),
            propEq('created', created),
            propEq('col1', col1),
            propEq('col2', col2)
        ];
        const testProps = allPass(predicates);

        return !testProps(newProps);
    }

    isNeedShowTime(dateRange) {
        return (dateRange.to.valueOf() - dateRange.from.valueOf()) <= 345600000;
    }
}

export default StatsChart;
