import curryN from 'ramda/src/curryN';
import {AdsChartControls} from 'actions/types';

export function defaultAdsChartControls() {
    return {
        first: 'widget_load',
        second: 'ads_shows'
    };
}

const reducefn = AdsChartControls.caseOn({
    SetParam: (paramId, buttonId, state) => ({
        ...state,
        [paramId]: buttonId
    })
});

export default curryN(2, reducefn);
